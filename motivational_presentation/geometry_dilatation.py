__all__=["transform","inverse_transform", "interpolate", "inverse_interpolate", "jacobian"]

import numpy as np

def transform(x,calib):
	b=x[-1]
	a=x[0]
	y=np.zeros(np.shape(x))
	y=[pt%1 for pt in (x-calib+0.5)]
	return y

def inverse_transform(y,calib,a,b):
	b=x[-1]
	a=x[0]
	x=np.zeros(np.shape(y))
	x=[pt%(b-a)+a for pt in (y+calib-0.5)]
	return x

def jacobian(x,calib):
	b=x[-1]
	a=x[0]
	jac = 1.
	return jac 

def interpolate(calib_ind, U):
	V=np.zeros(np.size(U))
	truthN=np.size(U)
	for n in range(len(V)):
		V[n]= U[(n+calib_ind+200)%truthN]
	return V
	
def inverse_interpolate(calib_ind, U):
	V=np.zeros(np.size(U))
	truthN=np.size(U)
	for n in range(len(V)):
		V[n]= U[(n-calib_ind-200)%truthN]
	return V
	
