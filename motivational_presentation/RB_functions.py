__all__=["proj_EIM_fun","greedy_mu","proj_IC","POD"]

import numpy as np
import random
from cvxopt import matrix
import matplotlib.pyplot as plt
from numpy import linalg as LA
import shutil, os
import math
import solve as so
from bibli import *
from minL1 import *
from GS import *
from EIM import *
import settings
from geometry import *

def proj_EIM_fun(i, minimization,truthN,count_ndisc,RB,EIM_fun):
	tmp=so.solve(minimization,truthN,count_ndisc,RB,EIM_fun)
#	print i," function projected"
	return np.reshape(tmp,(count_ndisc))
 

def proj_IC(mu, truthN, param,x,minimization, count_ndisc, RB):
	u0      = np.zeros(truthN);
	w0, calib= createInitialCondition(truthN,u0,param,x);
	w = interpolate(calib,w0)
	w1_alpha  = so.solve(minimization,truthN,count_ndisc,RB,w);
	w1_alpha = np.reshape(w1_alpha, (count_ndisc))
#	print "projected IC for param ", mu
	return w1_alpha

def POD(UU,tol):
	print "########################"
	print "POD begins"
	print "########################"
	
	eigs,eigv	= np.linalg.eig(np.dot(np.transpose(UU),UU))
	#sort the eigenvalues
	idx = eigs.argsort() 
	idx = idx[::-1]	
	eigs = eigs[idx]
	eigv = eigv[:,idx]
	#remove complex parts
	eigs = np.real(eigs)
	eigv = np.real(eigv)
	#sum of eigenvalues
	tot = np.sum(eigs)
	#compute how many POD modes to have in the basis =>> M1
	M1=0 
	x1=0
	while (x1 < 1-tol):
		x1 +=eigs[M1]/tot
		M1+=1
	print "elem in the basis M=",M1
	#consider only the first M1 eigenvalues and eigenvectors 
	eigs = eigs[:M1]  
	eigv = eigv[:,:M1]

	#fill the reduced basis with the first M1 POD modes 

	POD_basis	=	np.dot(UU,eigv)
	
	print "Update the reduced basis with the next chosen M POD modes "
	print "########################"
	print "POD ends"
	print "########################"
	return POD_basis,M1

def greedy_mu(mu):
	print "#########################"
	print "MU====",mu
	print "#########################"
	# First find the reduced initial condition, i.e the $L^1$ projection of
	# the initial condition onto the current dictionnary
	# That is the dictionary of size count_ndisc !!
	u0      = np.zeros(truthN);
	w      = createInitialCondition(truthN,u0,param_domain[mu],x);
	w1_alpha  = so.solve(minimization,truthN,count_ndisc,RB[:,:count_ndisc],w);
	w1_alpha = np.reshape(w1_alpha,(count_ndisc,1))
	
	# From the coordinates on the reduced space, build the full reduced
	# initial condition
	w_ROM  = np.zeros((truthN));
	for j in range(count_ndisc):
		w_ROM[:]  = w_ROM[:] + w1_alpha[j]*RB[:,j];
	##Now, we run our time dependent scheme
	for it in range(1, Nt+1):
		#print "it=",it
		w0 			= np.copy(w_ROM);
		w0_alpha   	= np.copy(w1_alpha);
		# Use our numerical scheme to compute
		# w_{rom}(t^{n+1}) = F(w_{rom}(t^{n})
		uu1     	= np.zeros(count_ndisc);
		w1_alpha,dt,RHS_alp, RHS_M1  = diffSolve_RB_EIM_proj(dx,CFL,w0,w0_alpha,uu1,truthN, magic_pts, EIM_fun_RB,EIM_inv, magic_M1);

		# Project onto reduced space	
		w_ROM 	= np.zeros((truthN));
						
		for j in range(count_ndisc):
			w_ROM[:]  = w_ROM[:]+ w1_alpha[j]*RB[:,j];
			
#			plt.plot(w_ROM)
#			plt.plot(w);
#			plt.show();		
		tmp=np.dot(RB[magic_M1,:count_ndisc],RHS_alp)
		tmp2=(tmp-RHS_M1)*norm_fnc_M1
		err_ind[mu]  += CE*np.linalg.norm(tmp2,1)/Nt/M1_EIM;
		err_ind[mu]  += np.linalg.norm(w1_alpha[:]-w0_alpha[:]+RHS_alp,1)/Nt/count_ndisc;
		true_err[mu] += dx*(np.linalg.norm(w_ROM[:]-real_sol[:,it,mu],2))/Nt


