## Greedy alg for Burgers eq.
## plot the err indicator, L2 truth error and L2 average error
## plot greedy coef
## plot dictionary members
## this is used for the paper =>>  param from greedy disc_locs= [0.45, 0.2, 0.7, 0.35, 0.6, 0.55, 0.3] 

import numpy as np
import random
from RB_functions import *
from cvxopt import matrix
import matplotlib.pyplot as plt
from numpy import linalg as LA
import shutil, os
import math
import solve as so
from bibli import *
from minL1 import *
from GS import *
from EIM import *
from joblib import Parallel, delayed
import multiprocessing
from geometry import *




###########################################
###Choose the desired minimization type
#minimization = 'L1min_LP'
#minimization = 'L1min_IRLS'
#minimization  = 'Hubermin_IRLS'
minimization = 'L2'
#minimization  = 'Hull'

###########################################
## Greedy parameters
## Given tolerance
tol_EIM=1.e-6
tol_greedy=1.e-4
tol_POD_1=1.e-2
tol_POD_2= 1.e-1
max_it_EIM=200
M1_EIM=15
EIM_import =False# True#
load_real_solution =False#True# 

###############################################################################
## DEFINE PDE Problem Parameters !

#Size of truth approximation space : \mathcal{N}
truthN =400;
# CFL
CFL    = 0.5;
# Number of time steps !
Nt     = 300; #max nb of iterations
# Final time
#T      = 0.1;

CE=1. #should be 2


# Define \Omega : [0, \pi]
x      = np.linspace(0.,1.,truthN+1)[:-1]*math.pi;
dx     = x[1]-x[0];
dt     = 0.;
x_calib = np.linspace(0.,1.,truthN)


###############################################################################
# Parameter space now !
# Define the fine parameter space

interval_min=0.4
interval_max=0.6
param_domain = np.arange(interval_min,interval_max+0.025,0.01)
param_domain=np.array(param_domain)
print param_domain

max_nb_cand  = len(param_domain)
print max_nb_cand


## How big will our final basis be ?
ndisc=200 ##max can be max_nb_cand*(Nt+1)


## Initialize our dictionary !
new_dict    = np.zeros(ndisc)
disc_init   = [0.5]

new_dict[0] = disc_init[0]

	
###############################################################################
## Let's go for the actual greedy algorithm !
u0      = np.zeros(truthN);
uu0     = np.zeros(truthN);
uu1     = np.zeros(truthN);
U       = np.zeros(truthN*(Nt+1)).reshape(truthN,Nt+1);
RB	  = np.zeros((truthN,ndisc))
calibs_ind=np.zeros((max_nb_cand,Nt+1),dtype=np.int)
calibs=np.zeros((max_nb_cand,Nt+1))

# Thats it, our dictionary has M1 POD modes for the initial starting param \mu=0.45!

	
## Build Real solutions !
if (load_real_solution==False):
	fluxes=np.zeros((truthN,max_nb_cand*(Nt+1))) #fluxes for EIM
	real_sol = np.zeros((truthN,Nt+1,max_nb_cand));
	fluxes_not_cal=np.zeros((truthN,max_nb_cand*(Nt+1))) #fluxes for EIM
	real_sol_not_cal = np.zeros((truthN,Nt+1,max_nb_cand));
	for cand in range(max_nb_cand):
		u0 ,calib_index    = createInitialCondition(truthN,u0,param_domain[cand],x);
		calibs_ind[cand,0] = calib_index

		U, U_flux, calib_index= solveFOM(CFL,truthN,Nt,U,uu0,uu1,u0,dx,dt);
		calibs_ind[cand,1:]=calib_index		
		calibs[cand,:]=x[calibs_ind[cand,:]]
		real_sol_not_cal[:,:,cand] = U;
		fluxes_not_cal[:,cand*Nt:(cand+1)*Nt]=np.transpose(U_flux) #All fluxes for every timestep of this parameter
		for it in range(Nt+1):
			real_sol[:,it,cand]= interpolate(calibs_ind[cand,it],U[:,it]) # U[(n+calibs_ind[cand,it]+200)%truthN,it]
			if it<Nt+1:
				fluxes[:,cand*Nt+it] = interpolate(calibs_ind[cand,it],fluxes_not_cal[:,cand*Nt+it])
	np.savez('data/real_sol',real_sol,fluxes, calibs_ind, calibs, real_sol_not_cal, fluxes_not_cal)
else:
	real_data=np.load('data/real_sol.npz')
	real_sol=real_data['arr_0']
	fluxes=real_data['arr_1']
	calibs_ind = real_data['arr_2']
	calibs = real_data['arr_3']
	real_sol_not_cal = real_data['arr_4']
	fluxes_not_cal = real_data ['arr_5']



#for cand in [0,1,10]:#,34,50,64,75,80,99]:
#		for it in [0,100]:
#			plt.plot(real_sol_not_cal[:,it,cand], label="real")
#			plt.plot(calibs_ind[cand,it],real_sol_not_cal[calibs_ind[cand,it],it,cand] , 'o', label="calib")

#plt.show()

#for cand in [0,1,10]:#,34,50,64,75,80,99]:
#		for it in [0,100]:
#			plt.plot(real_sol[:,it,cand], label="calibsol")
#plt.show()

#for cand in [0,1,10]:#,34,50,64,75,80,99]:
#		for it in [0,100]:
#			plt.plot(fluxes_not_cal[:,it+ Nt*cand], label="real")
#			
#plt.show()

#for cand in [0,1,10]:#,34,50,64,75,80,99]:
#		for it in [0,100]:
#			plt.plot(fluxes[:,it + Nt*cand], label="calibsol")
#plt.show()




if (EIM_import==True) :
	EIM_fun = np.load('data/fun_EIM.npy')
	magic_pts = np.load('data/magic_pts.npy')
	magic_M1=np.load('data/magic_M1.npy')
	fnc_M1=np.load('data/fnc_M1.npy')
	
else:
	[ EIM_fun, magic_pts,magic_M1, fnc_M1] = EIM (fluxes ,tol_EIM, max_it_EIM, M1_EIM)
	np.save('data/fun_EIM.npy', EIM_fun)
	np.save('data/magic_pts.npy', magic_pts)
	np.save('data/magic_M1.npy', magic_M1)
	np.save('data/fnc_M1.npy', fnc_M1)
	
#magic_M1= range(truthN)
norm_fnc_M1=np.linalg.norm(fnc_M1,1,axis=1)
EIM_dim=np.shape(EIM_fun)[1]
EIM_inv=np.linalg.inv(EIM_fun[magic_pts,:])

max_err_ind=np.zeros(truthN)
avg_err=np.zeros(truthN)
t_err=np.zeros(truthN)


true_err=np.zeros(truthN)
err_ind=np.zeros(truthN)
dim_basis_func=[]

count_ndisc	= 0
count_M1	= 0

err_ind	= np.ones(max_nb_cand);
max_err_for_tolerance = 1.;
num_cores = multiprocessing.cpu_count()

##### Perform POD on the snapshots previously computed UU, and store the first
    #  POD modes in the basis functions matrix.
    #  Input arguments are: inner product matrix UU^T * UU and tol 
    #  Output arguments are: POD modes, number of POD modes


## Firstly compute the full solution for \mu in disc_init
calib_indices=np.zeros(Nt+1,dtype='int')
u0_not_cal  , calib_indices[0]    = createInitialCondition(truthN,u0,new_dict[0],x);
U_not_cal,U_flux, calib_indices[1:] = solveFOM(CFL,truthN,Nt,U,uu0,uu1,u0,dx,dt);

U=np.zeros(np.shape(U_not_cal))
for it in range(Nt+1):
	U[:,it]=interpolate(calib_indices[it], U_not_cal[:,it])

###### Start the POD process for first chosen param
POD_basis, M1 = POD(U,tol_POD_1)
RB[:,0:M1]	  = gs(POD_basis)


count_ndisc=count_ndisc+M1
dim_basis_func.append(count_ndisc)


while (count_ndisc<ndisc and count_ndisc<=truthN and max_err_for_tolerance>tol_greedy ):
	w_ROM 	= np.zeros((truthN,Nt+1,max_nb_cand));
	print "#########################"
	print "count_ndisc====",count_ndisc
	print "#########################"
	# Initialize our error indicator !
	err_ind = np.zeros(max_nb_cand);
	true_err= np.zeros(max_nb_cand);
#	Projection of EIM onto RB space
	print "#####################"
	print "Projecting EIM basis onto RB basis"
	print "#####################"
	EIM_fun_RB =np.zeros((count_ndisc,EIM_dim))
	
#	for i in :
	EIM_fun_RB=Parallel(n_jobs=num_cores)(delayed(proj_EIM_fun)(i, minimization, truthN, count_ndisc, np.array(RB[:,:count_ndisc]), np.array(EIM_fun[:,i])) for i in range(EIM_dim))
	EIM_fun_RB = np.transpose(np.array ( EIM_fun_RB ))

#	for n in range(count_ndisc):
#		plt.plot(EIM_fun_RB[:,n])
#	plt.show()

	
	# We go trough our fine parameter space
	print 'EIM nan', np.isnan(EIM_fun_RB).any()
  #w_init_alpha shape (count_ndisc, max_nb_cand)
	w_init_alpha = Parallel(n_jobs=num_cores)(delayed(proj_IC)(mu, truthN, param_domain[mu],x,minimization, count_ndisc, RB[:,:count_ndisc]) for mu in range(max_nb_cand))
	w1_init_alpha = np.transpose(np.array(w_init_alpha))
	print np.shape(w1_init_alpha)

	print "#####################"
	print "Greedy step"
	print "#####################"
	for mu in range(max_nb_cand):
		# First find the reduced initial condition, i.e the $L^1$ projection of
		# the initial condition onto the current dictionnary
		# That is the dictionary of size count_ndisc !!
		w1_alpha = np.reshape(w1_init_alpha[:,mu],(count_ndisc,1))
    
		
		# From the coordinates on the reduced space, build the full reduced
		# initial condition
		for j in range(count_ndisc):
			w_ROM[:,0,mu]  = w_ROM[:,0,mu] + w1_alpha[j]*RB[:,j];
#		plt.plot(w_ROM)
#		plt.plot(real_sol[:,0,mu]);
#		plt.show();
		#print "cond RB for IC=",np.linalg.cond(RB[:,:count_ndisc])	
		##Now, we run our time dependent scheme
		for it in range(1, Nt+1):
			#print "it=",it
			w0 			= np.copy(w_ROM[:,it-1,mu]);
			w0_alpha   	= np.copy(w1_alpha);
			# Use our numerical scheme to compute
			# w_{rom}(t^{n+1}) = F(w_{rom}(t^{n})
#			uu1     	= np.zeros(truthN);
			uu1     	= np.zeros(count_ndisc);

			w0_copy=np.copy(w0)
#			while(calib_correct==False):
			w1_alpha,dt,RHS_alp, RHS_M1  = diffSolve_RB_EIM_proj(dx,CFL,w0_copy,w0_alpha,uu1,truthN, magic_pts, EIM_fun_RB,EIM_inv, magic_M1);
#			w1,dt,RHS  = diffSolve_EIM(dx,CFL,w0,uu1,truthN, magic_pts, EIM_fun);
#			w1_alpha  = so.solve(minimization,truthN,count_ndisc,RB[:,:count_ndisc],w1);

			# Project onto reduced space	
			w1_0=np.zeros(truthN)
						
			for j in range(count_ndisc):
				w1_0  = w1_0+ w1_alpha[j]*RB[:,j];

			if ((calibs_ind[mu,it]-calibs_ind[mu,it-1])!=0):
				print calibs_ind[mu,it]-calibs_ind[mu,it-1]
				w1_0=interpolate(calibs_ind[mu,it]-calibs_ind[mu,it-1]-200, w1_0)
				w1_alpha=np.reshape(so.solve(minimization,truthN,count_ndisc,RB[:,:count_ndisc],w1_0),(count_ndisc,1))
				for j in range(count_ndisc):
					w_ROM[:,it,mu] = w_ROM[:,it,mu]+ w1_alpha[j]*RB[:,j];
#				plt.plot(w_ROM[:,it,mu],label="calibr")
#				plt.plot(w1_0)
#				plt.legend()
#				plt.show()
			else:
				w_ROM[:,it,mu]=w1_0
#				calib_index = np.argmax(abs(w_ROM[:-1,it,mu]-w_ROM[1:,it,mu]))
#				if abs(calib_index-200)>2:
#					print calib_index, it
##					plt.plot(w0,label="or")
#					w0=interpolate(calib_index,w0)
#					w0_alpha=np.reshape(so.solve(minimization,truthN,count_ndisc,RB[:,:count_ndisc],w0),(count_ndisc,1))
#					w0_copy=np.zeros(truthN)
#					for j in range(count_ndisc):
#						w0_copy  = w0_copy + w0_alpha[j]*RB[:,j];
##					plt.plot(w0,label="recomputed")
##					plt.legend()
##					plt.show()
#					print "recompute"
#				else:
#					calib_correct=True
#				


#			plt.plot(w_ROM)
#			plt.plot(w);
#			plt.show();		
			tmp=np.dot(RB[magic_M1,:count_ndisc],RHS_alp)
			tmp2=(tmp-RHS_M1)*norm_fnc_M1
			err_ind[mu]  += CE*np.linalg.norm(tmp2,1)/Nt/M1_EIM;
			err_ind[mu]  += np.linalg.norm(w1_alpha[:]-w0_alpha[:]+RHS_alp,1)/Nt/count_ndisc;
			true_err[mu] += dx*(np.linalg.norm(w_ROM[:,it,mu]-real_sol[:,it,mu],2))/Nt
		#print "mu=",mu
#		print "true_ind=",true_err
#		print "err_ind=",err_ind
#		plt.plot(w_ROM[:,Nt,mu]);
#		plt.plot(real_sol[:,Nt,mu])
#		plt.plot(RB[:,:count_ndisc]);
#		plt.show();
	
	
	# PICK the worst parameter
	chosen_param		= np.argmax(true_err);
	new_dict[count_ndisc]	= param_domain[chosen_param]
      #take the maximum error ind in L1 and truth error in L2 and average error in L2
	max_err_for_tolerance =  max(true_err)#max(err_ind);#
	max_err_ind[count_M1] = max(err_ind);
	t_err[count_M1]	= max(true_err);
	avg_err[count_M1]	= sum(true_err)/max_nb_cand
	# print error
	print "max_err_ind",max_err_ind[:count_M1+1]
	print "max_true_err",t_err[:count_M1+1]
	print "avg_err",avg_err[:count_M1+1]
	print "at position",count_ndisc,"chosen_param=",param_domain[chosen_param];
	#plot the reduced sol and the real sol
#	plt.plot(w_ROM[:,Nt,chosen_param],label="rom");
#	plt.plot(real_sol[:,Nt,chosen_param],label="real");
#	plt.plot(magic_M1,w_ROM[magic_M1,Nt,chosen_param],'r.')
#	plt.legend()
#	plt.show();
	########### Do again the POD 
#	real_sol_proj		  =np.zeros((truthN,Nt+1))
	#RB_GS				  =np.zeros((truthN,count_ndisc)) 
	#perform Gram-Schmidt orthogonalisation
	#RB_GS[:,:]			  =gs(RB[:,:count_ndisc])
	#RB[:,:count_ndisc]	  =gs(RB[:,:count_ndisc])
	#compute ROM for the chosen param =>> w_ROM_RB

#	w_ROM_RB	= w_ROM[:,:,chosen_param]
	w		= real_sol[:,:,chosen_param]
	
	w_ROM_RB = np.zeros((truthN, Nt+1))
	#project the real_sol for the chosen param onto the reduced orthogonalized basis (in order not to consider the same values if the chosen_param will be choosen again)
	w_ROM_RB_alp=Parallel(n_jobs=num_cores)(delayed(proj_EIM_fun)(it, minimization, truthN, count_ndisc, np.array(RB[:,:count_ndisc]), w[:,it] ) for it in range(Nt+1))
	w_ROM_RB_alp = np.transpose(np.array(w_ROM_RB_alp))
	print np.shape(w_ROM_RB_alp)
	print np.shape(RB[:,:count_ndisc])
	for it in range(Nt+1):
		for j in range(count_ndisc):
			w_ROM_RB[:,it]  = w_ROM_RB[:,it] + w_ROM_RB_alp[j,it]*RB[:,j];

#	for it in range(Nt+1):
		# Project the real sol for chosen_param onto the reduced space	
#		alpha 	= so.solve(minimization,truthN,count_ndisc,RB[:,:count_ndisc],w[:,it]);								
#		for j in range(count_ndisc):
#			w_ROM_RB[:,it]  = w_ROM_RB[:,it] + alpha[j]*RB[:,j];


	real_sol_proj = w-w_ROM_RB  ##proj in L1

#	plt.plot(w[:,150])
#	plt.plot(w_ROM_RB[:,150])
#	plt.plot(real_sol_proj[:,150])
#	plt.title("Comp proj")
#	plt.show()
#	plt.plot(RB[:,:count_ndisc], label="RB")
#	plt.plot(w[:,150], label="w exact")
#	plt.plot(w_ROM_RB[:,150], label="reduced")
#	plt.plot(real_sol_proj[:,150], label="proj ortogonal")
#	plt.legend()
#	plt.show()

	########### Update the reduced basis with the next chosen M1 POD modes
	POD_basis, M1=	POD(real_sol_proj[:,:], tol_POD_2 )
	RB[:,count_ndisc:(M1+count_ndisc)] = POD_basis
	RB[:,count_ndisc:(M1+count_ndisc)] = gs(	RB[:,count_ndisc:(M1+count_ndisc)])	
	POD_res, M2=POD(RB[:,:count_ndisc+M1],10.**(-9))
	RB[:,:M2]=POD_res

#	plt.plot(POD_basis)
#	plt.title("POD basis")
#	plt.show()
#	POD_basis, M2 = POD(RB[:,:(M1+count_ndisc)],tol_POD_2)
	#plot the reduced basis
#	plt.plot(x,RB[:,count_ndisc:(M1+count_ndisc)])
#	plt.show()
	#update the nb of elem in the basis
#	count_ndisc=M2
	count_ndisc=M2
	count_M1=count_M1+1
	dim_basis_func.append(count_ndisc)
	print count_ndisc
	print count_M1
	print dim_basis_func

EIM_fun_RB =np.zeros((count_ndisc,EIM_dim))

EIM_fun_RB=Parallel(n_jobs=num_cores)(delayed(proj_EIM_fun)(i, minimization, truthN, count_ndisc, np.array(RB[:,:count_ndisc]), np.array(EIM_fun[:,i])) for i in range(EIM_dim))

EIM_fun_RB = np.transpose(np.array ( EIM_fun_RB ))

np.save('data/RB',RB[:,:count_ndisc])
np.save('data/EIM_fun_RB.npy',EIM_fun_RB)

count_nonzero_err_ind=np.count_nonzero(max_err_ind)
max_err_ind_plot=max_err_ind[:count_nonzero_err_ind]
print max_err_ind_plot
count_nonzero_t_err=np.count_nonzero(t_err)
t_err_plot=t_err[:count_nonzero_t_err]
print t_err_plot
count_nonzero_avg_err=np.count_nonzero(avg_err)
avg_err_plot=avg_err[:count_nonzero_avg_err]
print avg_err_plot
	
## print the error indicator
plt.ion()
plt.figure()
plt.plot(dim_basis_func[:-1],max_err_ind_plot,'x-',label="Error indicator L1")
plt.plot(dim_basis_func[:-1],t_err_plot,label="True error")
plt.plot(dim_basis_func[:-1],avg_err_plot,label="Average error")
plt.yscale('log')
plt.xlabel('Dimension Reduced Basis')
plt.ylabel('Error')
plt.legend(loc=3)
plt.show()
plt.savefig('errors_with_POD.png')




