import numpy as np
def EIM(fluxes, tol, max_it, Mp):
	print '#######################'
	print 'EIM begins'
	print '#######################'
	err=1.+tol	
	m=0
	fluxes=np.array(fluxes)
	print np.shape(fluxes)
	truthN, N_flux = np.shape(fluxes)
	EIM_fun=np.zeros((truthN, max_it))
	magic_pts=[]
	proj=np.zeros((truthN,N_flux))
	
	norms=np.linalg.norm(fluxes, np.inf ,axis=0)
	max_ind=np.argmax(norms)
	while (err>tol and m<max_it):
		m=m+1
		print 'EIM iteration: ',m
		if m==1:
			r=fluxes[:,max_ind]
		else:
			beta=np.linalg.solve(EIM_fun[magic_pts,:m-1],fluxes[magic_pts,max_ind])
   			r=fluxes[:,max_ind]- np.dot(EIM_fun[:,:m-1],beta)
		new_pt=np.argmax(abs(r))
		magic_pts.append(new_pt)
		EIM_fun[:,m-1]=r/r[magic_pts[-1]]
		betas=[]
		for i in range(N_flux):
			betas.append(np.linalg.solve(EIM_fun[magic_pts,:m],fluxes[magic_pts,i]))
		proj=np.dot(EIM_fun[:,:m],np.transpose(betas))
		diff=fluxes-proj
		norms=np.linalg.norm(diff, np.inf ,axis=0)
		err=np.max(norms)
		print 'EIM error: ',err
		max_ind=np.argmax(norms)
	m_EIM= m
	Mp_fun=np.zeros((truthN,Mp))
	err_EIM=err
	
	print m, Mp, m_EIM
	while (m<Mp+m_EIM):
		m=m+1
		print 'EIM iteration: ',m
		if m==1:
			r=fluxes[:,max_ind]
		else:
			beta=np.linalg.solve(EIM_fun[magic_pts,:m-1],fluxes[magic_pts,max_ind])
   			r=fluxes[:,max_ind]- np.dot(EIM_fun[:,:m-1],beta)
		new_pt=np.argmax(abs(r))
		magic_pts.append(new_pt)
		EIM_fun[:,m-1]=r/r[magic_pts[-1]]
		betas=[]
		for i in range(N_flux):
			betas.append(np.linalg.solve(EIM_fun[magic_pts,:m],fluxes[magic_pts,i]))
		proj=np.dot(EIM_fun[:,:m],np.transpose(betas))
		diff=fluxes-proj
		norms=np.linalg.norm(diff, np.inf ,axis=0)
		err=np.max(norms)
		print 'EIM error: ',err
		max_ind=np.argmax(norms)	
		
	Mp_pts = magic_pts[m_EIM:m]
	Mp_fun = EIM_fun[m_EIM:m]
	EIM_fun=np.delete(EIM_fun, np.s_[m_EIM:max_it], axis=1)
	magic_pts = magic_pts[:m_EIM]
	print np.shape(EIM_fun)
	print np.shape(magic_pts)
	print '#######################'
	print 'EIM ends'
	print 'EIM basis functions selected: ', m_EIM
	print 'EIM error: ', err_EIM
	print '#######################'
	return EIM_fun, magic_pts,Mp_pts,Mp_fun

