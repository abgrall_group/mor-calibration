import numpy as np
import random
from cvxopt import matrix
import matplotlib.pyplot as plt
from numpy import linalg as LA
import shutil, os
import math
import solve as so
from bibli import *
from minL1 import *
from GS import *
from EIM import *
import time

###########################################
###Choose the desired minimization type

#minimization = 'L1min_LP'
#minimization = 'L1min_IRLS'
#minimization  = 'Hubermin_IRLS'
minimization = 'L2'
#minimization  = 'Hull'


EIM_fun = np.load('data/fun_EIM.npy')
magic_pts = np.load('data/magic_pts.npy')
magic_M1=np.load('data/magic_M1.npy')
fnc_M1=np.load('data/fnc_M1.npy')
EIM_fun_RB = np.load('data/EIM_fun_RB.npy')
M1_EIM = np.shape(fnc_M1)[0]

print np.shape(EIM_fun)
print np.shape(EIM_fun_RB)


real_data=np.load('data/real_sol.npz')
real_sol=real_data['arr_0']
fluxes=real_data['arr_1']

norm_fnc_M1=np.linalg.norm(fnc_M1,1,axis=1)
EIM_dim=np.shape(EIM_fun)[1]
EIM_inv=np.linalg.inv(EIM_fun[magic_pts,:])
RB = np.load ("data/RB.npy")
print np.shape(RB)

count_ndisc = np.shape(EIM_fun_RB)[0]
RB=RB[:,:count_ndisc]
###############################################################################
## DEFINE PDE Problem Parameters !

#Size of truth approximation space : \mathcal{N}
truthN = np.shape(RB)[0];
# CFL
CFL    = 0.5;
# Number of time steps !
Nt     = 159; #max nb of iterations
# Final time
#T      = 0.1;

CE=2.


# Define \Omega : [0, \pi]
x      = np.linspace(0.,1.,truthN)*math.pi;
dx     = x[1]-x[0];
dt     = 0.;

########################################



mu = 0.5763

err_ind=0
true_err=0
print "Computing real solution"
u0      = np.zeros(truthN);
uu0     = np.zeros(truthN);
uu1     = np.zeros(truthN);
U       = np.zeros(truthN*(Nt+1)).reshape(truthN,Nt+1);
t_init=time.time()
u0                  = createInitialCondition(truthN,u0,mu,x);
U,U_flux            = solveFOM(CFL,truthN,Nt,U,uu0,uu1,u0,dx,dt);
t_fin=time.time() - t_init
print "time_truthN", t_fin
real_sol          	= U;
#print "u0", np.isnan(u0).any()
w_ROM  = np.zeros((truthN));

w      = u0

t_init=time.time()
w1_alpha  = so.solve(minimization,truthN,count_ndisc,RB,w);
w1_alpha = np.reshape(w1_alpha,(count_ndisc,1))
#print "w1_alpha", np.isnan(w1_alpha).any()
# From the coordinates on the reduced space, build the full reduced
# initial condition

for j in range(count_ndisc):
	w_ROM[:]  = w_ROM[:] + w1_alpha[j]*RB[:,j];
#		plt.plot(w_ROM)
#		plt.plot(real_sol[:,0,mu]);
#		plt.show();
#print "cond RB for IC=",np.linalg.cond(RB[:,:count_ndisc])	
##Now, we run our time dependent scheme
for it in range(1, Nt+1):
	#print "it=",it
	w0 			= np.copy(w_ROM);
	w0_alpha   	= np.copy(w1_alpha);
	# Use our numerical scheme to compute
	# w_{rom}(t^{n+1}) = F(w_{rom}(t^{n})
	uu1     	= np.zeros(count_ndisc);
#	print "un", np.isnan(w0).any()
#	print "un alpha", np.isnan(w0_alpha).any()
	w1_alpha,dt,RHS_alp, RHS_M1  = diffSolve_RB_EIM_proj(dx,CFL,w0,w0_alpha,uu1,truthN, magic_pts, EIM_fun_RB,EIM_inv, magic_M1);
#	print "un1 alpha", np.isnan(w1_alpha).any()
	# Project onto reduced space	
	w_ROM 	= np.zeros((truthN));
					
	for j in range(count_ndisc):
		w_ROM[:]  = w_ROM[:]+ w1_alpha[j]*RB[:,j];
#	print "un1", np.isnan(w_ROM).any()	
#	plt.plot(w_ROM)
#	plt.show();		
	tmp=np.dot(RB[magic_M1,:count_ndisc],RHS_alp)
	tmp2=(tmp-RHS_M1)*norm_fnc_M1
	err_ind  += CE*np.linalg.norm(tmp2,1)/Nt/M1_EIM;
	err_ind  += np.linalg.norm(w1_alpha[:]-w0_alpha[:]+RHS_alp,1)/Nt/count_ndisc;
	true_err += dx*(np.linalg.norm(w_ROM[:]-real_sol[:,it],2))/Nt
#print w1_alpha,sum(w1_alpha)	
t_fin=time.time() - t_init
print "RB_time", t_fin
print sum(w1_alpha>0.1)
#print "mu=",mu
print "true_ind=",true_err
print "err_ind=",err_ind
plt.plot(w_ROM, label="RB");
plt.plot(real_sol[:,Nt],label="real")
#for j in range(count_ndisc):
#	plt.plot(RB[:,j]);
plt.legend()
plt.show();
