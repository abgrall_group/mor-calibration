__all__=["buildAdvOperator", "createInitialCondition", "solveFOM", "solveFOM2D", "diffSolve", "diffSolve_EIM", "diffSolve_RB_EIM", "diffSolve2D", "burger", "maximum_vitesse","diffSolve_RB_EIM_proj","aperiodicity"]
import math
import numpy as np
from scipy.sparse import spdiags
import solve as so


def aperiodicity(U):
	n=np.shape(U)[0]
	aper=np.max(abs(U[:n/2]-U[n/2:]))/U.max()
	return aper


#############################
def buildAdvOperator(param,N,dx):
    #for the steady case
    paramMatrix=np.zeros(N*2).reshape(N,2)
    A=np.zeros(N*N).reshape(N,N)
    paramMatrix[:,0]=-param
    paramMatrix[:,1]= param
    paramMatrix[:,:]=paramMatrix[:,:]/dx
    A=spdiags(np.transpose(paramMatrix), [-1,0], N, N).toarray()
    return A
	


#####################
def  createInitialCondition(n,u0,disc_locs,x):
   max_speed=0.
   for i in range(n):
#    if x[i]<=disc_locs:
#        u0[i] = 1
#       u0[i]=(disc_locs)*(abs((math.sin(2*x[i]))))+0.1#math.cos(2*x[i])#disc_locs)#+1.0*disc_locs) # la vitesse change
       u0[i]=abs((math.sin(x[i]+disc_locs)))+0.1
#       u0[i]=((disc_locs/10.*math.sin(4*x[i]))+1.+0.2*math.cos(disc_locs*100.))#*math.exp(disc_locs)+math.cos(2*x[i])#disc_locs)#+1.0*disc_locs) # la vitesse change
   #   print 'u0 disc_locs= ', disc_locs
 #     u0[i]=1.0#disc_locs
# fort source term
 #   print u0
#   print
 #  u0[0]=disc_locs
   calib = np.argmax(abs(u0[:-1]-u0[1:]))
   return u0, calib
#########################################

##########################################
def solveFOM(CFL,n,Nt,U,uu0,uu1,u0,dx, dt):
# solve the Burger problem with upwind first order scheme.
  temps=0.
  dt=0.
  U[:,0]=u0
  EIM_fun=[]
  calib=[]
  for it in range(Nt):
     jt=it+1
     for i in range(n):
      uu0[i]=U[i,it]
     uu1,dt,RHS=diffSolve(dx,CFL,uu0,uu1,n)
     EIM_fun.append(RHS)
     temps=temps+dt    # en fait il doit y avoir un probleme de chronologie: il serait bon que les U soit au meme temps !
     for i in range(n):
      U[i,jt]=uu1[i]
     calib.append(np.argmax(abs(uu1[:-1]-uu1[1:])))
  return U, EIM_fun, calib
##########################################
def solveFOM2D(CFL,n,Nt,U,uu0,uu1,u0,dx, dt,disc):
# solve the Burger problem with upwind first order scheme.
  temps=0.
  dt=0.
  U[:,0]=u0
  for it in range(Nt):
     jt=it+1
     for i in range(n):
      uu0[i]=U[i,it]
     uu1,dt=diffSolve2D(dx,CFL,uu0,uu1,n,disc)
     temps=temps+dt    # en fait il doit y avoir un probleme de chronologie: il serait bon que les U soit au meme temps !
     uu1[0]=u0[0]
     for i in range(n):
      U[i,jt]=uu1[i]
 

  return U
#############################################
def diffSolve(dx,CFL,w0,w1,n):
# on resoud le schema machin
   umax=maximum_vitesse(w0,n)
   
   RHS=np.zeros(n)
   lamd=CFL/max(umax,1.e-5)
   dt=CFL*dx/max(umax,1.e-5)
   for i in range(n):
      if i>0 :
        if n-1>i:
           jm1=i-1
           j  = i
           jp1=i+1
        else: #i=n-1
           j=n-1
           jm1=n-2
           jp1=0           
      else:
           j=0
           jm1=n-1
           jp1=1

      fm=burger(w0[jm1],w0[j])
      fp=burger(w0[j],w0[jp1])
      w1[i]=w0[i] -lamd*( fp-fm)
      RHS[i]=lamd*(fp-fm)
      
   #print "dt    ==== ",dt
   return w1, dt,RHS
#############################################
def diffSolve_EIM(dx,CFL,w0,w1,n,mgc_pts, EIM_fun):
# on resoud le schema machin
   umax=maximum_vitesse(w0,n)
   
   RHS=np.zeros(n)
   lamd=CFL/max(umax,1.e-5)
   dt=CFL*dx/max(umax,1.e-5)
   for i in mgc_pts:
      if i>0 :
        if n-1>i:
           jm1=i-1
           j  = i
           jp1=i+1
        else: #i=n-1
           j=n-1
           jm1=n-2
           jp1=0           
      else:
           j=0
           jm1=n-1
           jp1=1

      fm=burger(w0[jm1],w0[j])
      fp=burger(w0[j],w0[jp1])

      RHS[i]=lamd*(fp-fm)
   beta=np.linalg.solve(EIM_fun[mgc_pts,:],RHS[mgc_pts])
   RHS= np.dot(EIM_fun,beta)
   w1=w0 -RHS
   #print "dt    ==== ",dt
   return w1, dt,RHS

#######################################################
def diffSolve_RB_EIM(dx,CFL,w0,w0_alp,w1_alp,n,mgc_pts, EIM_fun,RB,minimization):
# on resoud le schema machin
   umax=maximum_vitesse(w0,n)
   
   RHS=np.zeros(n)
   lamd=CFL/max(umax,1.e-5)
   dt=CFL*dx/max(umax,1.e-5)
   for i in mgc_pts:
      if i>0 :
        if n-1>i:
           jm1=i-1
           j  = i
           jp1=i+1
        else: #i=n-1
           j=n-1
           jm1=n-2
           jp1=0           
      else:
           j=0
           jm1=n-1
           jp1=1

      fm=burger(w0[jm1],w0[j])
      fp=burger(w0[j],w0[jp1])

      RHS[i]=lamd*(fp-fm)
   beta=np.linalg.solve(EIM_fun[mgc_pts,:],RHS[mgc_pts])
   RHS= np.dot(EIM_fun,beta)
   RHS_alp=so.solve(minimization,np.shape(RHS)[0],np.shape(RB)[1],RB,RHS)
   w1_alp=w0_alp -RHS_alp
   #print "dt    ==== ",dt
   return w1_alp, dt,RHS
#############################################
def diffSolve_RB_EIM_proj(dx,CFL,w0,w0_alp,w1_alp,n,mgc_pts, EIM_fun_RB,EIM_inv, magic_M1):
# on resoud le schema machin
   umax=maximum_vitesse(w0,n)
   RHS=np.zeros(n)
   lamd=CFL/max(umax,1.e-5)
   dt=CFL*dx/max(umax,1.e-5)
   for i in mgc_pts:
      if i>0 :
        if n-1>i:
           jm1=i-1
           j  = i
           jp1=i+1
        else: #i=n-1
           j=n-1
           jm1=n-2
           jp1=0           
      else:
           j=0
           jm1=n-1
           jp1=1
           
      fm=burger(w0[jm1],w0[j])
      fp=burger(w0[j],w0[jp1])
      
      RHS[i]=lamd*(fp-fm)
   RHS_M1= np.zeros((np.shape(magic_M1)[0],1)) 
   k=0     
   for i in magic_M1:
      if i>0 :
        if n-1>i:
           jm1=i-1
           j  = i
           jp1=i+1
        else: #i=n-1
           j=n-1
           jm1=n-2
           jp1=0           
      else:
           j=0
           jm1=n-1
           jp1=1

      fm=burger(w0[jm1],w0[j])
      fp=burger(w0[j],w0[jp1])

      RHS_M1[k,0]=lamd*(fp-fm)
      k +=1

   RHS_alp_EIM=np.dot(EIM_inv,np.reshape(RHS[mgc_pts],(np.shape(mgc_pts)[0],1)))
   RHS_alp =np.dot(EIM_fun_RB,RHS_alp_EIM)

   w1_alp=w0_alp -RHS_alp
   
   #print "dt    ==== ",dt
   return w1_alp, dt,RHS_alp, RHS_M1
#############################################



def diffSolve2D(dx,CFL,w0,w1,n,disc):
# on resoud le schema machin
   umax=maximum_vitesse(w0,n)
   
   lamd=CFL/max(umax,1.e-5)
   dt=CFL*dx/max(umax,1.e-5)
   for i in range(n):
      if i>0 :
        if i<n-1:
           jm1=i-1
           j  = i
           jp1=i+1
        else:
           jm1=i-1
           j  = i
           jp1=i
        fm=burger(w0[jm1],w0[j])
        fp=burger(w0[j],w0[jp1])
        w1[i]=w0[i] -lamd*( fp-fm)#+dt*0.02*math.exp(disc*i*dx)
      else:
        w1[0]=w0[0]#+dt*0.02
#   print "dt    ==== ",dt
   return w1, dt
###################################################
def burger(a,b):
   flux=0.5*( 0.5*(a*a+b*b)-abs(a+b)*0.5*(b-a))
   return flux
def maximum_vitesse(w,n):
# I compute the maximum possible speed for all the possible samples. 
# doing so, I am sure that all the samples have the same chronology.
# otherwise, the time stepping relies on the CFL only, so we do not
# have the same chronology
    maxi=0.
    for i in range(n):
      maxi=1.#max(maxi,abs(w[i]))
    return maxi
###########################################
