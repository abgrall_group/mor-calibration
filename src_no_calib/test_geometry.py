#from geometry import *
from geometry import *
#from tests import *
import numpy as np
import matplotlib.pyplot as plt

n=50

extras = ExtraGeometry(5)
extras.set_original_domain(n)

geom2=ExtraGeometry(2)
geom2.set_original_domain()

geom3=ExtraGeometry(2)
geom3.set_reference_domain()
geom4=ExtraGeometry(2)
geom4.set_reference_domain(np.linspace(0,1,101))



x=extras.x

a= extras.init_point
b= extras.end_point

cal=0.6

U=(x-a)**2.*(x<cal)-(x>=cal)*(x-b)**2.




print("size domain", extras.size_domain)

y=transform_or2ref(x,cal,extras)

print("transformed x ", y)
print("min max ", y.min(), y.max())

yy=np.linspace(0,1, 55)[:-1]

VV=interpolate_or2ref(x,yy,cal, U, extras)

UU=interpolate_ref2or(yy,x,cal,VV, extras)

plt.plot(x, U, label="u")
plt.plot(yy,VV, label ="v")
plt.plot(x,UU, label="reconstructed uu")
plt.legend()
plt.show()


calib_ind=detect_calib_ind(U, "max")

print("manual calib, automated")
print(cal, x[calib_ind] )
