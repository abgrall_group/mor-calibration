__all__=["proj_on_RB","POD", "offline_step", "RBStructure", "L2_error"]

import numpy as np
import random
import matplotlib.pyplot as plt
from numpy import linalg as LA
import shutil, os
import time
import math
import solve as so
from bibli import *
from minL1 import *
from GS import *
from EIM import *
from geometry import *
from RB_parameters import *
from joblib import Parallel, delayed
import multiprocessing

class RBStructure:
  def __init__(self,physical, Greedy_param = None, POD_param = None):
    self.physical = physical
    if Greedy_param is None:
      self.greedy_param = GreedyParameters()
    else:
      self.greedy_param = Greedy_param
    if POD_param is None:
      self.POD_param = PODParameters()
    else:
      self.POD_param = POD_param
    self.RB = np.zeros((self.physical.geometry.n_dofs,0))
    self.dim = np.shape(self.RB)[1]
    self.dimensions = []
    self.true_err = 100.
    self.err_ind = 100.
    self.final_err = 100.
    self.avg_err = 100.
    self.final_avg_err = 100.
    self.error_for_tolerance = 100.
    self.errors = []
    self.consecutive_discards = 0
    self.previous_good_error_step = 0
    self.greedy_iterations = 0
    self.chosen_param = []
    self.second_choice = []
    self.param_dict = []

  def project_EIM_on_RB(self,EIM, minimization):
    num_cores = min(multiprocessing.cpu_count(),50)
    EIM_fun_RB=Parallel(n_jobs=num_cores) (delayed(proj_on_RB) (minimization, self, EIM.EIM_fun[:,i]) for i in range(EIM.EIM_dim))
    self.EIM_fun_RB = np.transpose(EIM_fun_RB)

  def init_RB(self, U):
    POD_basis, M1 = POD(U.U,self.POD_param.tol_POD_1)
    self.dim = M1
    self.dimensions.append(self.dim)
    self.RB  = np.concatenate([self.RB,gs(POD_basis)], axis = 1)

  def add_RB(self,U):
    RB_add,M  = POD(U,self.POD_param.tol_POD_new)
    RB_partial  = np.concatenate([self.RB,gs(RB_add)], axis = 1)
    new_RB, M_temp =POD(RB_partial, self.POD_param.tol_POD_2)
    print ("reduced the RB basis from ", self.dim+int(M), " to ", M_temp)
    self.dim = int(M_temp)
    self.dimensions.append(self.dim)
    self.RB = new_RB

  def save_RB(self, fname):
    RBfname = fname+'RB/'
    os.system('mkdir '+RBfname)
    np.save(RBfname+'RB.npy', self.RB)
    np.save(RBfname+'EIM_fun_RB.npy',self.EIM_fun_RB)

  def load_RB(self, fname):
    RBfname = fname+'RB/'
    try:
      self.EIM_fun_RB = np.load(RBfname+'EIM_fun_RB.npy')
      self.RB = np.load(RBfname+'RB.npy')
      self.dim = np.size(self.RB, 1)
    except:
      raise ValueError('RB files not found')

  def plot_all(self, stop=False):
    for k in range(self.dim):
      plt.plot(self.RB[:,k], label = str(k))
    plt.title('RB')
    plt.legend()
    plt.show()
    # if stop==False:
    #   time.sleep(5.)
    #   plt.close()

def proj_on_RB(minimization, RB,fun):
	tmp=so.solve(minimization,RB.physical.geometry.n_dofs, RB.dim,RB.RB,fun)
#	print i," function projected"
	return np.reshape(np.array(tmp),(RB.dim))


def POD(UU, tol):
	eigs,eigv	  	  = np.linalg.eig(np.dot(np.transpose(UU),UU))
	#sort the eigenvalues
	idx = eigs.argsort()
	idx = idx[::-1]
	eigs = eigs[idx]
	eigv = eigv[:,idx]
	#remove complex parts
	eigs = np.real(eigs)
	eigv = np.real(eigv)
	#sum of eigenvalues
	tot = np.sum(eigs)
	#compute how many POD modes to have in the basis =>> M1
	M1=0
	x1=0
	while (x1 < 1-tol):
		x1 +=eigs[M1]/tot
		M1+=1
	print ( "elem in the basis=",M1)
	#consider only the first M1 eigenvalues and eigenvectors
	eigs = eigs[:M1]
	eigv = eigv[:,:M1]
	#fill the reduced basis with the first M1 POD modes

	POD_basis =	np.dot(UU,eigv)
	return POD_basis, int(M1)

def L2_error(physical, real_sol, ROM_sol):
  sum_pts = np.sqrt( np.sum((real_sol-ROM_sol)**2.*physical.geometry.dx))
  return sum_pts

def offline_step(cand, mu, w1_init_alpha, RB, EIM, physical, real_sol, plot_flag, minimization, with_calibration, physical_or=None):
  if physical_or is None and with_calibration==True:
    raise ValueError('If with calibration, offline_step needs physical_ref and (physical_or at the end)')
  # print ( ["MU====",cand] )
  # First find the reduced initial condition, i.e the $L^1$ projection of
  # the initial condition onto the current dictionnary
  # That is the dictionary of size count_ndisc !!
  w1_alpha=[]

  #	w1_alpha=np.reshape(w1_init_alpha[:,cand],(count_ndisc,1))
  w1_alpha=np.reshape(w1_init_alpha,(RB.dim))

  # From the coordinates on the reduced space, build the full reduced initial condition
  if 1==1: #it == physical.Nt:
    w_ROM=np.dot(RB.RB, w1_alpha)
  else:
    w_ROM=np.zeros((physical.geometry.n_dofs));
    w_ROM[EIM.all_magic_points]=np.dot(RB.RB[EIM.all_magic_points, :], w1_alpha)

#  w_ROM=np.dot(RB.RB, w1_alpha)

#  w_ROM  = np.zeros((physical.geometry.n_dofs));
#  for k in range(RB.dim):
#	  w_ROM[:]  = w_ROM[:] + w1_alpha[k]*RB.RB[:,k];

  err_ind=0.
  true_err=0.
  err_ind += physical.CE**(physical.Nt)*L2_error(physical,real_sol.U[:,0], w_ROM)
  true_err += L2_error(physical,real_sol.U[:,0], w_ROM)/physical.Nt
  ##  reduced_manifold.reduced_sol[cand].set_solution(0, w1_alpha)

  ##Now, we run our time dependent scheme
  for it in range(1,physical.Nt+1):
    w0    	= np.copy(w_ROM);
    w0_alpha	= np.copy(w1_alpha);

    # Use our numerical scheme to compute  w_{rom}(t^{n+1}) = F(w_{rom}(t^{n})

    if with_calibration==True:
      w1_alpha, RHS_alpha, RHS_M1_alpha = diffSolve_RB_EIM_proj_ref(physical, physical_or, w0, w0_alpha, EIM, RB, mu, real_sol.calib_values[it-1], real_sol.calib_values[it])
    else:
      w1_alpha, RHS_alpha, RHS_M1_alpha = diffSolve_RB_EIM_proj(physical, w0, w0_alpha, EIM, RB, mu)

    ##    reduced_manifold.reduced_sol[cand].set_solution(it, w1_alpha)

    # From the coordinates on the reduced space, build the full reduced initial condition
    if 1==1: #it == physical.Nt:
        w_ROM=np.dot(RB.RB, w1_alpha)
    else:
        w_ROM=np.zeros((physical.geometry.n_dofs));
        w_ROM[EIM.all_magic_points]=np.dot(RB.RB[EIM.all_magic_points, :], w1_alpha)

  #  w_ROM=np.dot(RB.RB, w1_alpha)

  #  w_ROM  = np.zeros((physical.geometry.n_dofs));
  #  for k in range(RB.dim):
  #	  w_ROM[:]  = w_ROM[:] + w1_alpha[k]*RB.RB[:,k];

    if sum(np.isnan(w_ROM))>0:
        return[ 100., 100., 100.]


    ##		if ((calibs_ind[cand,it]-calibs_ind[cand,it-1])!=0 and with_calibration==True):
    ##			with_shift=1
    ###				print 'ciao', calibs_ind[cand,it]-calibs_ind[cand,it-1]
    ##      w1_0=np.copy(w_ROM)
    ##			w1_0=interpolate(calibs_ind[cand,it]-calibs_ind[cand,it-1], w1_0, with_calibration)
    ##			w1_alpha=np.reshape(so.solve(minimization,n,count_ndisc,RB[:,:count_ndisc],w1_0),(count_ndisc,1))
    ##			for j in range(count_ndisc):
    ##				w_ROM[:] = w_ROM[:]+ w1_alpha[j]*RB[:,j];


    if it in [1,physical.Nt-1] and plot_flag==1:
    #			mario = np.zeros(n)
    #			rino = np.zeros(n)
    #			for s in range(len(EIM_fun_RB[j][0])):
    #				rino = rino + RHS_EIM_coef[j][s]*EIM_fun[j][:,s]
    #			for k in range(count_ndisc[j]):
    #				mario = mario - (w1_alpha[j][k]-w0_alpha[j][k])*RB[j,:,k]
    #				plt.plot(w_ROM[j,:], label="ROM slt")
    #				plt.plot(real_sol.U[j,:,it,cand]/20000.,label="real slt")
    #				plt.plot(w0[0]/20000., label="previous iter rho")
    #				plt.plot(w0[1]/20000., label="previous iter vel")
    #				plt.plot(w0[2]/20000., label="previous iter ener")
    #				plt.plot(rino, label="flux EIM")
    #				plt.plot(mario, label="rom flux");
    #				plt.plot(flux_array[j,:,it-1,cand], label="flux")
      w_ROM=np.dot(RB.RB, w1_alpha)
      plt.plot(w_ROM[:],label='rb'+str(cand))
      plt.plot(real_sol.U[:,it],label='real'+str(cand))
      #				#					plt.plot(magic_pts[j], rino[magic_pts[j]], 'o')
      plt.title(str(it)+",cand "+str(cand))

      plt.legend()
      plt.show(block=False)
      time.sleep(cand/2.)
      plt.close()


    #		err_ind=0.
    #		true_err = 0.
    #		print(np.shape(RHS_M1),np.shape(RB[magic_M1,:int(count_ndisc)]), np.shape(RHS_alp))
    tmp=np.dot(RB.RB[EIM.magic_M1,:int(RB.dim)],RHS_alpha)
    tmp2=(tmp-np.reshape(RHS_M1_alpha,np.shape(tmp)))*np.reshape(EIM.norm_fnc_M1,np.shape(tmp))
    err_ind  += physical.CE**(physical.Nt-it)*np.linalg.norm(tmp2,2);#dt is already inside RHS !!! I don't need to put it!
    err_ind += physical.CE**(physical.Nt-it)*np.linalg.norm(w1_alpha-w0_alpha+RHS_alpha,1);
    #		err_loc=physical.CE**(physical.Nt-it)*np.linalg.norm(tmp2,2)
    #		err_loc2 = physical.CE**(physical.Nt-it)*np.linalg.norm(w1_alpha-w0_alpha+RHS_alpha,1);

    true_err += L2_error(physical,real_sol.U[:,it], w_ROM)/physical.Nt

    if np.isnan(err_ind)==True:
      err_ind= (2. + np.random.rand())*RB.err_ind*physical.Nt/it
    if np.isnan(true_err)==True:
      true_err= (2. + np.random.rand())*RB.true_err*physical.Nt/it
  #	if true_err > max_err_ind:
  #		np.savez("data/vector_wrong"+str(cand)+".npz", w_ROM, real_sol.U[:,:,it], w1_alpha, RB, EIM_fun_RB )
  #		print "something went wrong, saved things to trace"

  final_err  = L2_error(physical,real_sol.U[:,it], w_ROM)

  #  reduced_manifold.reduced_sol[cand].set_true_error(true_err)
  #  reduced_manifold.reduced_sol[cand].set_error_ind(err_ind)
  #  reduced_manifold.reduced_sol[cand].set_final_error(final_err)


  return [err_ind,true_err, final_err]
