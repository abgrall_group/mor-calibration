import numpy as np
import random
from RB_functions import *
import matplotlib.pyplot as plt
from numpy import linalg as LA
import shutil, os
import math
import solve as so
from bibli import *
from minL1 import *
from GS import *
from EIM import *
from joblib import Parallel, delayed
import multiprocessing
import time
from geometry import *
import pickle
import copy
from parameters import *
from mollifier import *

ic_type=0
fname='test'+str(ic_type)+'/'
os.system('mkdir '+fname)
folder_calibs = fname+'calibs'
os.system('mkdir '+folder_calibs)

extras = ExtraGeometry(ic_type)
extras.set_original_domain()
with_calibration=True
geometry=extras

physical = PhysicalProblem(extras, ic_type)

param = ParameterDomain(ic_type, 500)
physical.set_dt(param)


CE=1.+physical.dt/2. #lambda dt: 1.+dt/2.
physical.set_CE_error(CE)

if with_calibration  == True:
  geometry_ref = ExtraGeometry(ic_type)
  geometry_ref.set_reference_domain()
  physical_ref = PhysicalProblem(geometry_ref, ic_type)
  physical_ref.set_Nt(physical.get_Nt())
  physical_ref.set_CE_error(CE)

print( "final time=" , physical.T, ", dx=", np.mean(geometry.dx), ", dt=", physical.dt, ", Nt=", physical.Nt)

original_manifold = SolutionManifold(param, physical, False)
original_manifold.build_all_solutions(True) #parallel True


calibs=np.zeros((physical.Nt+1, param.n_param))
for k in range(param.n_param):
    calibs[:,k]=original_manifold.real_sol[k].calib_values[:]

with open(folder_calibs+'/calib_points','wb') as problem:
  pickle.dump(param.param_domain, problem)
  pickle.dump(physical,problem)
  pickle.dump(calibs,problem)

#smoothening
for k in range(param.n_param):
  mollify_calibs(original_manifold.real_sol[k])

calibs=np.zeros((physical.Nt+1, param.n_param))
for k in range(param.n_param):
    calibs[:,k]=original_manifold.real_sol[k].calib_values[:]

with open(folder_calibs+'/calib_points_mollified','wb') as problem:
  pickle.dump(param.param_domain, problem)
  pickle.dump(physical,problem)
  pickle.dump(calibs,problem)

# To load the calibration points of the parameter domain
# with open(folder_calibs+'/calib_points_mollified', 'rb') as problem:
#    param_domain = pickle.load(problem)
#    physical = pickle.load(problem)
#    calibs = pickle.load(problem)
