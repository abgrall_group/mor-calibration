import numpy as np
import random
from cvxopt import matrix
import matplotlib.pyplot as plt
from numpy import linalg as LA
import shutil, os
import math
import solve as so
from bibli import *
from minL1 import *
from GS import *
from EIM import *

def init():
	###########################################
	###Choose the desired minimization type
	global minimization
	#minimization = 'L1min_LP'
	#minimization = 'L1min_IRLS'
	#minimization  = 'Hubermin_IRLS'
	minimization = 'L2'
	#minimization  = 'Hull'
	
	###########################################
	global tol_main, tol_EIM, tol_start, tol_greedy, max_it_EIM, M1_EIM, EIM_import, load_real_solution
	## Greedy parameters
	## Given tolerance
	tol_main=1.e-2
	tol_start=1.e-2
	tol_EIM=1.e-5
	tol_greedy=1.e-4
	max_it_EIM=100
	M1_EIM=8
	EIM_import = True#False#
	load_real_solution = True#False#

	###############################################################################
	## DEFINE PDE Problem Parameters !

	#Size of truth approximation space : \mathcal{N}
	global truthN, CFL, Nt, CE,  dx, dt, x
	truthN = 1000;
	# CFL
	CFL    = 0.5;
	# Number of time steps !
	Nt     = 50; #max nb of iterations
	# Final time
	#T      = 0.1;

	CE=2.


	# Define \Omega : [0, \pi]
	x      = np.linspace(0.,1.,truthN)*math.pi;
	dx     = x[1]-x[0];
	dt     = 0.;


	###############################################################################
	# Parameter space now !
	# Define the fine parameter space
	global interval_min, interval_max, max_nb_cand, ndisc, new_dict, disc_init, RB, param_domain
	global fluxes, real_sol, true_err, err_ind, count_ndisc
	global EIM_fun_RB,EIM_inv, magic_M1, EIM_fun, magic_pts, fnc_M1

	interval_min=0.2
	interval_max=0.8
	param_domain = np.arange(interval_min,interval_max+0.025,0.05)
	param_domain=np.array(param_domain)
	print param_domain

	max_nb_cand  = len(param_domain)
	print max_nb_cand


	## How big will our final basis be ?
	ndisc=200 ##max can be max_nb_cand*(Nt+1)


	## Initialize our dictionary !
	new_dict    = np.zeros(ndisc)
	disc_init   = [0.45]

	new_dict[0] = disc_init[0]

	RB	  = np.zeros((truthN,ndisc))

	fluxes=np.zeros((truthN,max_nb_cand*(Nt+1))) #fluxes for EIM
	real_sol = np.zeros((truthN,Nt+1,max_nb_cand));



