## Greedy alg for Burgers eq.
## plot the err indicator, L2 truth error and L2 average error
## plot greedy coef
## plot dictionary members
## this is used for the paper =>>  param from greedy disc_locs= [0.45, 0.2, 0.7, 0.35, 0.6, 0.55, 0.3]

import numpy as np
import random
from RB_functions import *
import matplotlib.pyplot as plt
from numpy import linalg as LA
import shutil, os
import math
import solve as so
from bibli import *
from minL1 import *
from GS import *
from EIM import *
from joblib import Parallel, delayed
import multiprocessing
import time
from geometry import *
import pickle
import copy
from parameters import *
from mollifier import *

verbose=0
with_calibration=False


###########################################
###Choose the desired minimization type
#minimization = 'L1min_LP'
#minimization = 'L1min_IRLS'
#minimization  = 'Hubermin_IRLS'
minimization = 'L2'
#minimization  = 'Hull'

###########################################
## Greedy parameters
EIM_import = False#True#False#True#False#
load_real_solution =  False#True#True# False#

###############################################################################
## DEFINE PDE Problem Parameters !
ic_type=1 #test case
fname='test'+str(ic_type)+'/'
os.system('mkdir '+fname)

if load_real_solution == False:
  geometry = ExtraGeometry(ic_type)
  geometry.set_original_domain()

  physical = PhysicalProblem(geometry, ic_type)

  # Parameter space now
  max_nb_cand = 50
  param = ParameterDomain(ic_type, 200, "uniform")
  physical.set_dt(param)
  param = ParameterDomain(ic_type, max_nb_cand, "uniform")

  CE=1.+physical.dt/2. #lambda dt: 1.+dt/2.
  physical.set_CE_error(CE)

  if with_calibration  == True:
    geometry_ref = ExtraGeometry(ic_type)
    geometry_ref.set_reference_domain()
    physical_ref = PhysicalProblem(geometry_ref, ic_type)
    physical_ref.set_Nt(physical.get_Nt())
    physical_ref.set_CE_error(CE)

  print( "final time=" , physical.T, ", dx=", np.mean(geometry.dx), ", dt=", physical.dt, ", Nt=", physical.Nt)

  with open(fname+'problem_data','wb') as problem:
    pickle.dump(with_calibration, problem)
    pickle.dump(geometry,problem)
    if with_calibration  == True:
      pickle.dump(geometry_ref,problem)
    pickle.dump(physical,problem)
    if with_calibration  == True:
      pickle.dump(physical_ref,problem)

  #Build real solutions
  original_manifold = SolutionManifold(param, physical, False)
  original_manifold.build_all_solutions(True) #parallel True
  folder_calibs = fname+'calibs'
  os.system('mkdir '+folder_calibs)
  calibs=np.zeros((physical.Nt+1, param.n_param))
  for k in range(param.n_param):
      calibs[:,k]=original_manifold.real_sol[k].calib_values[:]
  with open(folder_calibs+'/calib_points','wb') as problem:
    pickle.dump(param.param_domain, problem)
    pickle.dump(physical,problem)
    pickle.dump(calibs,problem)

  for sol in original_manifold.real_sol:
      mollify_calibs(sol)

  calibs=np.zeros((physical.Nt+1, param.n_param))
  for k in range(param.n_param):
      calibs[:,k]=original_manifold.real_sol[k].calib_values[:]

  with open(folder_calibs+'/calib_points_mollified','wb') as problem:
    pickle.dump(param.param_domain, problem)
    pickle.dump(physical,problem)
    pickle.dump(calibs,problem)

  if with_calibration  == True:
    reference_manifold = SolutionManifold(param, physical_ref, True)
    reference_manifold.build_all_solutions(True, physical, original_manifold)
  print('writing solutions')
  with open(fname+'real_sol.npy', 'wb') as real_sol:
    pickle.dump(original_manifold, real_sol)
    if with_calibration  == True:
      pickle.dump(reference_manifold, real_sol)
    pickle.dump(param, real_sol)
  print('finished')

else:
  with open(fname+'problem_data', 'rb') as problem:
    with_calibration = pickle.load(problem)
    geometry = pickle.load(problem)
    if with_calibration  == True:
      geometry_ref = pickle.load(problem)
    physical = pickle.load(problem)
    if with_calibration  == True:
      physical_ref = pickle.load(problem)
  print("Loading real solutions")
  with open(fname+'real_sol.npy', 'rb') as real_sol:
    original_manifold = pickle.load(real_sol)
    if with_calibration  == True:
      reference_manifold = pickle.load(real_sol)
    param = pickle.load(real_sol)
  print("Finished loading solutions")
## How big will our final basis be ?

# for cand in range(0,param.n_param, 3):
#     plt.plot(original_manifold.real_sol[cand].U[:,-1])
# plt.title("original solutions")
# plt.show()

# if with_calibration:
#     for cand in range(0,param.n_param, 3):
#         plt.plot(reference_manifold.real_sol[cand].U[:,-1])
#     plt.title("reference solutions")
#     plt.show()

ndisc=200 ##max can be max_nb_cand*(Nt+1)

param_dict    = [] #np.zeros((ndisc,param.dim_param))
param_dict.append( param.pick_parameter_inside())

plot_flag=np.zeros(param.n_param,dtype="int")
plot_flag[12]=0
###############################################################################

# Thats it, our dictionary has M1 POD modes for the initial starting param \mu=0.45!




##### Build Real solutions !
###if (load_real_solution==False):
###	fluxes=np.zeros((truthN,max_nb_cand*(Nt+1))) #fluxes for EIM
###	real_sol = np.zeros((truthN,Nt+1,max_nb_cand));
###	fluxes_not_cal=np.zeros((truthN,max_nb_cand*(Nt+1))) #fluxes for EIM
###	real_sol_not_cal = np.zeros((truthN,Nt+1,max_nb_cand));
###	for cand in range(max_nb_cand):
###		print( "computing solution for cand " + str(cand) +" of total " +str(max_nb_cand) )
###		u0 ,calib_index    = createInitialCondition(truthN,param_domain[cand],x, with_calibration);
###		calibs_ind[cand,0] = calib_index

###		U, U_flux, calib_index= solveFOM(truthN,Nt,u0,dx,dt, with_calibration, param_domain[cand]);
###		calibs_ind[cand,1:]=calib_index
###		calibs[cand,:]=x[calibs_ind[cand,:]]
###		real_sol_not_cal[:,:,cand] = U;
###		fluxes_not_cal[:,cand*Nt:(cand+1)*Nt]=np.transpose(U_flux) #All fluxes for every timestep of this parameter
###		for it in range(Nt+1):
###			real_sol[:,it,cand]= interpolate(calibs_ind[cand,it],U[:,it], with_calibration) # U[(n+calibs_ind[cand,it])%truthN,it]
###			if it<Nt+1:
###				fluxes[:,cand*Nt+it] = interpolate(calibs_ind[cand,it],fluxes_not_cal[:,cand*Nt+it],with_calibration)
###	np.savez(fname+'real_sol',real_sol,fluxes, calibs_ind, calibs, real_sol_not_cal, fluxes_not_cal)
###else:
###	real_data=np.load(fname+'real_sol.npz')
###	real_sol=real_data['arr_0']
###	fluxes=real_data['arr_1']
###	calibs_ind = real_data['arr_2']
###	calibs = real_data['arr_3']
###	real_sol_not_cal = real_data['arr_4']
###	fluxes_not_cal = real_data ['arr_5']

#x=geometry.x
#for cand in [0,1,10]:#,34,50,64,75,80,99]:
#		for it in [0,100]:
#			U=original_manifold.real_sol[cand].U[:,it]
#			plt.plot(x,U, label="real")
#			x0=original_manifold.real_sol[cand].calib_values[it]
#			plt.plot(x0,U[np.argmin(x0-x)] , 'o', label="calib")

#plt.show()

#y=geometry_ref.x
#for cand in [0,1,10]:#,34,50,64,75,80,99]:
#		for it in [0,100]:
#			U=reference_manifold.real_sol[cand].U[:,it]
#			plt.plot(y,U, label="calibrated")
#plt.show()

#x=geometry.x
#for cand in [0,1,10]:#,34,50,64,75,80,99]:
#		for it in [0,100]:
#			U=original_manifold.EIM_fun[cand].U[:,it]
#			plt.plot(x,U, label="real")
#			x0=original_manifold.real_sol[cand].calib_values[it]
#			plt.plot(x0,U[np.argmin(x0-x)] , 'o', label="calib")

#plt.show()

#y=geometry_ref.x
#for cand in [0,1,10]:#,34,50,64,75,80,99]:
#		for it in [0,100]:
#			U=reference_manifold.EIM_fun[cand].U[:,it]
#			plt.plot(y,U, label="calib")
#plt.show()

## Let's go for the actual greedy algorithm !

max_nb_cand = 200
if (EIM_import==True) :
  if with_calibration:
    EIM = EIMStructure(geometry_ref)
  else:
    EIM = EIMStructure(geometry)
  EIM.load_EIM(fname)

else:
  selection = list(set(np.random.randint(0,np.shape(original_manifold.fluxes)[1], size=(physical.Nt+1)*(max_nb_cand/100) )))
  if with_calibration:
    EIM = EIMStructure(geometry_ref)
    EIM.compute_EIM( reference_manifold.fluxes[:,selection])
  else:
    EIM = EIMStructure(geometry)
    EIM.compute_EIM( original_manifold.fluxes[:,selection])
  EIM.save_EIM(fname)

EIM.compute_extras()
EIM.update_history()
EIM.compute_boundary_magic_points()


#all_points=range(n)#list(set(real_magic_elem).union(set(real_magic_M1)))#

if with_calibration == True:
  RB = RBStructure(physical_ref)
else:
  RB = RBStructure(physical)


#calibs_ind=np.zeros((max_nb_cand,Nt+1),dtype=np.int) ##index of shifting
#calibs=np.zeros((max_nb_cand,Nt+1))  #point of shifting

consecutive_discards=0
#err_ind	= np.ones(max_nb_cand);


num_cores = min(multiprocessing.cpu_count(),30)

##### Perform POD on the snapshots previously computed UU, and store the first
    #  POD modes in the basis functions matrix.
    #  Input arguments are: inner product matrix UU^T * UU and tol
    #  Output arguments are: POD modes, number of POD modes


## Firstly compute the full solution for \mu in disc_init
u_original = solveFOM(physical, param_dict[0])
mollify_calibs(u_original)
calibs  = u_original.calib_values
if with_calibration == True:
  u = solveFOM_ref(physical_ref, physical, param_dict[0], calibs, physical_ref.geometry.x)
else:
  u = u_original

if verbose==1:
  for it in [0, int(physical.Nt/3), physical.Nt]:
    plt.plot(physical.geometry.x,u.U[:,it], 'r', label='calibrated')
    if with_calibration == True:
      plt.plot(physical_ref.geometry.x, u_original.U[:, it], 'b', label='original function')
    plt.legend()
    plt.title('First parameter, timestep =' +str(it))
    plt.show()

#initialization of variables
count_M1=-1; count_ndisc	= 0;
dim_basis_func=[]; t_err=[]; avg_err=[]; max_err_ind=[]; f_err=[]; f_avg_err=[]
dims_EIM=[]
###### Start the POD process for first chosen param
###POD_basis, M1 = POD(U,tol_POD_1)
###RB[:,0:M1]	  = gs(POD_basis)
###previous_good_error=count_M1
####count_ndisc=count_ndisc+int(M1)
RB.init_RB(u)


it_greedy=0
while (RB.dim<geometry.n_dofs and it_greedy<=param.n_param and RB.error_for_tolerance>RB.greedy_param.tol_greedy ):
  it_greedy+=1

#  if with_calibration:
#    reduced_manifold = ReducedManifold(param, physical_ref, RB.dim)
#  else:
#    reduced_manifold =  ReducedManifold(param, physical, RB.dim)
#  reduced_manifold.initialize_reduced_solutions()


  print( "#########################")
  print( "RB dimensions====",RB.dim)
  print( "#########################")
  # Initialize our error indicator !

  #	Projection of EIM onto RB space
  print ("#####################")
  print ("Projecting EIM basis onto RB basis")
  print ("#####################")

#	for i in :
  RB.project_EIM_on_RB(EIM, minimization)

#	EIM_fun_RB=Parallel(n_jobs=num_cores)(delayed(proj_EIM_fun)(i, minimization, truthN, count_ndisc, np.array(RB[:,:count_ndisc]), np.array(EIM_fun[:,i])) for i in range(EIM_dim))
#	EIM_fun_RB = np.transpose(np.array ( EIM_fun_RB ))

#	for n in range(count_ndisc):
#		plt.plot(EIM_fun_RB[:,n])
#	plt.show()

  if verbose==1 and consecutive_discards>0:
    for k in range(RB.dim):
      plt.plot(RB.RB[:,k], label=str(k))
    plt.title('RB')
    plt.legend()
    plt.plot()
    plt.show()
    for k in range(RB.dim,RB.dim+4):
      plt.plot(RB.RB[:,k], label=str(k))
    plt.legend()
    plt.title('RB')
    plt.plot()
    plt.show()


  # We go trough our fine parameter space
  print ('EIM nan', np.isnan(RB.EIM_fun_RB).any())
  #w_init_alpha shape (count_ndisc, max_nb_cand)
  if with_calibration:
    w_init_alpha = Parallel(n_jobs=num_cores)(delayed(proj_on_RB)(minimization,RB, reference_manifold.real_sol[cand].U[:,0]) for cand in range(param.n_param))
  else:
    w_init_alpha = Parallel(n_jobs=num_cores)(delayed(proj_on_RB)(minimization,RB, original_manifold.real_sol[cand].U[:,0]) for cand in range(param.n_param))

  w1_init_alpha = np.transpose(np.array(w_init_alpha))
  print ( np.shape(w1_init_alpha) )

  print ("#####################")
  print ("Greedy step")
  print ("#####################")

  if with_calibration==True:
    errors_new= Parallel(n_jobs=num_cores)(delayed(offline_step)(cand, param.param_domain[cand], w1_init_alpha[:,cand], RB, EIM, physical_ref, reference_manifold.real_sol[cand], plot_flag[cand], minimization, with_calibration, physical) for cand in range(param.n_param))
  else:
    errors_new= Parallel(n_jobs=num_cores)(delayed(offline_step)(cand, param.param_domain[cand], w1_init_alpha[:,cand], RB, EIM, physical, original_manifold.real_sol[cand], plot_flag[cand], minimization, with_calibration) for cand in range(param.n_param))

  err_ind=[]
  true_err=[]
  final_errors=[]
  for mu in range(param.n_param):
    err_ind.append(errors_new[mu][0])
    true_err.append(errors_new[mu][1])
    final_errors.append(errors_new[mu][2])

  # PICK the worst parameter
  sorted_list= np.argsort(true_err) #increasing order
  RB.chosen_param		= sorted_list[-1]
  RB.second_choice = sorted_list[-2]
  param_dict.append( param.param_domain[RB.chosen_param])
      #take the maximum error ind in L1 and truth error in L2 and average error in L2
  RB.error_for_tolerance =  max(true_err)#max(err_ind);#
  RB.err_ind = max(err_ind)
  max_err_ind.append(RB.err_ind)
  RB.true_err = max(true_err)
  t_err.append(RB.true_err)
  RB.final_err = max(final_errors)
  f_err.append(RB.final_err)
  RB.final_avg_err = sum(final_errors)/param.n_param
  f_avg_err.append(RB.final_avg_err)
  RB.avg_error= sum(true_err)/param.n_param
  avg_err.append( RB.avg_error)
  dim_basis_func.append(RB.dim)
  dims_EIM.append(EIM.EIM_dim)
  count_M1+=1

  # print error
  print ("max_err_ind",max_err_ind[-1])
  print ("max_true_err",t_err[-1])
  print ("avg_err",avg_err[-1])
  print ("max final err",f_err[-1])
  print ("at position",RB.dim,"chosen_param=",param.param_domain[RB.chosen_param]);

  if RB.error_for_tolerance < RB.greedy_param.tol_greedy:
    break


  ############ CHECK IF WE IMPROVED OR NOT THE REDUCED BASIS

  if (t_err[count_M1]< t_err[RB.previous_good_error_step] or it_greedy<4 or consecutive_discards>3):
    print ("NOT DISCARD")
    print ("previous good error", t_err[RB.previous_good_error_step], ", new err ", t_err[count_M1])
    RB.previous_good_error_step = count_M1
    consecutive_discards=0

    print ("#########################")
    print ("Computing real solution and projecting it on RB for MU=", param.param_domain[RB.chosen_param])
    print ("#########################")
    #	w_ROM_RB	= w_ROM[:,:,chosen_param]
    if with_calibration:
        w		= reference_manifold.real_sol[RB.chosen_param].U
    else:
        w = original_manifold.real_sol[RB.chosen_param].U

    w_ROM_RB = np.zeros(np.shape(w))
    #project the real_sol for the chosen param onto the reduced orthogonalized basis (in order not to consider the same values if the chosen_param will be choosen again)
    w_ROM_RB_alp=Parallel(n_jobs=num_cores)(delayed(proj_on_RB)(minimization, RB, w[:,it] ) for it in range(physical.Nt+1))
    w_ROM_RB_alp = np.transpose(np.array(w_ROM_RB_alp))
    print (np.shape(w_ROM_RB_alp))
    print (np.shape(RB.RB))
    for it in range(physical.Nt+1):
      for j in range(RB.dim):
        w_ROM_RB[:,it]  = w_ROM_RB[:,it] + w_ROM_RB_alp[j,it]*RB.RB[:,j];

  #	for it in range(Nt+1):
      # Project the real sol for chosen_param onto the reduced space
  #    alpha 	= so.solve(minimization,truthN,count_ndisc,RB[:,:count_ndisc],w[:,it]);
  #    for j in range(count_ndisc):
  #       w_ROM_RB[:,it]  = w_ROM_RB[:,it] + alpha[j]*RB[:,j];


    real_sol_proj = w-w_ROM_RB  ##proj in L1

    print ("############################")
    print ("Update the RB")
    print ("############################")

    RB_old=copy.deepcopy(RB)

    RB.add_RB(real_sol_proj)



  else:
    print ("DISCARD")
    print ("previous error", t_err[RB.previous_good_error_step], ", new err ", t_err[count_M1])
    consecutive_discards+=1
    RB = copy.deepcopy(RB_old)
    
  ################### EIM UPDATE #########################
  EIM.set_tol_EIM(1./physical.Nt* t_err[RB.previous_good_error_step]/100.)
  sel = [RB.chosen_param, RB.second_choice]
  selection = selection = [ i+physical.Nt*k for k in sel for i in range(physical.Nt)]
  # #remember to put fluxes[j,:,selection].T with transposition if you want to put a selection
  if with_calibration:
    EIM.extend_EIM( reference_manifold.fluxes[:,selection])
  else:
    EIM.extend_EIM( original_manifold.fluxes[:,selection])

  EIM.compute_extras()
  EIM.update_history()
  EIM.compute_boundary_magic_points()
  EIM.save_EIM(fname)

  # RB.plot_all()
  if verbose==1:
    for k in range(count_ndisc+5):
      plt.plot(RB.RB[:,k],label=str(k))
    plt.legend()
    plt.show(block=False)
    time.sleep(1.)
    plt.close()

# np.save(fname+'RB',RB[:,:count_ndisc])

RB.project_EIM_on_RB(EIM, minimization)
RB.save_RB(fname)

# EIM_fun_RB =np.zeros((count_ndisc,EIM_dim))
#
# EIM_fun_RB=Parallel(n_jobs=num_cores)(delayed(proj_EIM_fun)(i, minimization, truthN, count_ndisc, np.array(RB[:,:count_ndisc]), np.array(EIM_fun[:,i])) for i in range(EIM_dim))
#
# EIM_fun_RB = np.transpose(np.array ( EIM_fun_RB ))

# np.save(fname+'EIM_fun_RB.npy',EIM_fun_RB)

# stop()
# if with_calibration==True:
# 	possible_shifts_abs=25; #how much will I shift the solution between a time step and another one (not a lot) in both directions
# 	tot_possible_shifts=possible_shifts_abs*2+1
# 	map_shifts=np.zeros(truthN, np.dtype(int)) #for a shift of i in range(truthN) correspond the index in map_shifts[i]
# 	proj_shift_basis_coeff=np.zeros((tot_possible_shifts,count_ndisc, count_ndisc)) #(shift,i,j) compute the coefficients j of the projection of basis_i(x-shift)
# 	proj_shift_basis=np.zeros((tot_possible_shifts,count_ndisc, truthN)) #(shift,i,j) compute the function j (x direction) of the projection of basis_i(x-shift)
# 	err_proj_shift_basis=np.zeros((tot_possible_shifts,count_ndisc)) #(shift,i) compute the error of the projection of basis_i(x)-basis_i(x-shift) with respect to the original
# 	for k in range(-possible_shifts_abs,possible_shifts_abs+1):
# 		map_shifts[k%truthN]=k+possible_shifts_abs
# 	for shift in [k%truthN for k in range(-possible_shifts_abs,possible_shifts_abs+1)]:
# 		for i in range(count_ndisc):
# 			basis=RB[:,i]
# 			shift_ind=map_shifts[shift%truthN]
# 			shifted_basis=interpolate(shift, basis, with_calibration)
# 			w1_alpha=np.reshape(so.solve(minimization,truthN,count_ndisc,RB[:,:count_ndisc],shifted_basis),(count_ndisc))
# 			proj_shift_basis_coeff[shift_ind, i,:]=w1_alpha;
# 			w_rom=np.zeros(truthN)
# 			for z in range(count_ndisc):
# 				w_rom=w_rom+w1_alpha[z]*RB[:,z]
# 			proj_shift_basis[shift_ind, i,:]=w_rom
# 			err_proj_shift_basis[shift_ind, i]=np.linalg.norm(w_rom-shifted_basis,2)/np.sqrt(truthN)
#
# 	shift_file=open(fname+'shift_basis_data.pkl', 'wb')
# 	pickle.dump(proj_shift_basis_coeff,shift_file)
# 	pickle.dump(proj_shift_basis,shift_file)
# 	pickle.dump(err_proj_shift_basis,shift_file)
# 	pickle.dump(map_shifts,shift_file)
# 	shift_file.close()

#for i in range(count_ndisc):
#	for shift in [k%truthN for k in range(-possible_shifts_abs,possible_shifts_abs+1)]:
#		basis=RB[:,i]
#		shifted_basis=inverse_interpolate(shift, basis, with_calibration) #check sign: inverse_interpolate or interpolate??
#		w_rom=np.zeros(truthN)
#		for z in range(count_ndisc):
#			w_rom=w_rom+proj_shift_basis_coeff[map_shifts[shift%truthN],i,z]*RB[:,z]
#		plt.plot(shifted_basis,label='true')
#		plt.plot(w_rom, label='rom')
#		plt.title('shift='+str(shift)+', basis='+str(i))
#		plt.legend()
#		plt.show()


## print the error indicator
plt.ion()
plt.figure()
plt.plot(dim_basis_func,[min(x,10.) for x in max_err_ind],'x-',label="Max error indicator")
plt.plot(dim_basis_func,[min(x,10.)  for x in t_err ],label="Max error")
plt.plot(dim_basis_func,[min(x,10.) for x in avg_err],label="Average error")
for k, dims in enumerate(EIM.EIM_dimensions): 
  plt.annotate('EIM='+str(dims), [dim_basis_func[k],min(max_err_ind[k],10.)])
plt.yscale('log')
# plt.ylim([min(avg_err_plot)*0.9, 10.])
plt.xlabel('Dimension Reduced Basis')
plt.ylabel('Error')
plt.title('Error of greedy algorithm')
plt.legend(loc=3)
plt.show()
if  with_calibration:
	plt.savefig(fname+'errors_calibration.pdf')
else:
	plt.savefig(fname+'errors_no_calibration.pdf')


#plt.figure()
#plt.plot(dim_basis_func[1:],max_err_ind[1:],'x-',label="Max error indicator")
#plt.plot(dim_basis_func[1:],f_err[1:],label="Max final error")
#plt.plot(dim_basis_func[1:],f_avg_err[1:],label="Average final error")
#plt.yscale('log')
#plt.xlabel('Dimension Reduced Basis')
#plt.ylabel('Error')
#plt.title('Error of greedy algorithm')
#plt.legend(loc=3)
#plt.show()
#if  with_calibration:
#	plt.savefig('final_errors_calibration.pdf')
#else:
#	plt.savefig('final_errors_no_calibration.pdf')

plt.figure()
plt.plot(EIM.EIM_dimensions,EIM.EIM_errors, '.-', label="EIM error")
plt.yscale('log')
plt.xlabel('Dimension EIM space')
plt.ylabel('Error')
plt.legend(loc=3)
plt.title("EIM error")
plt.show()
if  with_calibration:
	plt.savefig(fname+'errors_EIM_calibration.pdf')
else:
	plt.savefig(fname+'errors_EIM_no_calibration.pdf')
