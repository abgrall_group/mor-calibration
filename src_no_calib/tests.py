#test parameters
__all__=["GeometryParameters", "PhysicalParameters", "BasicParameters", "ParameterDomainBoundaries"]
import numpy as np

class GeometryParameters:
  def __init__(self,ic):
    self.basics = BasicParameters()
    self.ic_type = ic
    self.n = self.basics.n
    self.CFL = self.basics.CFL
    if self.ic_type in [0,1,2,3,90]:
      self.bc_type="periodic"
    elif self.ic_type in [4,5,6,91]:
      self.bc_type = "outflow"
    else:
      raise ValueError("ic_type not contained in GeometryParameters")

    if self.ic_type in [1,2]:
      self.init_domain = 0.
      self.end_domain = np.pi
    elif self.ic_type in [0,3,4,5,6,90,91]:
      self.init_domain = 0.
      self.end_domain = 1.
    else:
      raise ValueError("ic not valid for domain definition passed in GeometryParameters")

    if self.ic_type in [1, 2]:
      self.detec_type = "min"
    elif self.ic_type in [0,3,90]:
      self.detec_type = "max"
    elif self.ic_type in [4,5,6]:
      self.detec_type = "sign_change"
    elif self.ic_type in [91]:
      self.detec_type = "steepest"
    else:
      raise ValueError("ic not valid for detection passed in GeometryParameters")


class PhysicalParameters:
  def __init__(self,ic):
    self.ic_type = ic
    self.CFL = BasicParameters().CFL
    if self.ic_type in [1,2,3,4,5,6]:
      self.equation = "burgers"
    elif self.ic_type in [0,90,91]:
      self.equation = "advection"
    else:
      raise ValueError("ic not valid for equation passed in PhysicalParameters")

    if self.ic_type in [1,2]:
      self.T_fin = 2.
    elif self.ic_type in [0,3,4,5,6,90]:
      self.T_fin = 0.6
    elif self.ic_type in [91]:
      self.T_fin = 0.15
    else:
      raise ValueError("ic not valid for final time passed in PhysicalParameters")

  def initialCondition(self, x, mu):
    ic_type= self.ic_type
    if ic_type==0:  #mu_min =[0,-1., -1.]   mu_max = [2,1.,1.]
      if len(mu)!=3:
        raise ValueError("parameter doesn't match the ic_type "+str(ic_type) + ": mu =" +str(mu)  )
      u0 = np.exp(-(1000.+500.*mu[1])*(x-0.2+0.1*mu[2])**2.)
    elif ic_type==90:  #mu_min =[0,-1.]   mu_max = [2,1.]
      if len(mu)!=2:
        raise ValueError("parameter doesn't match the ic_type "+str(ic_type) + ": mu =" +str(mu)  )
      u0 = np.exp(-1000.*(x-0.2+0.1*mu[1])**2.)
    elif ic_type==91:  #mu_min =[0,-1., 0.3]   mu_max = [2,1.,0.7]
      if len(mu)!=3:
        raise ValueError("parameter doesn't match the ic_type "+str(ic_type) + ": mu =" +str(mu)  )
      u0 = mu[1]*(x<0.35+0.05*mu[2])
    elif ic_type==1:  #mu_min =[0,0.]   mu_max = [2,3.14]
      if len(mu)!=2:
        raise ValueError("parameter doesn't match the ic_type "+str(ic_type) + ": mu =" +str(mu)  )
      u0 = abs(np.sin(x+mu[1]))+0.1
    elif ic_type==2:  #mu_min =[0,0., -1.]   mu_max = [2,3.14,1.]
      if len(mu)!=3:
        raise ValueError("parameter doesn't match the ic_type "+str(ic_type) + ": mu =" +str(mu)  )
      u0 = abs(np.sin(x+mu[1]))+ mu[2]
    elif ic_type==3:  #mu_min =[0,0.5,0.1,0.6]   mu_max = [2,3,1,0.8]
      if len(mu)!=4:
        raise ValueError("parameter doesn't match the ic_type "+str(ic_type) + ": mu =" +str(mu) )
      u0 = -mu[1]*25.*(x-0.5)*(x-0.1)*(x<0.5)*(x>0.1)-mu[2]*400.*(x-mu[3]-0.05)*(x-mu[3]+0.05)*(x<mu[3]+0.05)*(x>mu[3]-0.05)
    elif ic_type==4:#mu_min =[0,0.5,-3,0.6]   mu_max = [2,3,-0.5,0.8]
      if len(mu)!=4:
        raise ValueError("parameter doesn't match the ic_type "+str(ic_type) + ": mu =" +str(mu)  )
      u0 = -mu[1]*100.*(x-0.5)*(x-0.3)*(x<0.5)*(x>0.3)-mu[2]*4./max((mu[3]-0.5)**2., 10.**-10.)*(x-0.5)*(x-mu[3])*(x<mu[3])*(x>0.5)
    elif ic_type==5:#mu_min =[0,0.5,-3,0.6]   mu_max = [2,3,-0.5,0.8]
      if len(mu)!=4:
        raise ValueError("parameter doesn't match the ic_type "+str(ic_type) + ": mu =" +str(mu)  )
      u0 = -mu[1]*100.*(x-0.5)*(x-0.3)*(x<0.5)*(x>0.3)-mu[2]*400.*(x-0.05-mu[3])*(x+0.05-mu[3])*(x<mu[3]+0.05)*(x>mu[3]-0.05)
    elif ic_type == 6:
      if len(mu)!= 4:
        raise ValueError("parameter doesn't match the ic_type "+str(ic_type) + ": mu =" +str(mu)  )
      u0 = np.sin(2.*np.pi*(x+0.1*mu[1]))*np.exp(-(60.+20.*mu[2])*(x-0.5)**2.)*(1.+0.5*mu[3]*x)
    else:
      raise ValueError("ic not defined for ic_type:"+str(ic_type))
    return u0

class BasicParameters:
  def __init__(self):
    self.n = 1000
    self.CFL = 0.3

class ParameterDomainBoundaries:
  def __init__(self, ic_type):
    if ic_type==0:
      self.mu_min = np.array([0.,-1.,-1.])
      self.mu_max = np.array([2., 1., 1.])
    elif ic_type==90:
      self.mu_min = np.array([0.,-1.])
      self.mu_max = np.array([2.,1.])
    elif ic_type==91:
      self.mu_min = np.array([0.,-1.,-1.])
      self.mu_max = np.array([2.,1., 1.])
    elif ic_type==1:
      self.mu_min = np.array([0.,0.])
      self.mu_max = np.array([2.,3.14])
    elif ic_type==2:
      self.mu_min = np.array([0.,0., -1.])
      self.mu_max = np.array([2.,3.14,1.])
    elif ic_type==3:
      self.mu_min =np.array([0.,0.5,0.1,0.6])
      self.mu_max = np.array([2.,3.,0.5,0.8])
    elif ic_type==4:
      self.mu_min =np.array([1.,0.5,-1.,0.55] )
      self.mu_max = np.array([1.5,1.,-0.5,0.6])
    # elif ic_type==4:
    #   self.mu_min =np.array([1.,0.2,-2.,0.55] )
    #   self.mu_max = np.array([1.5,2.,-0.1,0.75])
    elif ic_type==5:
      self.mu_min =np.array([1.,0.1,-1.5,0.55] )
      self.mu_max = np.array([1.5,1.5,-0.5,0.75])
    elif ic_type==6:
      self.mu_min =np.array([1.,-1.,-1.,-1.] )
      self.mu_max = np.array([1.5,1.,1.,1.])
    else:
      raise ValueError("min max parameters not defined for ic_type:"+str(ic_type))
