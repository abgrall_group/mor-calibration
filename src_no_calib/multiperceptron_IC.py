import numpy as np
import matplotlib.pyplot as plt
import keras
import pickle
import sklearn.model_selection
import pandas as pd
from bibli import *
from geometry import *
from ml_models import *

ic_type=1
fname='test'+str(ic_type)+'/'
folder_calibs = fname+'calibs'

with open(folder_calibs+'/calib_points', 'rb') as problem:   #_mollified
    param_domain = pickle.load(problem)
    physical = pickle.load(problem)
    calibs = pickle.load(problem)

times=np.arange(0.,physical.T+physical.dt, physical.dt)
[n_param, dim_param] = np.shape(param_domain)

x_dim = dim_param #time  +1
n_data = n_param  #(physical.Nt+1)*

for par in range(n_param):
    adjust_calibs=np.zeros(np.shape(calibs[:,par]))
    shift=0.
    thres=physical.geometry.size_domain/2.
    adjust_calibs[0]=calibs[0,par]
    for k in range(1,len(calibs)):
      if calibs[k,par]-calibs[k-1,par]>thres:
        shift=shift-physical.geometry.size_domain
      elif calibs[k,par]-calibs[k-1,par]<-thres:
        shift=shift+physical.geometry.size_domain
      adjust_calibs[k]=calibs[k,par]+shift
    calibs[:,par]=adjust_calibs



x_all  =np.zeros((n_data,x_dim))
y_all = np.zeros(n_data)

z=0
# for it in range(physical.Nt+1):
for par in range(n_param):
    # x_all[z,0]=times[it]
    x_all[z,0:dim_param] = param_domain[par,:]
    y_all[z] = calibs[-1,par]
    z=z+1

x_train, x_valid, y_train, y_valid = sklearn.model_selection.train_test_split(x_all, y_all, test_size=0.2, random_state=0)
x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(x_train, y_train, test_size=0.2, random_state=0)
print(x_train.shape[1])

model = BasicNeuralModel(x_dim)

model.fit(x_train,y_train,x_valid,y_valid , epochs=5000)
#error on test
y_test_predict=model.predict(x_test)

error=np.reshape(y_test,np.shape(y_test_predict))-y_test_predict
print("square mean error on test is ", np.mean(np.abs(error)**2))
print("mean error on test is ", np.sqrt(np.mean(np.abs(error)**2)))

plt.plot(y_test_predict,y_test,'.')
plt.show()
# #one parameter
# for k in range(10):
#     x_one_param=np.zeros((physical.Nt+1,x_dim))
#     x_one_param[:,1:]=param_domain[k,:]
#     x_one_param[:,0] = times
#     y_predict_one = model.predict(x_one_param)
#     plt.plot(times,y_predict_one,label="prediction")
#     plt.plot(times, calibs[:,k], label="exact")
#     plt.legend()
#     plt.title("param="+str(param_domain[k,:]))
#     plt.show()
