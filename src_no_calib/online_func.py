import numpy as np
import random
import matplotlib.pyplot as plt
import shutil, os
import solve as so
from bibli import *
from minL1 import *
from GS import *
from EIM import *
from RB_functions import *
import time
from geometry import *

def online(mu, RB, EIM, physical, minimization, real_sol, with_calibration=False, physical_or=None,calib_values=None):
	if with_calibration==False or physical_or is None or calib_values is None:
		u0,calib_ind, calib =createInitialCondition(physical, mu)
	else:
		y, u0, calib = createInitialCondition_reference(physical, physical_or, mu, calib_values[0], physical.geometry.x)
	err_ind =0.
	true_err=0.
	t_init=time.time()
	w1_alpha  = proj_on_RB(minimization, RB, u0);
	w1_alpha = np.reshape(w1_alpha,(RB.dim))
	w_ROM=np.dot(RB.RB, w1_alpha)

	#  w_ROM  = np.zeros((physical.geometry.n_dofs));
	#  for k in range(RB.dim):
	#	  w_ROM[:]  = w_ROM[:] + w1_alpha[k]*RB.RB[:,k];

	#print "w1_alpha", np.isnan(w1_alpha).any()
	# From the coordinates on the reduced space, build the full reduced
	# initial condition
	# w_ROM=np.zeros((physical.geometry.n_dofs));
	# w_ROM[EIM.all_magic_points]=np.dot(RB.RB[EIM.all_magic_points, :], w1_alpha)
	#		plt.plot(w_ROM)
	#		plt.plot(real_sol[:,0]);
	#		plt.show();
	#print "cond RB for IC=",np.linalg.cond(RB[:,:count_ndisc])
	##Now, we run our time dependent scheme
	err_ind += physical.CE**(physical.Nt)*L2_error(physical,real_sol.U[:,0], w_ROM)
	true_err += L2_error(physical,real_sol.U[:,0], w_ROM)/physical.Nt

	for it in range(1,physical.Nt+1):
		w0 			= np.copy(w_ROM);
		w0_alpha   	= np.copy(w1_alpha);
		# Use our numerical scheme to compute
		# w_{rom}(t^{n+1}) = F(w_{rom}(t^{n})
		if with_calibration==True:
			w1_alpha, RHS_alpha, RHS_M1_alpha = diffSolve_RB_EIM_proj_ref(physical, physical_or, w0, w0_alpha, EIM, RB, mu, calib_values[it-1], calib_values[it])
		else:
			w1_alpha, RHS_alpha, RHS_M1_alpha = diffSolve_RB_EIM_proj(physical, w0, w0_alpha, EIM, RB, mu)

		w1_alpha_old=np.copy(w1_alpha)
	#	print "un1 alpha", np.isnan(w1_alpha).any()
		# Project onto reduced space
		# w_ROM=np.zeros((physical.geometry.n_dofs));
		# w_ROM[EIM.all_magic_points]=np.dot(RB.RB[EIM.all_magic_points, :], w1_alpha)

		w_ROM=np.dot(RB.RB, w1_alpha)

		#  w_ROM  = np.zeros((physical.geometry.n_dofs));
		#  for k in range(RB.dim):
		#	  w_ROM[:]  = w_ROM[:] + w1_alpha[k]*RB.RB[:,k];

		tmp=np.dot(RB.RB[EIM.magic_M1,:int(RB.dim)],RHS_alpha)
		tmp2=(tmp-np.reshape(RHS_M1_alpha,np.shape(tmp)))*np.reshape(EIM.norm_fnc_M1,np.shape(tmp))
		err_ind  += physical.CE**(physical.Nt-it)*np.linalg.norm(tmp2,2);#dt is already inside RHS !!! I don't need to put it!
		err_ind += physical.CE**(physical.Nt-it)*np.linalg.norm(w1_alpha-w0_alpha+RHS_alpha,1);
		#		err_loc=physical.CE**(physical.Nt-it)*np.linalg.norm(tmp2,2)
	#		err_loc2 = physical.CE**(physical.Nt-it)*np.linalg.norm(w1_alpha-w0_alpha+RHS_alpha,1);
		true_err += L2_error(physical,real_sol.U[:,it], w_ROM)/physical.Nt


	t_fin=time.time() - t_init
	return w_ROM, err_ind, true_err, t_fin
