from bibli import *
import numpy as np
import math
import matplotlib.pyplot as plt
import time
from geometry import *
from parameters import *
from mollifier import *


ic_type=1
extras = ExtraGeometry(ic_type)
extras.set_original_domain()

physical = PhysicalProblem(extras, ic_type)

param = ParameterDomain(ic_type, 200)
physical.set_dt(param)


#mu=[1.2, .1]

mu=[1.98564, 0.156]#[1.70262103,  2.97791494,  0.34113323,  0.60731798] #param.pick_parameter_outside()

print("computing initial condition")

u0 ,calib_ind, calib   = createInitialCondition(physical, mu);

y, v0, calib  =  createInitialCondition_reference(physical, physical, mu, calib)

geometry_ref = ExtraGeometry(ic_type)
geometry_ref.set_reference_domain(y)


plt.plot(extras.x,u0, label="original")
plt.plot(geometry_ref.x,v0, label="reference")
plt.legend()
plt.show()

print "computing original problem"
solution_or= solveFOM(physical, mu);
calibs=np.copy(solution_or.calib_values)

# adjust_calibs=np.zeros(np.shape(calibs))
# shift=0.
# thres=2.
# adjust_calibs[0]=calibs[0]
# for k in range(1,len(calibs)):
#   if calibs[k]-calibs[k-1]>thres:
#     shift=shift-physical.geometry.size_domain
#   elif calibs[k]-calibs[k-1]<-thres:
#     shift=shift+physical.geometry.size_domain
#   adjust_calibs[k]=calibs[k]+shift
#
# # Make a +/- x range large enough to let kernel drop to zero
#  # Calculate kernel
#
# window = int(2./physical.CFL)
# sigma = window*physical.dt
# x_for_kernel = np.linspace(-10.*sigma, 10.*sigma, 100) #np.linspace(-10.*sigma, 10.*sigma, 1000) #np.linspace(-1.,1., 1000)#np.linspace(-10.*sigma, 10.*sigma, 1000)
# kernel = np.exp(-(x_for_kernel) ** 2 / (2 * sigma ** 2))
# # Threshold
# kernel_above_thresh = kernel > 0.0001
#  # Find x values where kernel is above threshold
# x_within_thresh = x_for_kernel[kernel_above_thresh]
#
#
# calibs_ext = np.concatenate((np.ones(6*window)*adjust_calibs[0],adjust_calibs,np.ones(6*window)*adjust_calibs[-1]))
# finite_kernel = kernel[kernel_above_thresh]
# finite_kernel = finite_kernel / finite_kernel.sum()
# plt.semilogy(x_within_thresh, finite_kernel)
# plt.show()
#
#
# convolved_y = np.convolve(calibs_ext, finite_kernel, mode="same")

mollify_calibs(solution_or)
convolved_calibs= solution_or.calib_values
plt.plot(convolved_calibs[:])
plt.plot(calibs)
plt.show()
