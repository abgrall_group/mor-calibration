import numpy as np
import random
from RB_functions import *
import matplotlib.pyplot as plt
from numpy import linalg as LA
import shutil, os
import math
import solve as so
from bibli import *
from minL1 import *
from GS import *
from EIM import *
from joblib import Parallel, delayed
import multiprocessing
import time
from geometry import *
import pickle
import copy
from parameters import *
from mollifier import *
from online_func import *

# def main_online():
###########################################
###Choose the desired minimization type

#minimization = 'L1min_LP'
#minimization = 'L1min_IRLS'
#minimization  = 'Hubermin_IRLS'
minimization = 'L2'
#minimization  = 'Hull'


with_calibration=False


ic_type=91
fname='test'+str(ic_type)+'/'

with open(fname+'problem_data', 'rb') as problem:
	with_calibration = pickle.load(problem)
	geometry = pickle.load(problem)
	if with_calibration  == True:
		geometry_ref = pickle.load(problem)
	physical = pickle.load(problem)
	if with_calibration  == True:
		physical_ref = pickle.load(problem)

nb_param_online=1
param_domain = ParameterDomain(ic_type, nb_param_online, "uniform")

if with_calibration:
	EIM = EIMStructure(geometry_ref)
else:
	EIM = EIMStructure(geometry)
EIM.load_EIM(fname)

EIM.compute_extras()
EIM.update_history()
EIM.compute_boundary_magic_points()

if with_calibration:
	RB = RBStructure(physical_ref)
else:
	RB = RBStructure(physical)
RB.load_RB(fname)

param = param_domain.pick_parameter_outside()

err_ind=0
true_err=0
print( "Computing real solution")
t_init=time.time()
real_sol = solveFOM(physical, param)
t_original_sol=time.time()-t_init

if with_calibration:
  print( "Computing reference solution")
  t_init=time.time()
  real_sol_ref = solveFOM_ref(physical_ref, physical, param, real_sol.calib_values, physical_ref.geometry.x)
  t_ref_sol  = time.time()- t_init
#print "u0", np.isnan(u0).any()

print( "Computing RB solution")
if with_calibration:
  w_ROM, err_ind, true_err, t_fin= online(param, RB, EIM, physical_ref, minimization, real_sol_ref, with_calibration, physical, real_sol.calib_values)
else:
  w_ROM, err_ind, true_err, t_fin= online(param, RB, EIM, physical, minimization, real_sol, with_calibration, physical, real_sol.calib_values)

#print w1_alpha,sum(w1_alpha)
print("original time", t_original_sol)
if with_calibration:
  print("reference time", t_ref_sol)
print ("RB_time", t_fin)
#print "mu=",mu
print("true_ind=",true_err)
print("err_ind=",err_ind)
#	plt.plot(inverse_interpolate(calibs_ROM_ind[ Nt], w_ROM, with_calibration), label="RB");
#	plt.plot(real_sol[:,Nt],label="real")
	#for j in range(count_ndisc):
	#	plt.plot(RB[:,j]);
#	plt.legend()
#	plt.show();

for k in range(0,physical.Nt+1, int(physical.Nt/10)):
  plt.plot(real_sol.U[:,k])

plt.show()


for k in range(0,physical.Nt+1, int(physical.Nt/10)):
  if with_calibration:
    plt.plot(real_sol_ref.U[:,k])
  plt.plot(w_ROM, label="time="+str(physical.dt*k))
plt.legend()
plt.show()
