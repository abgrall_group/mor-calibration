import numpy as np
import matplotlib.pyplot as plt
import keras
import pickle
import sklearn.model_selection
import pandas as pd
from bibli import *
from geometry import *
from ml_models_custom_loss import *

ic_type=0
fname='test'+str(ic_type)+'/non_rd/'
folder_calibs = fname+'calibs'

with open(folder_calibs+'/calib_points_mollified', 'rb') as problem: #
    param_domain = pickle.load(problem)
    physical = pickle.load(problem)
    calibs = pickle.load(problem)

times=np.arange(0.,physical.T+physical.dt, physical.dt)
[n_param, dim_param] = np.shape(param_domain)

x_dim = dim_param+1 #time
n_data = (physical.Nt+1)*n_param

for par in range(n_param):
    adjust_calibs=np.zeros(np.shape(calibs[:,par]))
    shift=0.
    thres=physical.geometry.size_domain/2.
    adjust_calibs[0]=calibs[0,par]
    for k in range(1,len(calibs)):
      if calibs[k,par]-calibs[k-1,par]>thres:
        shift=shift-physical.geometry.size_domain
      elif calibs[k,par]-calibs[k-1,par]<-thres:
        shift=shift+physical.geometry.size_domain
      adjust_calibs[k]=calibs[k,par]+shift
    calibs[:,par]=adjust_calibs



x_all  =np.zeros((n_data,x_dim))
y_all = np.zeros(n_data)

n_param_train=int(n_param*0.6)
n_param_valid=int(n_param*0.2)
n_param_test = n_param- n_param_train-n_param_valid


x_train  =np.zeros(( (physical.Nt+1)*n_param_train,x_dim))
y_train = np.zeros( (physical.Nt+1)*n_param_train)

x_valid  =np.zeros(( (physical.Nt+1)*n_param_valid,x_dim))
y_valid = np.zeros( (physical.Nt+1)*n_param_valid)

x_test  =np.zeros(( (physical.Nt+1)*n_param_test,x_dim))
y_test = np.zeros( (physical.Nt+1)*n_param_test)

z=0
for it in range(physical.Nt+1):
    for par in range(n_param_train):
        x_train[z,0]=times[it]
        x_train[z,1:dim_param+1] = param_domain[par,:]
        y_train[z] = calibs[it,par]
        z=z+1
z=0
for it in range(physical.Nt+1):
    for par in range(n_param_train, n_param_train+ n_param_valid):
        x_valid[z,0]=times[it]
        x_valid[z,1:dim_param+1] = param_domain[par,:]
        y_valid[z] = calibs[it,par]
        z=z+1

z=0
for it in range(physical.Nt+1):
    for par in range(n_param_train+ n_param_valid, n_param_train+ n_param_valid + n_param_test):
        x_test[z,0]=times[it]
        x_test[z,1:dim_param+1] = param_domain[par,:]
        y_test[z] = calibs[it,par]
        z=z+1



#
# x_train, x_valid, y_train, y_valid = sklearn.model_selection.train_test_split(x_all, y_all, test_size=0.2, random_state=0)
# x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(x_train, y_train, test_size=0.2, random_state=0)
# print(x_train.shape[1])

model = BasicNeuralModel(x_dim)

model.fit(x_train,y_train,x_valid,y_valid, epochs=10)
#error on test
y_test_predict=model.predict(x_test)

error=np.reshape(y_test,np.shape(y_test_predict))-y_test_predict
print("mean error on test is ", np.mean(np.abs(error)))
print("max error on test is ", np.max(np.abs(error)))

with open(folder_calibs+'/multiLayerPerceptron.pickle','wb') as NN:
    pickle.dump(model, NN)


#one parameter
for k in range(10):
    x_one_param=np.zeros((physical.Nt+1,x_dim))
    x_one_param[:,1:]=param_domain[-k,:]
    x_one_param[:,0] = times
    y_predict_one = model.predict(x_one_param)
    # plt.plot(times,y_predict_one-np.reshape(calibs[:,-k], np.shape(y_predict_one)),label="error prediction")
    plt.plot(times,y_predict_one[:,0]+times*y_predict_one[:,1],label="pred")
    plt.plot(times,np.reshape(calibs[:,-k], np.shape(y_predict_one[:,0])),label="exact")
    plt.plot(times, times*param_domain[-k,0]+(0.2-0.1*param_domain[-k,2]), label="my prediction")
    plt.legend()
    plt.title("param="+str(param_domain[-k,:]))
    plt.show()
