## Model order reduction for Residual distribution code with calibration

This code provides a Greedy POD EIM approach to solve model order reduction tasks.

The Greedy search is done in parameter space, the POD in time space and the EIM procedure is needed to interpolate the non linear fluxes.

The solver used are based on residual distribution technique.

Hopefully, we will be able to add a calibration process to reshape the function in time such that physical waves coincide.