__all__=["ParameterDomain"]
import numpy as np
from geometry import *
from tests import ParameterDomainBoundaries

class ParameterDomain:
  def __init__(self, ic_type, n_param, distribution=None):
    param_bound = ParameterDomainBoundaries(ic_type)
    self.min_vals=param_bound.mu_min
    self.max_vals = param_bound.mu_max
    if len(self.min_vals)!= len(self.max_vals):
      raise ValueError("the size of min vals and max vals should be the same")
    self.dim_param = len(self.min_vals)
    self.n_param = n_param
    self.param_domain = np.zeros(( self.n_param, self.dim_param))
    for k in range(self.n_param):
      if (distribution is None) or distribution=="uniform":
        self.param_domain[k,:]=self.pick_parameter_outside(distribution)
      else:
        raise ValueError("distribution is not defined")

  def pick_parameter_outside(self, distribution=None):
    if distribution is None:
      distribution = "uniform"
    if distribution=="uniform":
      return self.min_vals + (self.max_vals-self.min_vals)*np.random.rand(self.dim_param)
    else:
      raise ValueError("distribution is not defined")

  def pick_parameter_inside(self):
    chosen = np.random.randint(self.n_param)
    return self.param_domain[chosen,:]

