__all__=["PolyRegressionMap"]

import numpy as np
import time


class PolyRegressionMap:
	def __init__(self,d):
		self.dim = d
		self.time_training=0.


	def train(self, x_train, y_train):
		t_init=time.time()
		PHI=basis_funs(x_train,self.dim)
		PP=np.dot(np.transpose(PHI),PHI)
		self.cc=np.linalg.solve(PP, np.dot(np.transpose(PHI),y_train))
		self.time_training=self.time_training+time.time()-t_init

	def validate(self, x_val,y_val):
		y_predict = self.predict(x_val)
		error= y_val-y_predict
		self.mean_error = np.mean(np.abs(error))
		self.max_error = np.max(np.abs(error))
		return self.mean_error, self.max_error


	def predict(self, x_test):
		try:
			y_predict=np.dot(basis_funs(x_test,self.dim),self.cc)
			return y_predict
		except:
			raise ValueError('Regression Map not trained yet')
			



def basis_funs(XX,d):
    # d is the maximum degree for each basis funcitons Q^(d-1)
    # shape of XX are ndata, features
    n=np.shape(XX)[1]
    YY=np.ones((np.shape(XX)[0],d**n));
    for k in range(d**n):
        z=decimalTodthary(k,d)
        z=np.concatenate([np.zeros(n,dtype=np.int),z])
        for i in range(n):
            YY[:,k]=YY[:,k]*(XX[:,i]**z[-i-1])
    return YY

def decimalTodthary(n,d):  
    if(n > d-1):  
        # divide with integral result  
        # (discard remainder) 
        z=decimalTodthary(n//d,d) 
        z.append(n%d)
        return z
    else:
        return [n]