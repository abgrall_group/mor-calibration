__all__=["mollify_calibs"]
from bibli import *
import numpy as np
from geometry import *
from parameters import *

def mollify_calibs(solution):
    physical=solution.physical
    calibs=solution.calib_values
    adjust_calibs=np.zeros(np.shape(calibs))
    shift=0.
    thres=physical.geometry.size_domain/2.
    adjust_calibs[0]=calibs[0]
    for k in range(1,len(calibs)):
      if calibs[k]-calibs[k-1]>thres:
        shift=shift-physical.geometry.size_domain
      elif calibs[k]-calibs[k-1]<-thres:
        shift=shift+physical.geometry.size_domain
      adjust_calibs[k]=calibs[k]+shift

    # Make a +/- x range large enough to let kernel drop to zero
     # Calculate kernel
    window = int(2./physical.CFL)
    sigma = window*physical.dt
    x_for_kernel = np.linspace(-10.*sigma, 10.*sigma, 100) #np.linspace(-10.*sigma, 10.*sigma, 1000) #np.linspace(-1.,1., 1000)#np.linspace(-10.*sigma, 10.*sigma, 1000)
    kernel = np.exp(-(x_for_kernel) ** 2 / (2 * sigma ** 2))
    # Threshold
    kernel_above_thresh = kernel > 0.0001
     # Find x values where kernel is above threshold
    x_within_thresh = x_for_kernel[kernel_above_thresh]
    calibs_ext = np.concatenate((np.ones(6*window)*adjust_calibs[0],adjust_calibs,np.ones(6*window)*adjust_calibs[-1]))
    finite_kernel = kernel[kernel_above_thresh]
    finite_kernel = finite_kernel / finite_kernel.sum()
    # plt.semilogy(x_within_thresh, finite_kernel)
    # plt.show()
    convolved_y = np.convolve(calibs_ext, finite_kernel, mode="same")
    convolved_calibs=[x%physical.geometry.size_domain for x in convolved_y]
    convolved_calibs=convolved_calibs[6*window:-6*window]
    # plt.plot(convolved_calibs[6*window:-6*window])
    # plt.plot(calibs)
    # plt.show()
    solution.calib_values=convolved_calibs
    return solution
