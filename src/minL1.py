__all__=["L1min","L1min_LP","Hull","Hubermin_IRLS","L1min_IRLS","L2"]

import numpy as np
import math


def L1min(n,m,A,b):
  import cvxopt
  from cvxopt import  solvers,matrix

  cvxopt.solvers.options['show_progress'] = False
# il faut n>m
# Id: identite (n,n) define dans le programme principal
# A matrice (n,m)
# b(vecteur n
# calcule min_x||Ax-b||_1

  B=matrix(0.,(2*n,n+m))

  for i in range(n):
    for j in range(m):
     B[i,j]    = A[i,j]
     B[i+n,j]  =-A[i,j]

  for i in range(n):
#     for j in range(n):
        B[i    ,i+m]  =-1.0
        B[i+n  ,i+m]  =-1.0

#  for i in range(2*n):
#   print B[i,:]

  c=matrix(0.,(n+m,1))
  c[m:n+m]=1.0


  h=matrix(0.,(2*n,1))
  for i in range(n):
    h[i]  = b[i]
    h[i+n]=-b[i]


  sol=solvers.lp(c,B,h)
  s= sol['x']
#  print "s",s
#  print s[0:n-1]
  return s[0:n-1]


def L1min_LP(n,m,A,b):
  import cvxopt
  from cvxopt import  solvers,matrix

  cvxopt.solvers.options['show_progress'] = False
# il faut n>m
# Id: identite (n,n) define dans le programme principal
# A matrice (n,m)
# b(vecteur n
# calcule min_x||Ax-b||_1+eps*||x||_1

  eps=1.e-6


  B=matrix(0.,(2*n+2*m,2*m+n))

  for i in range(n):
    for j in range(m):
     B[i,j]    = A[i,j]
     B[i+n,j]  =-A[i,j]

  for i in range(n):
        B[i    ,i+m]  =-1.0
        B[i+n  ,i+m]  =-1.0

  for i in range(m):
        B[i+2*n  ,i]= 1.0
        B[i+2*n+m,i]=-1.0
        B[i+2*n  , i+n+m]=-1.0
        B[i+2*n+m, i+n+m]=-1.0


#  for i in range(2*n):
#   print B[i,:]

  c=matrix(0.,(n+2*m,1))
  c[m:n+m]=1.0
  c[0:m]=eps


  h=matrix(0.,(2*n+2*m,1))
  for i in range(n):
    h[i]  = float(b[i])
    h[i+n]=-float(b[i])


  sol=solvers.lp(c,B,h)
  s= sol['x']
#  print "s",s
#  print s[0:n-1]
  return np.reshape(s[0:m],(m,1))


def Hull(n,m,A,b):
  import cvxopt
  from cvxopt import  solvers,matrix

  cvxopt.solvers.options['show_progress'] = False
# il faut n>m
# Id: identite (n,n) define dans le programme principal
# A matrice (n,m)
# b vecteur n


  eps=1.e-5


  B=matrix(0.,(2*n+m+2,m+n))

  for i in range(n):
    for j in range(m):
     B[i,j]    = A[i,j]
     B[i+n,j]  =-A[i,j]

  for i in range(n):
        B[i    ,i+m]  =-1.0
        B[i+n  ,i+m]  =-1.0

  for i in range(m):
        B[i+2*n  ,i]=- 1.0

        B[2*n+m  ,i]= 1.0

        B[2*n+m+1  ,i]=- 1.0

  c=matrix(0.,(n+m,1))
  c[m:n+m]=1.0
  c[0:m]=0.0


  h=matrix(0.,(2*n+m+2,1))
  for i in range(n):
    h[i]  = float(b[i])
    h[i+n]=-float(b[i])
    h[2*n:2*n+m]=0.0
    h[2*n+m]=1.0
    h[2*n+m+1]=-1.0


  sol=solvers.conelp(c,B,h)
  s=sol['x']
  #print( sol['x'])
  #print "s",s
  #print s[0:n-1]
  return s[0:m]

def Hubermin_IRLS(n,m,A,b):


  epsIRLS = 1.e-16
  weights = np.zeros(n)
  alpha = np.zeros(m)
  delalpha = np.zeros(m)
  res = np.zeros(n)
  bweighted = np.zeros(n)
  Aweighted = matrix(0.,(n,m))
  iterMax = 200

  # initial guess: L2
  alphaL2 = np.linalg.lstsq(A,b)
  for j in range(m):
    alpha[j] = alphaL2[0][j]

  for iter in range(iterMax):
    for i in range(n):
      res[i] = -b[i]
      for j in range(m):
        res[i] = res[i] + A[i,j]*alpha[j]
      maxres = max(res)
      M = 1e-6*max(1,maxres)
      if (abs(res[i])<M):
        weights[i] = 1
      else:
        weights[i] = M/(abs(res[i])**0.5+epsIRLS)
      bweighted[i] = -res[i]*weights[i]
      for j in range(m):
        Aweighted[i,j] = A[i,j]*weights[i]
    delalpha = np.linalg.lstsq(Aweighted,bweighted)
    normalpha = 0
    normdelalpha = 0
    for j in range(m):
      normalpha = normalpha + abs(alpha[j])
      normdelalpha = normdelalpha + abs(delalpha[0][j])
      alpha[j] = alpha[j] + delalpha[0][j]
    if (normdelalpha<1e-12):
      break
  #print "converged after", iter, "iterations"
  return alpha






def L1min_IRLS(n,m,A,b):


  epsIRLS = 1.e-8
  weights = np.zeros(n)
  alpha = np.zeros(m)
  delalpha = np.zeros(m)
  res = np.zeros(n)
  bweighted = np.zeros(n)
  Aweighted = matrix(0.,(n,m))
  iterMax = 20

  # initial guess: L2
  alphaL2 = np.linalg.lstsq(A,b)
  for j in range(m):
    alpha[j] = alphaL2[0][j]

  for iter in range(iterMax):
    for i in range(n):
      res[i] = -b[i]
      for j in range(m):
        res[i] = res[i] + A[i,j]*alpha[j]
      weights[i] = 1/(abs(res[i]+epsIRLS)**0.5)
      bweighted[i] = -res[i]*weights[i]
      for j in range(m):
        Aweighted[i,j] = A[i,j]*weights[i]
    delalpha = np.linalg.lstsq(Aweighted,bweighted)
    normalpha = 0
    normdelalpha = 0
    for j in range(m):
      normalpha = normalpha + abs(alpha[j])
      normdelalpha = normdelalpha + abs(delalpha[0][j])
      alpha[j] = alpha[j] + delalpha[0][j]
    if (normdelalpha<1e-8*(normalpha)):
      break
  #print "converged after", iter, "iterations"
  return alpha


def L2(n,m,A,b):
# il faut n>m
# Id: identite (n,n) define dans le programme principal
# A matrice (n,m)
# b(vecteur n
# calcule min_x||Ax-b||_1+eps*||x||_1
  alpha = np.linalg.lstsq(A,b) #,rcond=-1)
  return alpha[0]
