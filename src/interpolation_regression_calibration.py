import numpy as np
import matplotlib.pyplot as plt
import pickle
from bibli import *
from geometry import *
import time
from polynomial_regression import *
from pwLinear_regression import *
from ml_models import *
import matplotlib.cm as cm

colors=["#E69F00", "#56B4E9", "#009E73", "#0072B2", "#D55E00", "#CC79A7", "#F0E442"]

from cycler import cycler
line_cycler   = (cycler(color=["#E69F00", "#56B4E9", "#009E73", "#0072B2", "#D55E00", "#CC79A7", "#F0E442","#E69F00", "#56B4E9", "#009E73", "#0072B2", "#D55E00", "#CC79A7", "#F0E442"]) +
                 cycler(linestyle=["-", "--", "-.", ":", "-", "--", "-.",":","-", "--", "-.", ":", "-", "--"]))
marker_cycler = (cycler(color=["#E69F00", "#56B4E9", "#009E73", "#0072B2", "#D55E00", "#CC79A7", "#F0E442"]) +
                 cycler(linestyle=["none", "none", "none", "none", "none", "none", "none"]) +
                 cycler(marker=["4", "2", "3", "1", "+", "x", "."]))


ic_type=91  #0 1 6 90 91
fname='test'+str(ic_type)+'/' #'non_rd/'
folder_calibs = fname+'calibs'

with open(folder_calibs+'/calib_points_mollified', 'rb') as problem: #
    param_domain = pickle.load(problem)
    physical = pickle.load(problem)
    calibs = pickle.load(problem)

times=np.arange(0.,physical.T+physical.dt, physical.dt)
[n_param, dim_param] = np.shape(param_domain)

x_dim = dim_param+1 #time
n_data = (physical.Nt+1)*n_param

if physical.geometry.geometry_parameters.bc_type=='periodic':
    for par in range(n_param):
        adjust_calibs=np.zeros(np.shape(calibs[:,par]))
        shift=0.
        thres=physical.geometry.size_domain/2.
        adjust_calibs[0]=calibs[0,par]
        for k in range(1,len(calibs)):
          if calibs[k,par]-calibs[k-1,par]>thres:
            shift=shift-physical.geometry.size_domain
          elif calibs[k,par]-calibs[k-1,par]<-thres:
            shift=shift+physical.geometry.size_domain
          adjust_calibs[k]=calibs[k,par]+shift
        calibs[:,par]=adjust_calibs


trains=[5,10,20,50,100,200]
ntrains=len(trains)
error_NN=np.zeros((ntrains,2)) #average and maximum
error_pwLr=np.zeros((ntrains,2))
error_P=np.zeros((6,ntrains,2))


for train_param, n_param_train in enumerate(trains):
    n_param_valid=int(n_param_train/2)
    n_param_test = np.min([n_param- n_param_train-n_param_valid,int(n_param_train/2)])
    print("Training parameters" , n_param_train)


    x_train  =np.zeros( [n_param_train,dim_param])
    y_train = np.zeros( [physical.Nt+1,n_param_train])

    x_valid  =np.zeros( [n_param_valid,dim_param])
    y_valid = np.zeros( [physical.Nt+1,n_param_valid])

    x_test  =np.zeros( [n_param_test,dim_param])
    y_test = np.zeros( [physical.Nt+1,n_param_test])

    x_train_long = np.zeros(((physical.Nt+1)*n_param_train,dim_param+1))
    y_train_long = np.zeros((physical.Nt+1)*n_param_train)
    x_valid_long = np.zeros(((physical.Nt+1)*n_param_valid,dim_param+1))
    y_valid_long = np.zeros((physical.Nt+1)*n_param_valid)
    x_test_long = np.zeros(((physical.Nt+1)*n_param_test, dim_param+1))
    y_test_long = np.zeros((physical.Nt+1)*n_param_test)


    paridx=-1
    for par in range(n_param_train):
        paridx=paridx+1
        x_train[par,:] = param_domain[par,:]   
        for it in range(physical.Nt+1):
            y_train[it,par] = calibs[it,par]
            x_train_long[paridx*(physical.Nt+1)+it,:] = np.concatenate([np.array([it*physical.dt]),param_domain[par,:]])
            y_train_long[paridx*(physical.Nt+1)+it] = calibs[it,par]

    z=-1
    paridx=-1
    for par in range(n_param_train, n_param_valid+n_param_train):
        paridx=paridx+1
        x_valid[paridx,:] = param_domain[par,:]   
        for it in range(physical.Nt+1):
            z=z+1
            y_valid[it,paridx] = calibs[it,par]
            x_valid_long[z,:] = np.concatenate([np.array([it*physical.dt]),param_domain[par,:]])
            y_valid_long[z] = calibs[it,par]

    z=-1
    paridx=-1
    for par in range(n_param_train+ n_param_valid, n_param_train+ n_param_valid + n_param_test):
        paridx=paridx+1 
        z=z+1
        x_test[z,:] = param_domain[par,:]   
        for it in range(physical.Nt+1):
            y_test[it,z] = calibs[it,par]
            x_test_long[paridx*(physical.Nt+1)+it,:] = np.concatenate([np.array([it*physical.dt]),param_domain[par,:]])
            y_test_long[paridx*(physical.Nt+1)+it] = calibs[it,par]


    NNRegr=NNRegressionMap(x_dim)
    NNRegr.train(x_train_long, y_train_long, x_valid_long, y_valid_long)
    t_init=time.time()
    NNRegr.validate(x_valid_long, y_valid_long)
    t_fin=time.time()-t_init
    print("mean error on test is ", NNRegr.mean_error)
    print("max error on test is ", NNRegr.max_error)
    print("ave time for a whole time seq", t_fin/n_param_test )
    error_NN[train_param,:]=[NNRegr.mean_error,NNRegr.max_error]



    pwRegr=PWRegressionMap()
    pwRegr.train(x_train,y_train)
    t_init=time.time()
    pwRegr.validate(x_test, y_test)
    t_fin=time.time()-t_init
    print("mean error on test is ", pwRegr.mean_error)
    print("max error on test is ", pwRegr.max_error)
    print("ave time for a whole time seq", t_fin/n_param_test )

    error_pwLr[train_param,:]=[pwRegr.mean_error,pwRegr.max_error]


    #one parameter
    # for k in range(0,20,2):
    #     # x_one_param=np.zeros((physical.Nt+1,x_dim))
    #     x_one=param_domain[-k,:]
    #     # x_one_param[:,0] = times
    #     # y_predict_one = model.predict(x_one_param)
    #     y_predict_one = pwRegr.predict(x_one)
    #     # plt.plot(times,y_predict_one-np.reshape(calibs[:,-k], np.shape(y_predict_one)),label="error prediction")
    #     plt.plot(times, y_predict_one,'o',label="pred")
    #     plt.plot(times, calibs[:,-k],'.',label="exact")
    #     #    plt.plot(times, times*param_domain[-k,0]+(0.2-0.1*param_domain[-k,1]), label="my prediction")
    #     plt.legend()
    #     #    plt.title("param="+str(param_domain[-k,:]))

    #plt.title("Interpolation test")
    #plt.show(block=False)


    # t_init=time.time()
    # y_exact = np.zeros([physical.Nt+1,n_param_test])
    # for par in range(n_param_test):
    #     for it in range(physical.Nt+1):
    #         y_exact[it,par]=x_test[par,0]*physical.dt*it +0.2-0.1*x_test[par,2] 

    # t_fin=time.time()-t_init

    # error=np.reshape(y_test,np.shape(y_exact))-y_exact
    # print("mean error on test exact is ", np.mean(np.abs(error)))
    # print("max error on test exact is ", np.max(np.abs(error)))
    # print("ave time for a whole time seq", t_fin/n_param_test )




    degrees=range(1,7)
    for dd,dimension_Q_space in enumerate(degrees):
        regr=PolyRegressionMap(dimension_Q_space)
        regr.train(x_train_long,y_train_long)
        print(dimension_Q_space)
        t_init=time.time()
        mean_error,max_error= regr.validate(x_test_long, y_test_long)
        t_fin=time.time() - t_init
        print("mean error on test exact is ", mean_error)
        print("max error on test exact is ", max_error)
        print("ave time for a whole time seq", t_fin/n_param_test )
        error_P[dd,train_param,:]=[regr.mean_error,regr.max_error]

    regr=PolyRegressionMap(4)
    regr.train(x_train_long,y_train_long)
    regr.validate(x_test_long, y_test_long)
        
    #one parameter
    Nt=physical.Nt
    for k in range(0,20,2):
        col=colors[int(k/2)%np.shape(colors)[0]]
        x_one=[np.concatenate( [[it*physical.dt],param_domain[-k,:]]) for it in range(physical.Nt+1)]
        x_one=np.array(x_one)
        y_predict_poly=regr.predict(x_one)
        y_predict_NN=NNRegr.predict(x_one)
        x_one=param_domain[-k,:]
        y_predict_pwlin = pwRegr.predict(x_one)
        slice=range(0,Nt+1,int(Nt/30))
        plt.plot(times[slice], y_predict_pwlin[slice],'o',label="PWlin",color=col)
        plt.plot(times[slice], y_predict_poly[slice],'d',label="poly4 regr",color=col)
        plt.plot(times[slice], y_predict_NN[slice],'*',label="NN",color=col)
        plt.plot(times[slice], calibs[slice,-k],'.',label="exact",color=col)
        #    plt.plot(times, times*param_domain[-k,0]+(0.2-0.1*param_domain[-k,1]), label="my prediction")
        plt.legend()
        #    plt.title("param="+str(param_domain[-k,:]))

    # plt.title("Q3 test")
    plt.savefig(fname+"RegressionTest_train"+str(n_param_train)+".pdf")
    # plt.show()

    for k in [7]:
        x_one=[np.concatenate( [[it*physical.dt],param_domain[-k,:]]) for it in range(physical.Nt+1)]
        x_one=np.array(x_one)
        y_predict_poly=regr.predict(x_one)
        y_predict_NN=NNRegr.predict(x_one)
        x_one=param_domain[-k,:]
        y_predict_pwlin = pwRegr.predict(x_one)
        slice=range(0,Nt+1,int(Nt/30))
        plt.plot(times[slice], y_predict_pwlin[slice],'o',label="PWlin",color=colors[0])
        plt.plot(times[slice], y_predict_poly[slice],'d',label="poly4 regr",color=colors[1])
        plt.plot(times[slice], y_predict_NN[slice],'*',label="NN",color=colors[2])
        plt.plot(times[slice], calibs[slice,-k],'.',label="exact",color=colors[3])
        #    plt.plot(times, times*param_domain[-k,0]+(0.2-0.1*param_domain[-k,1]), label="my prediction")
        plt.legend()
        #    plt.title("param="+str(param_domain[-k,:]))

    # plt.title("Q3 test")
    plt.savefig(fname+"RegressionTestOne_train"+str(n_param_train)+".pdf")
    # plt.show()

plt.rc("axes", prop_cycle=line_cycler)
plt.cla()

plt.loglog(trains,error_NN[:,0],label="Neural network" )
plt.loglog(trains,error_pwLr[:,0],label="Piecewise Interpolation" )
for dd, deg in enumerate(degrees):
    plt.loglog(trains,error_P[dd,:,0], label="Poly deg"+str(deg) )

plt.loglog(trains,np.ones(np.shape(trains))*physical.geometry.dx[0]*3,':',label="Threshold 3dx")
plt.legend()
plt.xlabel("Training set")
plt.ylabel("Validation error")
#plt.set_ylim([10^-3, 10^1])
plt.savefig(fname+"Mean_error_regression.pdf")


plt.cla()
plt.clf()
plt.loglog(trains,error_NN[:,1],label="Neural network" )
plt.loglog(trains,error_pwLr[:,1],label="Piecewise Interpolation" )
for dd, deg in enumerate(degrees):
    plt.loglog(trains,error_P[dd,:,1], label="Poly deg"+str(deg) )

plt.loglog(trains,np.ones(np.shape(trains))*physical.geometry.dx[0]*3,':',label="Threshold 3dx")

plt.legend()
plt.xlabel("Training set")
plt.ylabel("Validation error")
plt.savefig(fname+"Max_error_regression.pdf")


for k in range(10):
    x_one=param_domain[-k,:]
    y_predict_one=np.zeros([physical.Nt+1])
    for it in range(physical.Nt+1):
        y_predict_one[it]= regression(x_one, x_train, y_train[it,:])
    # plt.plot(times,y_predict_one-np.reshape(calibs[:,-k], np.shape(y_predict_one)),label="error prediction")
    #    plt.plot(times, y_predict_one,label="pred")
    plt.plot(times,calibs[:,-k]- y_predict_one,label="mu="+str(k))
    #    plt.plot(times, times*param_domain[-k,0]+(0.2-0.1*param_domain[-k,1]), label="my prediction")
    plt.legend()
    #    plt.title("param="+str(param_domain[-k,:]))

plt.title("error piecewise linear regression test")
plt.show()

for k in range(10):
    x_one=param_domain[-k,:]
    y_predict_one=np.zeros([physical.Nt+1])
    for it in range(physical.Nt+1):
        y_predict_one[it]= regression(x_one, x_train, y_train[it,:])
    # plt.plot(times,y_predict_one-np.reshape(calibs[:,-k], np.shape(y_predict_one)),label="error prediction")
    #    plt.plot(times, y_predict_one,label="pred")
    plt.plot(calibs[:,-k], y_predict_one,label="mu="+str(k))
    #    plt.plot(times, times*param_domain[-k,0]+(0.2-0.1*param_domain[-k,1]), label="my prediction")
    plt.legend()
    #    plt.title("param="+str(param_domain[-k,:]))

plt.title("error test")
plt.show()

for k in range(10):
    x_one_param=np.zeros((physical.Nt+1,x_dim))
    x_one_param[:,1:]=param_domain[k,:]
    x_one_param[:,0] = times
    y_predict_one = model.predict(x_one_param)
    # plt.plot(times,y_predict_one-np.reshape(calibs[:,-k], np.shape(y_predict_one)),label="error prediction")
    #    plt.plot(times, y_predict_one,label="pred")
    plt.plot(times,np.reshape(calibs[:,k], np.shape(y_predict_one[:,0]))-y_predict_one[:,0],label="error")
    #    plt.plot(times, times*param_domain[-k,0]+(0.2-0.1*param_domain[-k,1]), label="my prediction")
    plt.legend()
    #    plt.title("param="+str(param_domain[-k,:]))

plt.title("error train")
plt.show()



for k in range(10):
    x_one_param=np.zeros((physical.Nt+1,x_dim))
    x_one_param[:,1:]=param_domain[k,:]
    x_one_param[:,0] = times
    y_predict_one = model.predict(x_one_param)
    # plt.plot(times,y_predict_one-np.reshape(calibs[:,-k], np.shape(y_predict_one)),label="error prediction")
    plt.plot(times, y_predict_one,label="pred")
    plt.plot(times,np.reshape(calibs[:,k], np.shape(y_predict_one[:,0])),label="exact")
    #plt.plot(times, times*param_domain[-k,0]+(0.2-0.1*param_domain[-k,1]), label="my prediction")
    plt.legend()
    #    plt.title("param="+str(param_domain[-k,:]))

plt.title("train")
plt.show()


