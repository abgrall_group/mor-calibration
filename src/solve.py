__all__=["solve"]
import   minL1 as mi
import numpy as np

def solve(n,ndisc,A,v):
  alpha = np.zeros(ndisc)
  alpha=mi.L2(n,ndisc,A,v)
  # if minimization=='L1min_LP':
  #  alpha= mi.L1min_LP(n,ndisc,A,v)
  # elif minimization=='L1min_IRLS':
  #  alpha = mi.L1min_IRLS(n,ndisc,A,v)
  # elif minimization=='Hubermin_IRLS':
  #  alpha = mi.Hubermin_IRLS(n,ndisc,A,v)
  # elif minimization=='L2':
  #  alpha = mi.L2(n,ndisc,A,v)
  # elif minimization=='Hull':
  #  alpha = mi.Hull(n,ndisc,A,v)
  return alpha

