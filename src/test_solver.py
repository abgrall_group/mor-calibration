from bibli import *
import numpy as np
import math
import matplotlib.pyplot as plt
import time
from geometry import *
from parameters import *
from mollifier import *


ic_type=91
extras = ExtraGeometry(ic_type)
extras.set_original_domain()

physical = PhysicalProblem(extras, ic_type)

param = ParameterDomain(ic_type, 200)
physical.set_dt(param)

geometry_ref = ExtraGeometry(ic_type)
geometry_ref.set_reference_domain()
print("STARTTTT")
physical_ref=PhysicalProblem(geometry_ref, ic_type)
physical_ref.set_Nt(physical.get_Nt())
#mu=[1.2, .1]

mu=[ 1.28028367,  0.83005941,  0.71980742]#, -0.81841042]#[ 1.14130878, -0.49526511,  0.16696342, -0.52744407] #param.pick_parameter_outside()#[1.98564, 0.156]#[1.70262103,  2.97791494,  0.34113323,  0.60731798] #param.pick_parameter_outside()

print("computing initial condition")

u0 ,calib_ind, calib   = createInitialCondition(physical, mu);

y, v0, calib  =  createInitialCondition_reference(physical_ref, physical, mu, calib, geometry_ref.x)




plt.plot(extras.x,u0, label="original")
plt.plot(geometry_ref.x,v0, label="reference")
plt.legend()
plt.show()

print("computing original problem")
solution_or= solveFOM(physical, mu);
calibs_old=np.copy(solution_or.calib_values)
mollify_calibs(solution_or)
calibs=solution_or.calib_values


plt.plot([k*physical.dt for k in range(physical.Nt +1)],calibs, label="mollified")
plt.plot([k*physical.dt for k in range(physical.Nt +1)],calibs_old, label="original")
plt.legend()
plt.title("Calibration parameter in time")
plt.show(block=False)

print("computing reference problem")


solution_ref= solveFOM_ref(physical_ref, physical, mu, calibs, physical_ref.geometry.x)


print("computing exact reference problem")
exact_ref = FullSolution(mu, physical_ref)

for it in range(physical.Nt+1):
  exact_ref.set_solution(it, interpolate_or2ref(extras.x,geometry_ref.x, calibs[it], solution_or.U[:,it], extras))

print("exact_ref, IC, y0, v0",geometry_ref.x[0], exact_ref.U[0,0])

plt.plot(geometry_ref.x,exact_ref.U[:,0])
plt.title("exact on reference")
plt.show(block=False)

#compare initial conditions
x=extras.x
for k in np.concatenate([[int(z) for z in np.linspace(0,physical.Nt,10)]]):#np.concatenate([[0,1,2,3],range(4,physical.Nt+1,10)]):
#  if (calib_indices[k]-calib_indices[max(k-1,0)])!=0:
    print(k)
    print("calib     ",calibs[k])
    print("calib_diff",calibs[k]-calibs[max(k-1,0)])
    plt.plot(x,solution_or.U[:,k], 'r', label="original"+str(k))
    plt.plot(calibs[k],solution_or.U[np.argmin(abs(calibs[k]-x)),k] , 'x')
    plt.plot(geometry_ref.x,exact_ref.U[:,k], 'b', label="exact"+str(k))
    plt.plot(physical_ref.geometry.x,solution_ref.U[:,k], 'g', label="computed"+str(k))
    plt.legend()
    plt.show()
  #	time.sleep(0.5)
  #	plt.close()
  #	plt.show(block=False)

#  plt.close()


#CE=1+dt/2. #lambda dt: 1.+dt/2.
