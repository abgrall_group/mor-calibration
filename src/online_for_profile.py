import numpy as np
import random
from cvxopt import matrix
import matplotlib.pyplot as plt
from numpy import linalg as LA
import shutil, os
import math
import solve as so
from bibli import *
from minL1 import *
from GS import *
from EIM import *
import time
from geometry import *

def online_profile():
	###########################################



	with_calibration=True


	folder="data"
	#folder="data_"+str(dim_par)+"_param"

	###############################################################################
	## DEFINE PDE Problem Parameters !

	problem_data=np.load('data/problem_data.npz')

	truthN=problem_data['arr_0']
	CFL=problem_data['arr_1']
	Nt=problem_data['arr_2']
	dx=problem_data['arr_3']
	dt=problem_data['arr_4']
	CE=problem_data['arr_5']
	x=problem_data['arr_6']
	T_fin=problem_data['arr_7']
	interval_min=[problem_data['arr_8']]
	interval_max=[problem_data['arr_9']]

	n=truthN

	dim_par=np.size(interval_max)

	#Import EIM and RB


	EIM_fun = np.load(folder+'/fun_EIM.npy')
	magic_pts = np.load(folder+'/magic_pts.npy')
	magic_M1=np.load(folder+'/magic_M1.npy')
	fnc_M1=np.load(folder+'/fnc_M1.npy')
	EIM_fun_RB = np.load(folder+'/EIM_fun_RB.npy')
	M1_EIM = np.shape(fnc_M1)[0]

	print np.shape(EIM_fun)
	print np.shape(EIM_fun_RB)


	norm_fnc_M1=np.linalg.norm(fnc_M1,2,axis=0)/np.sqrt(truthN)
	EIM_dim=np.shape(EIM_fun)[1]
	EIM_inv=np.linalg.inv(EIM_fun[magic_pts,:])
	RB = np.load (folder+'/RB.npy')
	print np.shape(RB)

	count_ndisc = np.shape(EIM_fun_RB)[0]
	RB=RB[:,:count_ndisc]

	###############################


	param = 0.5763

	err_ind=0
	true_err=0
	print "Computing real solution"
	u0      = np.zeros(truthN);
	U       = np.zeros((truthN,Nt+1));
	calibs_ind=np.zeros((Nt+1), dtype=np.int)
	calibs_ROM_ind=np.zeros((Nt+1), dtype=np.int)

	t_init=time.time()
	u0, calibs_ind[0] =createInitialCondition(truthN,param,x, with_calibration)
	U_not_calib,U_flux, calib_index = solveFOM(truthN,Nt,u0,dx,dt, with_calibration, param);
	calibs_ind[1:]=calib_index
	for it in range(0,Nt+1):
		U[:,it]=interpolate(calibs_ind[it],U_not_calib[:,it], with_calibration)
	t_fin=time.time() - t_init
	print "time_truthN", t_fin
	real_sol          	= U;
	#print "u0", np.isnan(u0).any()
	w_ROM  = np.zeros((truthN));

	w0  = u0

	t_init=time.time()
	calibs_ROM_ind[0] = calibs_ind[0]
	w = interpolate(calibs_ROM_ind[0],w0, with_calibration)
	w1_alpha  = so.solve(truthN,count_ndisc,RB,w);
	w1_alpha = np.reshape(w1_alpha,(count_ndisc,1))

	#print "w1_alpha", np.isnan(w1_alpha).any()
	# From the coordinates on the reduced space, build the full reduced
	# initial condition

	for j in range(count_ndisc):
		w_ROM[:]  = w_ROM[:] + w1_alpha[j]*RB[:,j];
	#		plt.plot(w_ROM)
	#		plt.plot(real_sol[:,0]);
	#		plt.show();

	err_ind += CE**(Nt)*np.linalg.norm(real_sol[:,0]-w_ROM[:],2)/np.sqrt(n)
	true_err += np.linalg.norm(real_sol[:,0]-w_ROM[:],2)/np.sqrt(n)/Nt

	#print "cond RB for IC=",np.linalg.cond(RB[:,:count_ndisc])	
	##Now, we run our time dependent scheme
	for it in range(1, Nt+1):
		with_shift=0
		#print "it=",it
		w0 			= np.copy(w_ROM);
		w0_alpha   	= np.copy(w1_alpha);

		w1_alpha,dt,RHS_alp,RHS_M1  = diffSolve_RB_EIM_proj(dx,dt,w0,w0_alpha,n, magic_pts, EIM_fun_RB,EIM_inv, magic_M1, param);
	#	print "un1 alpha", np.isnan(w1_alpha).any()
		# Project onto reduced space	
		w_ROM 	= np.zeros((truthN));

		w1_0  = np.zeros((n));

	
		for k in range(count_ndisc):
			w1_0[:]  = w1_0[:] + w1_alpha[k]*RB[:,k];

		calibs_ind[it]=(detect_calib_ind(w1_0, with_calibration)+calibs_ind[it-1])%n

		if ((calibs_ind[it]-calibs_ind[it-1])!=0 and with_calibration==True):
			with_shift=1
	#				print 'ciao', calibs_ind[it]-calibs_ind[it-1]
			w1_0=interpolate(calibs_ind[it], inverse_interpolate(calibs_ind[it-1],w1_0,with_calibration), with_calibration)
			w1_alpha=np.reshape(so.solve(n,count_ndisc,RB[:,:count_ndisc],w1_0),(count_ndisc,1))
			for j in range(count_ndisc):
				w_ROM[:] = w_ROM[:]+ w1_alpha[j]*RB[:,j];
		else:
			w_ROM[:]=w1_0					

	#	print "un1", np.isnan(w_ROM).any()	
	#	plt.plot(w_ROM)
	#	plt.show();		
		tmp=np.dot(RB[magic_M1,:count_ndisc],RHS_alp)
		tmp2=(tmp-np.reshape(RHS_M1,np.shape(tmp)))*np.reshape(norm_fnc_M1,np.shape(tmp))
		err_ind  += CE**(Nt-it)*np.linalg.norm(tmp2,2);#dt is already inside RHS !!! I don't need to put it!
		if with_shift==0:
			err_ind += CE**(Nt-it)*np.linalg.norm(w1_alpha-w0_alpha+RHS_alp,1);
		true_err += np.sqrt(dx)*(np.linalg.norm(w_ROM-real_sol[:,it],2))/Nt

	#print w1_alpha,sum(w1_alpha)	
	t_fin=time.time() - t_init
	print "RB_time", t_fin
	print sum(w1_alpha>0.1)
	#print "mu=",mu
	print "true_ind=",true_err
	print "err_ind=",err_ind
#	plt.plot(inverse_interpolate(calibs_ROM_ind[ Nt], w_ROM, with_calibration), label="RB");
#	plt.plot(real_sol[:,Nt],label="real")
#	#for j in range(count_ndisc):
#	#	plt.plot(RB[:,j]);
#	plt.legend()
#	plt.show();
	return
