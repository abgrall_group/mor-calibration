__all__=["createInitialCondition", "solveFOM", "solveFOM_ref", "diffSolve", "diffSolve_ref","diffSolve_EIM","diffSolve_EIM_ref", "diffSolve_RB_EIM", "diffSolve_RB_EIM_ref", "diffSolve_RB_EIM_proj", "diffSolve_RB_EIM_proj_ref",  "rusanov", "maximum_vitesse",  "createInitialCondition_reference", "PhysicalProblem", "FullSolution", "SolutionManifold"] #"diffSolve_EIM", "diffSolve_RB_EIM","diffSolve_RB_EIM_proj" ,
import math
import numpy as np
from scipy.sparse import spdiags
#import solve as so
from geometry import *
from tests import *
import time
import matplotlib.pyplot as plt
from parameters import *
from joblib import Parallel, delayed
import multiprocessing



class PhysicalProblem:
  def __init__(self, geometry, ic_type):
    self.physical_parameters = PhysicalParameters(ic_type)
    self.geometry = geometry
    self.test = self.physical_parameters.equation
    self.ic_type = ic_type
    self.detec_type = self.geometry.geometry_parameters.detec_type
    self.ndof = self.geometry.n_dofs
    self.T = self.physical_parameters.T_fin
    self.CFL = self.physical_parameters.CFL

  def set_dt(self, param):
    dt = 10.**10.
    mus = param.param_domain
    for k in range(param.n_param):
      mu = mus[k,:]
      dt = min(dt, self.compute_dt(mu))
    Nt = int(self.T/dt)+1
    self.Nt = Nt
    self.dt = self.T/Nt
  def set_Nt(self, Nt):
    self.Nt = Nt
    self.dt = self.T/Nt
  def get_dt(self):
    return self.dt
  def get_Nt(self):
    return self.Nt

  def set_CE_error(self, CE):
    self.CE=CE

  def compute_dt(self, mu):
    u0, calib_index, calib = createInitialCondition(self, mu)
    min_dx= np.min(self.geometry.dx)
    maxJf = np.max(self.spectral_radius(u0,mu))
    return self.CFL*min_dx*(maxJf**-1)

  def flux(self, u, param):
    mu_flux=param[0]
    if self.test=="burgers":
      return mu_flux*u**2./2.
    elif self.test=="advection":
      return mu_flux*u
    else:
      raise ValueError("flux not defined for the test "+self.test)
      return ""

  def jacobian_flux(self,u,param):
    mu_flux=param[0]
    if self.test=="burgers":
      return mu_flux*u
    elif self.test=="advection":
      return mu_flux
    else:
      raise ValueError("flux not defined for the test "+self.test)
      return ""
  def spectral_radius(self,u,param):
    jac=self.jacobian_flux(u,param)
#    if np.size(jac)==1:
    return abs(jac)
#    else:
#      return max(abs(np.linalg.eigvals(jac)))

class FullSolution:
  def __init__(self, mu, physical):
    self.mu = mu
    self.physical = physical
    self.U = np.zeros((self.physical.geometry.n_dofs, self.physical.Nt+1))
    self.EIM_fun = np.zeros((self.physical.geometry.n_dofs, self.physical.Nt))
    self.calib_values  = np.zeros(self.physical.Nt+1)
  def set_solution(self, it, U_time):
    self.U[:,it] = U_time
  def set_calib_value(self,it, calib):
    self.calib_values[it] = calib
  def set_flux(self, it, flux_time):
    self.EIM_fun[:,it-1] = flux_time
  def get_solution_time(self, it):
    return self.U[:,it]
  def get_flux_time(self, it):
    return self.EIM_fun[:,it-1]
  def get_whole_solution(self):
    return U
  def get_whole_flux(self):
    return EIM_fun

class ReducedSolution:
  def __init__(self, nRB, mu, physical):
    self.mu = mu
    self.nRB = nRB
    self.Nt = physical.Nt
    self.alpha = np.zeros((self.nRB, self.Nt+1))
    self.calib_values = np.zeros((self.Nt+1))
    self.true_err = 100.
    self.err_ind = 100.
    self.final_error = 100.
  def set_solution(self, it, alpha):
    self.alpha[:,it] = alpha
  def get_solution_time(self, it):
    return alpha[:,it]
  def set_calib_value(self,it, calib):
    self.calib_values[it] = calib
  def get_whole_solution(self):
    return alpha
  def set_true_error(self, err):
    self.true_err = err
  def set_error_ind(self, err):
    self.err_ind = err
  def set_final_error(self, err):
    self.final_error = err



class SolutionManifold:
  def __init__(self, param, physical, ref = None):
    if ref is None:
      self.reference_flag  = False
    else:
      self.reference_flag = ref
    self.param = param
    self.physical = physical
    self.n =  self.physical.geometry.n_dofs
    self.num_fluxes = self.param.n_param * (self.physical.Nt)
    self.real_sol = []
    self.fluxes = np.zeros( (self.n, self.num_fluxes)  )
  def build_all_solutions(self, parallel = None, physical_or=None, original_manifold = None):
    print('Starting building all FOM solutions')
    num_cores = multiprocessing.cpu_count()
    if self.reference_flag == False:
      self.real_sol = Parallel(n_jobs=num_cores)(delayed(solveFOM)(self.physical,mu) for mu in self.param.param_domain)
    else:
      if physical_or is None or original_manifold is None:
        raise ValueError('You should pass the original physical and original manifold to compute the calibrated solutions')
      self.real_sol = Parallel(n_jobs=num_cores)(delayed(solveFOM_ref)(self.physical, physical_or, self.param.param_domain[cand], original_manifold.real_sol[cand].calib_values, self.physical.geometry.x) for cand in range(self.param.n_param))
    Nt = self.physical.Nt
    for cand in range(self.param.n_param):
      self.fluxes[:,cand*Nt:(cand+1)*Nt] = self.real_sol[cand].EIM_fun
    print('Finished building all FOM solutions')

class ReducedManifold:
  def __init__(self, param, physical, nRB):
    self.param = param
    self.physical = physical
    self.reduced_dimension = 0
    self.nRB = nRB
    self.reduced_sol = []
  def initialize_reduced_solutions(self):
    for k in range(param.n_param):
      self.reduced_sol.append(ReducedSolution(self.nRB, self.param.param_domain[k], self.physical))

#  def build_all_reduction_solutions(self, parallel=False, physical_or=None, original_manifold = None):
#    print('Starting building all FOM solutions')
#    num_cores = multiprocessing.cpu_count()
#    if self.reference_flag == False:
#      self.reduced_sol = Parallel(n_jobs=num_cores)(delayed(offline_step)(mu, w1_init_alpha, count_ndisc, truthN, RB, Nt, dx, dt, magic_pts, EIM_fun_RB, EIM_inv, magic_M1, param_domain, EIM_fun,M1_EIM,tol_greedy, norm_fnc_M1, CE, real_sol[:,:,mu], max_err_ind[count_M1-1], t_err[count_M1-1], all_points, calibs_ind, plot_yes[mu], minimization, with_calibration) for mu in range(max_nb_cand))
#    else:
#      if physical_or is None or original_manifold is None:
#        raise ValueError('You should pass the original physical and original manifold to compute the calibrated solutions')
#      self.real_sol = Parallel(n_jobs=num_cores)(delayed(solveFOM_ref)(self.physical, physical_or, self.param.param_domain[cand], original_manifold.real_sol [cand].calib_values) for cand in range(self.param.n_param))
#    Nt = self.physical.Nt
#    for cand in range(self.param.n_param):
#      self.fluxes[:,cand*Nt:(cand+1)*Nt] = self.real_sol[cand].EIM_fun
#    print('Finished building all FOM solutions')


#####################
def  createInitialCondition(physical, mu, calib=None):
  x = physical.geometry.x
  u0 = physical.physical_parameters.initialCondition(x,mu)
  if calib is None:
    calib_ind = detect_calib_ind(u0, physical.detec_type)
    calib = x[calib_ind]
  else:
    calib_ind = np.argmin(np.abs(x-calib))
  return u0, calib_ind, calib


def createInitialCondition_reference(physical_ref, physical_or, mu, calib=None, y=None):
   u0, calib_index, calib =createInitialCondition(physical_or, mu, calib)
   x = physical_or.geometry.x
   if y is None:
     y = transform_or2ref(x,calib,physical_or.geometry)
   v0_ref = interpolate_or2ref(x,y,calib, u0, physical_or.geometry)
   return y, v0_ref, calib

##########################################
def solveFOM(physical, param):
# solve the Burger problem with upwind first order scheme.
  Nt = physical.Nt
  solution = FullSolution(param, physical)
  u0, calib_index, calib = createInitialCondition(physical,param)
  solution.set_solution(0,u0)
  solution.set_calib_value(0,calib)
  x = physical.geometry.x
  for it in range(1,Nt+1):
     uu0=solution.get_solution_time(it-1)
     uu1,RHS=diffSolve(physical,uu0,param)
     calib_index=detect_calib_ind(uu1, physical.detec_type, calib_index)
     solution.set_solution(it,uu1)
     solution.set_calib_value(it,x[calib_index])
     solution.set_flux(it,RHS)
  return solution


def solveFOM_ref(physical_ref, physical_or, param, calibs, y=None):
# solve the Burger problem with upwind first order scheme.
  Nt = physical_or.Nt
  solution = FullSolution(param, physical_ref)
  if y is None:
    y, v0, calib = createInitialCondition_reference(physical_ref, physical_or,param, calibs[0])
  else:
    y, v0, calib = createInitialCondition_reference(physical_ref, physical_or,param, calibs[0], y)
  solution.set_solution(0,v0)
  solution.set_calib_value(0,calibs[0])
  for it in range(1,Nt+1):
     solution.set_calib_value(it, calibs[it])
     uu0=solution.get_solution_time(it-1)
     uu1,RHS=diffSolve_ref(physical_ref,physical_or, uu0,param, calibs[it-1], calibs[it])
     solution.set_solution(it,uu1)
     solution.set_flux(it,RHS)
  return solution


#############################################
def diffSolve(physical, w0, param):
# on resoud le schema machin
   dt = physical.dt
   dx = physical.geometry.dx
   check_CFL(w0, param, physical)
   n = physical.geometry.n_dofs
   w1=np.zeros(n)
   RHS=np.zeros(n)
   for i in range(n):
      jm1, j, jp1 = physical.geometry.get_neighboorhood(i)
      fm=rusanov(w0[jm1],w0[j],param, physical)
      fp=rusanov(w0[j],w0[jp1],param, physical)
      RHS[i]=dt*( fp/dx[j]-fm/dx[jm1])
      w1[i]=w0[i] -RHS[i]
   #print "dt    ==== ",dt
   return w1, RHS

#############################################
def diffSolve_ref(physical_ref, physical_or, w0, param, calib_p, calib_a):
# on resoud le schema machin
   dt = physical_ref.dt
   dy = physical_ref.geometry.dx
   y = physical_ref.geometry.x
   x = physical_or.geometry.x
   n = physical_ref.geometry.n_dofs
   warn=check_CFL(w0, param, physical_or)
   w1=np.zeros(n)
   wstar=np.zeros(n)
   RHS=np.zeros(n)
   RHS2=np.zeros(n)
   dcalib = derivative_calib(calib_a, calib_p, physical_or.geometry)

   for i in range(n):
      jm1, j, jp1 = physical_ref.geometry.get_neighboorhood(i)
      fm=rusanov(w0[jm1],w0[j],param, physical_ref)
      fp=rusanov(w0[j],w0[jp1],param, physical_ref)
      jac = jacobian_or2ref(x[i], calib_a, physical_or.geometry)
      RHS[i]=dt*( fp/dy[j]-fm/dy[jm1]) * jac
      # wstar[i]=w0[i] -RHS[i]
      nct_j = ddcalib_trasform_ref2or(y[j],calib_a, physical_or.geometry)*dcalib*jac
      RHS2[i]= downwind_du(w0[jm1],w0[j],w0[jp1], nct_j, dy[jm1],dy[j],param, physical_ref)
      w1[i]=w0[i] -RHS[i]+RHS2[i]

#    for i in range(n):  #this gives the exact solution, but more expensive
#       jm1, j, jp1 = physical_ref.geometry.get_neighboorhood(i)
#       nct_j = ddcalib_trasform_ref2or(y[j],calib_a, physical_or.geometry)*dcalib*jac
#       nct_jm1 = ddcalib_trasform_ref2or(y[jm1],calib_a, physical_or.geometry)*dcalib*jac
#       nct_jp1 = ddcalib_trasform_ref2or(y[jp1],calib_a, physical_or.geometry)*dcalib*jac
# ##      ncfm = non_conservative_rusanov(w0[jm1], w0[j], param, physical_ref)
# ##      ncfp = non_conservative_rusanov(w0[j], w0[jp1], param, physical_ref)
# ##      RHS2[i] = (nct_j+nct_jp1)/2.*ncfp/dy[j] - (nct_j+nct_jm1)/2.*ncfm/dy[jm1]
#
#       RHS2[i]= downwind_du(wstar[jm1],wstar[j],wstar[jp1], nct_j, dy[jm1],dy[j],param, physical_ref)  #this gives the exact solution, but more expensive
#       w1[i]=w0[i] -RHS[i]+RHS2[i]
   if warn:
     plt.plot(w0)
     plt.plot(w1)
     plt.show()

   #print "dt    ==== ",dt
   return w1, RHS-RHS2
##############################################
def diffSolve_EIM(physical,w0,EIM, param):
# on resoud le schema machin
   dt = physical.dt
   dx = physical.geometry.dx
   check_CFL(w0, param, physical, EIM.magic_pts)
   n = physical.geometry.n_dofs
   RHS=np.zeros(n)
   for i in EIM.magic_pts:
      jm1, j, jp1 = physical.geometry.get_neighboorhood(i)
      fm=rusanov(w0[jm1],w0[j],param,physical)
      fp=rusanov(w0[j],w0[jp1],param,physical)
      RHS[i]=dt*( fp/dx[j]-fm/dx[jm1])
   beta=np.linalg.solve(EIM.EIM_fun[EIM.magic_pts,:],RHS[EIM.magic_pts])
   RHS= np.dot(EIM.EIM_fun,beta)
   w1=w0 -RHS
   #print "dt    ==== ",dt
   return w1,RHS

def diffSolve_EIM_ref(physical_ref, physical_or,w0,EIM, param, calib_p, calib_a):
# on resoud le schema machin
   dt = physical_ref.dt
   dy = physical_ref.geometry.dx
   y = physical_ref.geometry.x
   x = physical_or.geometry.x
   n = physical_ref.geometry.n_dofs
   check_CFL(w0, param, physical_or,EIM.magic_pts)
   w1=np.zeros(n)
   wstar=np.zeros(n)
   RHS=np.zeros(n)
   dcalib = derivative_calib(calib_a, calib_p, physical_or.geometry)

   for i in EIM.magic_pts:
      jm1, j, jp1 = physical_ref.geometry.get_neighboorhood(i)
      fm=rusanov(w0[jm1],w0[j],param,physical_ref)
      fp=rusanov(w0[j],w0[jp1],param,physical_ref)
      jac = jacobian_or2ref(x[i], calib_a, physical_or.geometry)
      RHS[i]=dt*( fp/dy[j]-fm/dy[jm1]) * jac
      nct_j = ddcalib_trasform_ref2or(y[j],calib_a, physical_or.geometry)*dcalib*jac
      RHS[i]= RHS[i]- downwind_du(w0[jm1],w0[j],w0[jp1], nct_j, dy[jm1],dy[j],param, physical_ref)

   beta=np.linalg.solve(EIM.EIM_fun[EIM.magic_pts,:],RHS[EIM.magic_pts])
   RHS= np.dot(EIM.EIM_fun,beta)
   w1=w0 -RHS

   #print "dt    ==== ",dt
   return w1,RHS

########################################################
def diffSolve_RB_EIM(physical,w0,w0_alp,EIM,RB,minimization, param):
# on resoud le schema machin
   dt = physical.dt
   dx = physical.geometry.dx
   check_CFL(w0, param, physical,EIM.magic_pts)
   n = physical.geometry.n_dofs
   RHS=np.zeros(n)
   for i in EIM.magic_pts:
      jm1, j, jp1 = physical.geometry.get_neighboorhood(i)
      fm=rusanov(w0[jm1],w0[j],param,physical)
      fp=rusanov(w0[j],w0[jp1],param,physical)

      RHS[i]=dt*( fp/dx[j]-fm/dx[jm1])
   beta=np.dot(EIM.EIM_inv,RHS[EIM.magic_pts])
   RHS= np.dot(EIM.EIM_fun,beta)
   RHS_alp=so.solve(minimization,np.shape(RHS)[0],np.shape(RB.RB)[1],RB.RB,RHS)
   w1_alp=w0_alp -RHS_alp
   #print "dt    ==== ",dt
   return w1_alp, RHS

def diffSolve_RB_EIM_ref(physical_ref, physical_or,w0,w0_alp, EIM,RB,minimization, param, calib_p, calib_a):
# on resoud le schema machin
   dt = physical_ref.dt
   dy = physical_ref.geometry.dx
   y = physical_ref.geometry.x
   x = physical_or.geometry.x
   n = physical_ref.geometry.n_dofs
   check_CFL(w0, param, physical_or, EIM.magic_pts)
   w1=np.zeros(n)
   wstar=np.zeros(n)
   RHS=np.zeros(n)
   dcalib = derivative_calib(calib_a, calib_p, physical_or.geometry)

   for i in EIM.magic_pts:
      jm1, j, jp1 = physical_ref.geometry.get_neighboorhood(i)
      fm=rusanov(w0[jm1],w0[j],param,physical_ref)
      fp=rusanov(w0[j],w0[jp1],param,physical_ref)
      jac = jacobian_or2ref(x[i], calib_a, physical_or.geometry)
      RHS[i]=dt*( fp/dy[j]-fm/dy[jm1]) * jac
      nct_j = ddcalib_trasform_ref2or(y[j],calib_a, physical_or.geometry)*dcalib*jac
      RHS[i]= RHS[i]- downwind_du(w0[jm1],w0[j],w0[jp1], nct_j, dy[jm1],dy[j],param, physical_ref)

   beta=np.dot(EIM.EIM_inv,RHS[EIM.magic_pts])
   RHS= np.dot(EIM.EIM_fun,beta)
   RHS_alp=so.solve(minimization,np.shape(RHS)[0],np.shape(RB.RB)[1],RB.RB,RHS)
   w1_alp=w0_alp -RHS_alp
   #print "dt    ==== ",dt
   return w1,RHS
##############################################
def diffSolve_RB_EIM_proj(physical,w0,w0_alp,EIM, RB, param):
# on resoud le schema machin
   dt = physical.dt
   dx = physical.geometry.dx
   check_CFL(w0, param, physical,EIM.magic_pts)
   n = physical.geometry.n_dofs
   RHS=np.zeros(n)
   for i in EIM.magic_pts:
      jm1, j, jp1 = physical.geometry.get_neighboorhood(i)
      fm=rusanov(w0[jm1],w0[j],param,physical)
      fp=rusanov(w0[j],w0[jp1],param,physical)

      RHS[i]=dt*( fp/dx[j]-fm/dx[jm1])

   RHS_M1= np.zeros((np.shape(EIM.magic_M1)[0],1))
   k=0
   for i in EIM.magic_M1:
      jm1, j, jp1 = physical.geometry.get_neighboorhood(i)
      fm=rusanov(w0[jm1],w0[j],param,physical)
      fp=rusanov(w0[j],w0[jp1],param,physical)

      RHS_M1[k,0]=dt*( fp/dx[j]-fm/dx[jm1])
      k +=1

   RHS_alp_EIM=np.dot(EIM.EIM_inv,np.reshape(RHS[EIM.magic_pts],(np.shape(EIM.magic_pts)[0],1)))
   RHS_alp = np.reshape(np.dot(RB.EIM_fun_RB,RHS_alp_EIM), np.shape(w0_alp))

   w1_alp=w0_alp -RHS_alp

   return w1_alp, RHS_alp, RHS_M1

def diffSolve_RB_EIM_proj_ref(physical_ref, physical_or,w0,w0_alp, EIM, RB, param, calib_p, calib_a):
# on resoud le schema machin
   dt = physical_ref.dt
   dy = physical_ref.geometry.dx
   y = physical_ref.geometry.x
   x = physical_or.geometry.x
   n = physical_ref.geometry.n_dofs
   warn=check_CFL(w0, param, physical_or,EIM.magic_pts)
   w1=np.zeros(n)
   wstar=np.zeros(n)
   RHS=np.zeros(n)
   dcalib = derivative_calib(calib_a, calib_p, physical_or.geometry)

   for i in EIM.magic_pts:
      jm1, j, jp1 = physical_ref.geometry.get_neighboorhood(i)
      fm=rusanov(w0[jm1],w0[j],param,physical_ref)
      fp=rusanov(w0[j],w0[jp1],param,physical_ref)
      jac = jacobian_or2ref(x[i], calib_a, physical_or.geometry)
      RHS[i]=dt*( fp/dy[j]-fm/dy[jm1]) * jac
      nct_j = ddcalib_trasform_ref2or(y[j],calib_a, physical_or.geometry)*dcalib*jac
      RHS[i]= RHS[i]- downwind_du(w0[jm1],w0[j],w0[jp1], nct_j, dy[jm1],dy[j],param, physical_ref)

   RHS_M1= np.zeros((np.shape(EIM.magic_M1)[0],1))
   k=0
   for i in EIM.magic_M1:
      jm1, j, jp1 = physical_ref.geometry.get_neighboorhood(i)
      fm=rusanov(w0[jm1],w0[j],param,physical_ref)
      fp=rusanov(w0[j],w0[jp1],param,physical_ref)
      jac = jacobian_or2ref(x[i], calib_a, physical_or.geometry)
      RHS_M1[k,0]=dt*( fp/dy[j]-fm/dy[jm1]) * jac
      nct_j = ddcalib_trasform_ref2or(y[j],calib_a, physical_or.geometry)*dcalib*jac
      RHS_M1[k,0]= RHS_M1[k,0]- downwind_du(w0[jm1],w0[j],w0[jp1], nct_j, dy[jm1],dy[j],param, physical_ref)
      k =k+1

   RHS_alp_EIM=np.dot(EIM.EIM_inv,np.reshape(RHS[EIM.magic_pts],(np.shape(EIM.magic_pts)[0],1)))
   RHS_alp = np.reshape(np.dot(RB.EIM_fun_RB,RHS_alp_EIM), np.shape(w0_alp))

   w1_alp=w0_alp -RHS_alp

   return w1_alp, RHS_alp, RHS_M1

#############################################

def upwind_du(a,b,c,wind,dxab, dxbc,param,physical):
  if abs(wind)>10.**-15:
    fp=max(wind,0)/abs(wind)
    fm=min(wind,0)/abs(wind)
  else:
    fp=0.
    fm=0.
  flux = fp*(b-a)/dxab+fm*(c-b)/dxbc
  return flux

def downwind_du(a,b,c,wind,dxab, dxbc,param,physical):
  fp=max(wind,0)
  fm=min(wind,0)
  flux = fm*(b-a)/dxab+fp*(c-b)/dxbc
  return flux

def symmetric_du(a,b,c,dxab, dxbc,param,physical):
#  f=physical.flux(b,param)
#  fp=max(f,0)/abs(f)
#  fm=min(f,0)/abs(f)
  flux = 0.5*((b-a)/dxab+(c-b)/dxbc)  - ((b-a)/dxab-(c-b)/dxbc)
  return flux

def non_conservative_rusanov(a,b,param, physical):
   lam = np.max([a,b])
   flux=0.5*(a+b) - lam*(b-a) #coef*0.5*( 0.5*(a*a+b*b)-abs(a+b)*0.5*(b-a))
   return flux


def rusanov(a,b,param, physical):
#   if np.size(param)==3:
#     coef=param[2]
#   else:
#     coef=1.
   fa=physical.flux(a,param)
   fb = physical.flux(b,param)
   lam = np.max([physical.spectral_radius(a,param),physical.spectral_radius(b,param)])
   flux=0.5*(fa+fb) - lam*(b-a) #coef*0.5*( 0.5*(a*a+b*b)-abs(a+b)*0.5*(b-a))
   return flux
def maximum_vitesse(w, param, physical):
# I compute the maximum possible speed for all the possible samples.
# doing so, I am sure that all the samples have the same chronology.
# otherwise, the time stepping relies on the CFL only, so we do not
# have the same chronology
    maxi=0.
    for i in range(len(w)):
      maxi=max(maxi,physical.jacobian_flux(w[i],param))
    return maxi

def check_CFL(w,param, physical, pts=None):
  problem = 0
  if pts is None:
    pts = range(len(w))
  for i in pts:
    umax=abs(physical.jacobian_flux(w[i],param))
    if umax*physical.dt>physical.geometry.dx[i]:
      problem = 1
  if problem==1:
    print("Warning: CFL not satisfied!!!")
    print("Parameter "+ str(param))
  return problem
###########################################
