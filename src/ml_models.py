import keras
from keras.models import Sequential
from keras.layers import Input
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.preprocessing import normalize, RobustScaler
import numpy as np
import matplotlib.pyplot as plt
import pickle
import sklearn.model_selection
import pandas as pd
import time
from bibli import *
from geometry import *


class BasicNeuralModel(object):
    # problem with Keras and TF interface
    def __init__(self,ndim):
        self.model = keras.models.Sequential()
        self.model.add(keras.layers.core.Dense(8, input_dim=ndim, activation='tanh'))
        # model.add(keras.layers.core.Dropout(rate=0.5))
        self.model.add(keras.layers.normalization.BatchNormalization())
        self.model.add(keras.layers.core.Dense(8, activation='tanh'))
        # model.add(keras.layers.core.Dropout(rate=0.5))
        self.model.add(keras.layers.normalization.BatchNormalization())
        self.model.add(keras.layers.core.Dense(8, activation='tanh'))
        self.model.add(keras.layers.normalization.BatchNormalization())
        self.model.add(keras.layers.core.Dense(8, activation='tanh'))
        self.model.add(keras.layers.normalization.BatchNormalization())
        self.model.add(keras.layers.core.Dense(8, activation='tanh'))
        # self.model.add(keras.layers.normalization.BatchNormalization())
        # self.model.add(keras.layers.core.Dense(8, activation='tanh'))
        # self.model.add(keras.layers.normalization.BatchNormalization())
        # self.model.add(keras.layers.core.Dense(8, activation='tanh'))
        # self.model.add(keras.layers.normalization.BatchNormalization())
        # self.model.add(keras.layers.core.Dense(8, activation='tanh'))
        # self.model.add(keras.layers.normalization.BatchNormalization())
        # self.model.add(keras.layers.core.Dense(8, activation='tanh'))
        # self.model.add(keras.layers.normalization.BatchNormalization())
        # self.model.add(keras.layers.core.Dense(8, activation='tanh'))
        # model.add(keras.layers.core.Dropout(rate=0.5))
        self.model.add(keras.layers.core.Dense(1,   activation='linear'))
        self.model.compile(loss="mean_squared_error", optimizer="adam") #,metrics=["accuracy"])

        # Use Early-Stopping
        # self.callback_early_stopping = keras.callbacks.EarlyStopping(monitor='val_loss', patience=30, verbose=0, mode='auto')

    def summary(self):
        print(self.model.summary())

    def fit(self,x_train,y_train,x_valid,y_valid,batch_size=1024,epochs=100):
        self.model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs,\
        validation_data=(x_valid, y_valid), verbose=1)#, callbacks=[self.callback_early_stopping])

    def predict(self,x):
        #x = normalize(x,axis=0)
        res_p = self.model.predict(x)
        return res_p

class BasicRecurrentModel(object):
    # problem with Keras and TF interface
    def __init__(self):
        self.model = keras.models.Sequential()

        self.model.compile(loss="mean_squared_error", optimizer="adam") #,metrics=["accuracy"])

        # Use Early-Stopping
        self.callback_early_stopping = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10, verbose=0, mode='auto')


    def summary(self):
        print(self.model.summary())

    def fit(self,x_train,y_train,x_valid,y_valid,batch_size=1024,epochs=50):
        model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs,\
        validation_data=(x_valid, y_valid), verbose=1, callbacks=[self.callback_early_stopping])

    def predict(self,x):
        #x = normalize(x,axis=0)
        res_p = self.model.predict(x)
        return res_p


class NNRegressionMap:
    # define a NN regression map, given some parameters. One can easily train and test
    def __init__(self, ndim): #ndim are the input dimensions
        self.time_training=0.
        self.model=BasicNeuralModel(ndim)

    def train(self, x_train, y_train,x_valid,y_valid, epochs=20):
        t_init=time.time()
        self.model.fit(x_train,y_train,x_valid,y_valid, epochs=epochs)
        self.time_training=self.time_training+time.time()-t_init

    def validate(self,x_valid, y_valid):
        y_predict = self.predict(x_valid)
        error= np.reshape(y_valid,np.shape(y_predict))-y_predict
        self.mean_error = np.mean(np.abs(error))
        self.max_error = np.max(np.abs(error))
        return self.mean_error, self.max_error

    def predict(self, x_test):
        y_predict=self.model.predict(x_test)
        return y_predict