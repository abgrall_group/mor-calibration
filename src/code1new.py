import numpy as np
import matplotlib.pyplot as plt

import pickle

import pandas as pd
from bibli import *
from geometry import *
from datetime import datetime
import sys

Mstop           = 6
rate_red   = 0.0001 
epoch_red  = 100
batch_size      = 100
ic_type=0
fname='test'+str(ic_type)+'/' #/non_rd/'
folder_calibs = fname+'calibs'

with open(folder_calibs+'/calib_points_mollified', 'rb') as problem: #
    param_domain = pickle.load(problem)
    physical = pickle.load(problem)
    calibs = pickle.load(problem)
print(calibs)
times=np.arange(0.,physical.T+physical.dt, physical.dt)
[n_param, dim_param] = np.shape(param_domain)

x_dim = dim_param+1 #time
#print(calibs)
#print(len(calibs))
#print(physical.Nt)
for par in range(n_param):
    for it in range(len(calibs)):
        if calibs[it,par]<0:
            print(2)
n_data = (physical.Nt+1)*n_param
#print(n_data)
#print(physical.Nt+1)
#print(n_param)
for par in range(n_param):
    adjust_calibs=np.zeros(np.shape(calibs[:,par]))
    shift=0.
    thres=physical.geometry.size_domain/2.
    adjust_calibs[0]=calibs[0,par]
    for k in range(1,len(calibs)):
      if calibs[k,par]-calibs[k-1,par]>thres:
        shift=shift-physical.geometry.size_domain
      elif calibs[k,par]-calibs[k-1,par]<-thres:
        shift=shift+physical.geometry.size_domain
      adjust_calibs[k]=calibs[k,par]+shift
    calibs[:,par]=adjust_calibs

print(calibs)

x_all  =np.zeros((n_data,x_dim))
y_all = np.zeros(n_data)

n_param_train=int(n_param*0.6)
n_param_valid=int(n_param*0.2)

n_param_test = n_param- n_param_train-n_param_valid
#print(n_param_train)

x_train  =np.zeros(( n_param_train,physical.Nt+3))
y_train = np.zeros((n_param_train,physical.Nt))

x_valid  =np.zeros(( n_param_valid,physical.Nt+3))
y_valid = np.zeros(( n_param_valid,physical.Nt))

x_test  =np.zeros(( n_param_test,physical.Nt+3))
y_test = np.zeros((n_param_test,physical.Nt))

z=0
#print(param_domain)
for par in range(n_param_train):
    for it in range(physical.Nt):
        x_train[z,physical.Nt:physical.Nt+3]=param_domain[par,:]
        x_train[z,it] = calibs[it,par]
        y_train[z,it] = calibs[it+1,par]
    z=z+1
z=0

for par in range(n_param_train, n_param_train+ n_param_valid):
    for it in range(physical.Nt):
        x_valid[z,physical.Nt:physical.Nt+3]=param_domain[par,:]
        x_valid[z,it] = calibs[it,par]
        y_valid[z,it] = calibs[it+1,par]
    z=z+1

z=0

for par in range(n_param_train+ n_param_valid, n_param_train+ n_param_valid + n_param_test):
    for it in range(physical.Nt):
        x_test[z,physical.Nt:physical.Nt+3]=param_domain[par,:]
        x_test[z,it] = calibs[it,par]
        y_test[z,it] = calibs[it+1,par]
    z=z+1



#
# x_train, x_valid, y_train, y_valid = sklearn.model_selection.train_test_split(x_all, y_all, test_size=0.2, random_state=0)
# x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(x_train, y_train, test_size=0.2, random_state=0)
# print(x_train.shape[1])
alpha=0.1
def leaky_relu(alpha, x):
 if x>=0:
  return x
 else:
  return alpha * x


def derive_leaky_relu(alpha, x):
 if x>=0:
  return 1
 else:
  return alpha



hidden_dim=8
nepoch=10
idxs = np.arange(n_param_train)
min_val_err  = 1e10
val_err      = np.zeros((nepoch))
train_cost   = np.zeros((nepoch))
Stopped_at   = nepoch*np.ones((1,1))


    

class RNNNumpy:
    
    def __init__(self, x_dim, hidden_dim=8, bptt_truncate=4):        
        self.x_dim = x_dim
        self.hidden_dim = hidden_dim
        self.bptt_truncate = bptt_truncate
        #print(self.x_dim)
        self.U = np.random.uniform(-np.sqrt(1./4), np.sqrt(1./4), (hidden_dim, 4))
        self.V = np.random.uniform(-np.sqrt(1./hidden_dim), np.sqrt(1./hidden_dim), (1, hidden_dim))
        self.W = np.random.uniform(-np.sqrt(1./hidden_dim), np.sqrt(1./hidden_dim), (hidden_dim, hidden_dim))
        #print(self.U.shape)
        #print(self.V.shape)
        #print(self.W.shape)
        
    def forward_propagation(self, x):
        #print(x)
        
                  
        s = np.zeros((physical.Nt+1,hidden_dim))
        s[-1] = np.zeros(hidden_dim)
        o = np.zeros((physical.Nt,1))       
        z=[]
        z.append(x[0])
        z.append(x[-3])
        z.append(x[-2])
        z.append(x[-1])
        for t in np.arange(physical.Nt):
            
            s[t] = 1/(1+np.exp(-(self.U.dot(z) + self.W.dot(s[t-1]))))
            o[t] = (self.V.dot(s[t]))
            z=[]
            z.append(o[t])
            z.append(x[-3])
            z.append(x[-2])
            z.append(x[-1])
        return [o, s]


    def calculate_loss(self, x, y):
        L = 0  
        for i in np.arange(len(y)):
            #print(len(y))
            o, s = self.forward_propagation(x[i])
            L = L +np.sum(np.square((np.reshape(y[i,:],o.shape)-o)))
        return L/(2*len(y))


    def bptt(self, x, y):
        T = len(y)
        o, s = self.forward_propagation(x)
        dLdU = np.zeros(self.U.shape)
        dLdV = np.zeros(self.V.shape)
        dLdW = np.zeros(self.W.shape)
        delta_o = (o-np.reshape(y,o.shape))/len(y)
        #print(delta_o.shape)
        #print(o.shape)
        #print(y.shape)
        #print(y)
        for t in np.arange(T)[::-1]:
            #print(delta_o[t])
            #print(delta_o[t].shape)
            #print(s[t].T.shape)
            dLdV = dLdV+np.outer(delta_o[t], s[t].T)
            # Initial delta calculation

            #delta_t = self.V.T.dot(delta_o[t]) * (1 - (s[t] ** 2))
            delta_t = self.V.T.dot(delta_o[t]) *s[t]* (1 - (s[t]))
            #delta_t = self.V.T.dot(delta_o[t]) *derive_leaky_relu(alpha,s[t])

            # Backpropagation through time (for at most self.bptt_truncate steps)
            for bptt_step in np.arange(max(0, t-self.bptt_truncate), t+1)[::-1]:
                dLdW = dLdW +np.outer(delta_t, s[bptt_step-1])              
                dLdU = dLdU +np.outer(delta_t,x[bptt_step])
                # Update delta for next step

                #delta_t = self.W.T.dot(delta_t) * (1 - s[bptt_step-1] ** 2)
                delta_t = self.W.T.dot(delta_t) * s[bptt_step-1]*(1 - s[bptt_step-1] )
                #delta_t = self.W.T.dot(delta_t) * derive_leaky_relu(alpha,s[bptt_step-1])

        return [dLdU, dLdV, dLdW]


    def sgd_step(self, x, y, learning_rate):
        dLdU, dLdV, dLdW = self.bptt(x, y)
        self.U = self.U -learning_rate * dLdU
        self.V = self.V -learning_rate * dLdV
        self.W = self.W -learning_rate * dLdW
        return [self.U,self.V,self.W]

def train_with_sgd( model,x_train, y_train,x_valid,y_valid, learning_rate=0.08):
    losses = []
    t = 0
    val_err_old       = 1e10
    val_err_train     = 0    
    M                 = 0   
    verr_avg0         = 0
    verr_avg1         = 0
    for epoch in range(nepoch):
        total_batch = int(np.ceil(n_param_train/batch_size))
            
        np.random.shuffle(idxs)
        c_ind       = 0
        for i in range(total_batch):
            c_ind_next = np.minimum(c_ind+batch_size,n_param_train) 
            bidxs = idxs[c_ind:c_ind_next]
            batch_x = x_train[bidxs,:]
            batch_y = y_train[bidxs,:]           
            train_cost[epoch]=train_cost[epoch]+ model.calculate_loss(batch_x, batch_y)
            #losses.append((t, loss))


            #if (len(losses) > 1 and losses[-1][1] > losses[-2][1]):
            #    learning_rate = learning_rate * 0.8  
                #print "Setting learning rate to %f" % learning_rate
            #sys.stdout.flush()
            c_ind     = c_ind_next 
        if((epoch+1)%epoch_red == 0):
            current_rate = initial_rate/(1.0 + rate_red*epoch)
                
            
        #time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        #print "%s: Loss at epoch=%d: %f" % (time, epoch,train_cost[epoch])

        for i in range(len(batch_y)):
            
                
            U,V,W=model.sgd_step(batch_x[i], batch_y[i], learning_rate)

            #print(U)
        # Evaluating Validation Error (need to do it batch wise to avoid memory issues)
        total_batch = int(np.ceil(n_param_valid/batch_size))
        c_ind       = 0
           
           
        # Loop over all batches
        for i in range(total_batch):
            c_ind_next = np.minimum(c_ind+batch_size,n_param_valid)
            bidxs = range(c_ind,c_ind_next)
            batch_x = x_valid[bidxs,:]
                
                
            batch_y = y_valid[bidxs,:]           
            val_err[epoch] = val_err[epoch]+model.calculate_loss(batch_x, batch_y)
            c_ind = c_ind_next
            
            
        if(epoch == 0):
            verr_avg0 = val_err[0]
            verr_avg1 = verr_avg0
        else:
            verr_avg1 = np.mean(val_err[0:epoch])
            
        print( "Sys_time = %s:   ""Epoch = %04d   ""val_acc = %.8f%%    " "train_acc = %.5f%%    "%(str(datetime.now().time()),epoch+1,val_err[epoch], train_cost[epoch]) )

        if(val_err[epoch] < val_err_old):
           print( " .......... Model saved in checkpoint file epoch= %s"%(epoch))
              
        else:
           print( " .......... Validation Error has increased!! No checkpoint file saved.")
               
        val_err_old = val_err[epoch]  
            
        if(verr_avg1 > verr_avg0):
           M = M + 1
        else:
           M = 0
                
        if(M==Mstop):
           print( "Moving average of validation error has increased %d consecutive times! Stopping training" %(Mstop))
           Stopped_at = epoch
           break         
           
        verr_avg0 = verr_avg1
    
    print(U)


#RNNNumpy.forward_propagation = forward_propagation
#RNNNumpy.calculate_total_loss = calculate_total_loss
#RNNNumpy.bptt = bptt
#RNNNumpy.sgd_step = numpy_sdg_step


np.random.seed(98)
model = RNNNumpy(physical.Nt)
#print(x_train.shape)
losses = train_with_sgd(model,x_train, y_train,x_valid,y_valid)


y_test_predict=np.zeros((n_param_test,1))

o, s = model.forward_propagation(x_test[0])
#for i in np.arange(len(y_test)):
           
    #o, s = model.forward_propagation(x_test[i])
    #y_test_predict[i]=o[-1]

error=np.reshape(y_test[:,-1],np.shape(y_test_predict[:,-1]))-y_test_predict[:,-1]
print("square mean error on test is ", np.mean(np.abs(error)**2))
print("mean error on test is ", np.sqrt(np.mean(np.abs(error)**2)))

#plt.plot(y_test_predict,y_test[:,-1],'r.')
plt.plot(o,y_test[0,:],'r.')
x = np.linspace(y_test_predict[0], y_test_predict[-1], n_param_test, endpoint=False)
print(y_test_predict[0])
print(y_test_predict[-1])
print(y_test[0,-1])
print(y_test[-1,-1])
plt.plot([min(y_test_predict),max(y_test_predict)],[min(y_test_predict),max(y_test_predict)],'k-')
plt.plot([min(y_test[:,-1]),max(y_test[:,-1])],[min(y_test[:,-1]),max(y_test[:,-1])],'b-')
#plt.plot(x,x, )
#ii=np.arange(n_param_test)
#plt.plot(ii,y_test[:,-1],'r.')
#plt.plot(ii,y_test_predict[:,-1],'b.')
plt.show()







    
        
















