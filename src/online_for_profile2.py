import numpy as np
import random
from cvxopt import matrix
import matplotlib.pyplot as plt
from numpy import linalg as LA
import shutil, os
import math
import solve as so
from bibli import *
from minL1 import *
from GS import *
from EIM import *
import time
from geometry import *
from online_func import *
import pickle

def online_profile():
	###########################################
	###Choose the desired minimization type

	#minimization = 'L1min_LP'
	#minimization = 'L1min_IRLS'
	#minimization  = 'Hubermin_IRLS'
	minimization = 'L2'
	#minimization  = 'Hull'


	with_calibration=True

	folder='data_3par_big'
#	folder="data"
	#folder="data_"+str(dim_par)+"_param"

	###############################################################################
	## DEFINE PDE Problem Parameters !

	problem_data=np.load(folder+'/problem_data.npz')

	truthN=problem_data['arr_0']
	CFL=problem_data['arr_1']
	Nt=problem_data['arr_2']
	dx=problem_data['arr_3']
	dt=problem_data['arr_4']
	CE=problem_data['arr_5']
	x=problem_data['arr_6']
	T_fin=problem_data['arr_7']
	interval_min=[problem_data['arr_8']]
	interval_max=[problem_data['arr_9']]

	n=truthN

	dim_par=np.size(interval_max)

	#Import EIM and RB


	EIM_fun = np.load(folder+'/fun_EIM.npy')
	magic_pts = np.load(folder+'/magic_pts.npy')
	magic_M1=np.load(folder+'/magic_M1.npy')
	fnc_M1=np.load(folder+'/fnc_M1.npy')
	EIM_fun_RB = np.load(folder+'/EIM_fun_RB.npy')
	M1_EIM = np.shape(fnc_M1)[0]

	print np.shape(EIM_fun)
	print np.shape(EIM_fun_RB)


	norm_fnc_M1=np.linalg.norm(fnc_M1,2,axis=0)/np.sqrt(truthN)
	EIM_dim=np.shape(EIM_fun)[1]
	EIM_inv=np.linalg.inv(EIM_fun[magic_pts,:])
	RB = np.load (folder+'/RB.npy')
	print np.shape(RB)

	count_ndisc = np.shape(EIM_fun_RB)[0]
	RB=RB[:,:count_ndisc]
	
	if with_calibration:
		file_shift=open(folder+'/shift_basis_data.pkl','rb')
		proj_shift_basis_coeff=pickle.load(file_shift)
		proj_shift_basis=pickle.load(file_shift)
		err_proj_shift_basis=pickle.load(file_shift)
		map_shifts=pickle.load(file_shift)
		file_shift.close()

	###############################


	param = 0.5763

	err_ind=0
	true_err=0
	print "Computing real solution"
	u0      = np.zeros(truthN);
	U       = np.zeros((truthN,Nt+1));
	calibs_ind=np.zeros((Nt+1), dtype=np.int)
	calibs_ROM_ind=np.zeros((Nt+1), dtype=np.int)

	t_init=time.time()
	u0, calibs_ind[0] =createInitialCondition(truthN,param,x, with_calibration)
	U_not_calib,U_flux, calib_index = solveFOM(truthN,Nt,u0,dx,dt, with_calibration, param);
	calibs_ind[1:]=calib_index
	for it in range(0,Nt+1):
		U[:,it]=interpolate(calibs_ind[it],U_not_calib[:,it], with_calibration)
	t_fin=time.time() - t_init
	print "time_truthN", t_fin
	real_sol          	= U;
	#print "u0", np.isnan(u0).any()

	w_ROM, calib_ROM_index, err_ind, true_err, t_fin= online(param, truthN, Nt,dt,dx, CFL, CE, minimization,x, RB, EIM_fun_RB, magic_pts,EIM_inv, magic_M1 ,norm_fnc_M1, M1_EIM , U, with_calibration, proj_shift_basis_coeff, proj_shift_basis, err_proj_shift_basis, map_shifts)

	#print w1_alpha,sum(w1_alpha)	
	print "RB_time", t_fin
	#print "mu=",mu
	print "true_ind=",true_err
	print "err_ind=",err_ind
#	plt.plot(inverse_interpolate(calibs_ROM_ind[ Nt], w_ROM, with_calibration), label="RB");
#	plt.plot(real_sol[:,Nt],label="real")
#	#for j in range(count_ndisc):
#	#	plt.plot(RB[:,j]);
#	plt.legend()
#	plt.show();
	return
