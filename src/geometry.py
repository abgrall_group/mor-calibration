__all__=["transform_or2ref","transform_ref2or", "interpolate_or2ref", "interpolate_ref2or", "jacobian_or2ref","jacobian_ref2or", "detect_calib_ind", "ddcalib_trasform_or2ref", "ddcalib_trasform_ref2or", "derivative_calib","ExtraGeometry"]

# Translation!
# This transformation map the original domain [a,b] with periodic bc, to the domain [0,1], with the transformation T(x,mu) = [(x-mu)/(b-a)+0.5]%1
from tests import *
import numpy as np
import bisect

class ExtraGeometry():
  def __init__(self, ic_type):
    self.geometry_parameters=GeometryParameters(ic_type)
    self.set_bc(self.geometry_parameters.bc_type)

  def set_original_domain(self, n=None):
    if n is None:
      n=self.geometry_parameters.n
    self.init_point= self.geometry_parameters.init_domain
    self.end_point = self.geometry_parameters.end_domain
    self.size_domain = self.end_point-self.init_point
    self.set_points(n)
  def set_reference_domain(self, x=None):
    self.geometry_parameters.init_domain=0.
    self.geometry_parameters.end_domain=1.
    self.init_point= self.geometry_parameters.init_domain
    self.end_point = self.geometry_parameters.end_domain
    self.size_domain = self.end_point-self.init_point
    if x is None:
      self.set_points(self.geometry_parameters.n)
    else:
      self.set_x(x)

  def set_bc(self, bc):
    self.bc=bc #bc = "periodic", "outflow"
  def set_points(self, n=100):
    self.n_elem=n
    self.elements=range(self.n_elem)
    self.elem_to_dofs=np.zeros((self.n_elem, 2), dtype=int)
    if self.bc=="periodic":
      self.x=np.linspace(self.init_point, self.end_point, n+1)[:-1]
      self.dx= (self.x-np.r_[self.x[-1],self.x[:-1]])%self.size_domain
      self.n_dofs = n
      self.dofs_to_elem=np.zeros((self.n_dofs, 2), dtype=int)
      for k in range(self.n_elem):
        self.elem_to_dofs[k,:]=[k,(k+1)%self.n_dofs]
      for i in range(self.n_dofs):
          self.dofs_to_elem[i,:]=[i-1%self.n_elem,i%self.n_elem]
    elif self.bc=="outflow":
      self.x=np.linspace(self.init_point, self.end_point, n+1)
      self.dx=np.concatenate([self.x[1:]-self.x[:-1], [self.x[-1] - self.x[-2]]])
      self.n_dofs = n+1
      self.dofs_to_elem=np.zeros((self.n_dofs, 2), dtype=int)
      for k in range(self.n_elem):
        self.elem_to_dofs[k,:]=[k,k+1]
      for i in range(self.n_dofs):
          self.dofs_to_elem[i,:]=[max(i-1,0),min(i,self.n_elem)]
  def set_x(self,x):
    self.x = x
    self.n_dofs = len(self.x)
    if self.bc=="periodic":
      self.dx= (self.x-np.r_[self.x[-1],self.x[:-1]])%self.size_domain
      self.n_elem = self.n_dofs
      self.elem_to_dofs=np.zeros((self.n_elem, 2), dtype=int)
      for k in range(self.n_elem):
        self.elem_to_dofs[k,:]=[k,(k+1)%self.n_dofs]
    elif self.bc=="outflow":
      self.dx= np.concatenate([self.x[1:]-self.x[:-1], [self.x[-1] - self.x[-2]]])
      self.n_elem = self.n_dofs -1
      self.elem_to_dofs=np.zeros((self.n_elem, 2), dtype=int)
      for k in range(self.n_elem):
        self.elem_to_dofs[k,:]=[k,k+1]
  def get_neighboorhood(self, i):
      n = self.n_dofs
      if i>0 and i<n-1:
        jm1=i-1
        j  = i
        jp1=i+1
      elif self.bc=="periodic":
        if i==n-1: #i=n-1
          j=n-1
          jm1=n-2
          jp1=0
        else: #i==0
          j=0
          jm1 = n-1
          jp1 = 1
      elif self.bc == "outflow":
        if i==n-1:
          j=n-1
          jm1 = n-2
          jp1 = n-1
        else: #i=0
          j=0
          jm1=0
          jp1=1
      else:
        raise ValueError("bc in time evolution (solveFOM) are not correct")
      return jm1, j, jp1


## The trasformation is going from the original domain [a,b] to the reference [0,1] shifting calib into 1/2.
def transform_or2ref(x,calib, extra):
  dom_len=extra.size_domain
  y=[pt%1 for pt in ((x-calib)/dom_len+0.5)]
  return np.array(y)

def transform_ref2or(y,calib,extra):
	b=extra.init_point
	a=extra.end_point
	dom_len=extra.size_domain
	x=[(pt-a)%(b-a)+a for pt in (y-0.5)*dom_len+calib]
	return np.array(x)

# this is the derivative of the trasformation on calib dT(x)/dcalib
def ddcalib_trasform_or2ref(x,calib,extra):
	dom_len=extra.size_domain
	dT=[-1./dom_len for pt in np.array([x])]
	return np.array(dT)

# this is the derivative of the inverse trasformation on calib dT^-1(y,calib)/dcalib
def ddcalib_trasform_ref2or(y,calib,extra):
  dT=np.ones(np.shape(y))
  return dT

# derivative of transform on d x  dT(x)/dx
def jacobian_or2ref(x,calib, extra):
	dom_len=extra.size_domain
	return 1./dom_len

# derivative of inverse transform on d y  dT^-1(y)/dy
def jacobian_ref2or(y,calib, extra):
	dom_len=extra.size_domain
	return dom_len

#given a function on original domain (x,U), compute the values on the reference (y,V)
def interpolate_or2ref(x,y, calib, U, extra):
	#evaluate V(y) = U(T^-1(y)), knowing U(x)
	n_ref=len(y)
	V=np.zeros(n_ref)
	Tx_or = transform_or2ref(x,calib, extra)
	sort_idx=np.argsort(Tx_or)
	Tx_or=Tx_or[sort_idx]
	U=U[sort_idx]

	for n in range(n_ref):
		idl, idr, xl, xr = detect_neighbourhood(Tx_or, y[n])
		V[n]= (U[idl]*(xr-y[n]) + U[idr]*(y[n]-xl))/(xr-xl)
	return V

#given a function on reference domain (y,V), compute the values on the original (x,U)
def interpolate_ref2or(y,x,calib, V, extra):
	n_ref=len(x)
	U=np.zeros(n_ref)
	Tiy_ref= transform_ref2or(y,calib, extra)
	sort_idx=np.argsort(Tiy_ref)
	Tiy_ref=Tiy_ref[sort_idx]
	V=V[sort_idx]

	for n in range(n_ref):
		idl, idr, yl, yr = detect_neighbourhood(Tiy_ref, x[n])
		U[n]= (V[idl]*(yr-x[n]) + V[idr]*(x[n]-yl))/(yr-yl)
	return U


def detect_neighbourhood(x, pt):
	#find points in x closer to pt
	idx = bisect.bisect_left(x, pt)
	idl = (idx -1)%len(x)
	idr = idx%len(x)
	vall = x[idl]
	valr = x[idr]
	return idl,idr, vall, valr


def detect_calib_ind(u, detec_type, previous_calib=None):
  n=len(u)
  if previous_calib is None:
    previous_calib = 0
    candidates = range(n)
  else:
    candidates = [(k+previous_calib)%len(u) for k in range(-10,10)]

  if detec_type == "min":
    selection = np.argmin(u[candidates])
    return candidates[selection]
  elif detec_type == "max":
    selection = np.argmax(u[candidates])
    return candidates[selection]
  elif detec_type == "steepest":
    qoi = abs(u-np.r_[u[-1],u[:-1]])
    selection = np.argmax(qoi[candidates])
    return candidates[selection]
  elif detec_type == "steepest_positive":
    if sum([(u[k]>0)==False for k in candidates])==len(candidates):
      return previous_calib
    else:
      qoi = (u>0)*(u-np.r_[u[1:],u[0]])
      selection = np.argmax(qoi[candidates])
      return candidates[selection]
  elif detec_type == "sign_change":
    chosen = previous_calib
    for k in candidates:# (k+2<len(u) and flag ==False):
      if (u[k]>=-10.**-5) and (u[(k+1)%n]<=-10.**-5):
        chosen=k
        break
    return chosen

# This is to compute dcalib/dt, but it just computes the dcalib given two calibs
def derivative_calib(a,p,geometry):
  if geometry.bc=="periodic":
#    return (a-p)%geometry.size_domain
    return (a-p+geometry.size_domain/2)%geometry.size_domain - geometry.size_domain/2
  elif geometry.bc=="outflow":
    return a-p
  else:
    raise ValueError("bc not matching in derivative_calib")
