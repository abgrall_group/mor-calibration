__all__ = ["EIMParameters", "GreedyParameters", "PODParameters"]

class EIMParameters:
  def __init__(self):
    self.tol_EIM_init=1.e-3  #first EIM tolerance
    self.M_add_EIM = 2    # every time we are adding EIM basis functions this is the limit of functions added
    self.tol_EIM=1.e-7    # tolerance for EIM every time we are adding basis functions
    self.max_it_EIM=200  # maximum dimension of EIM space
    self.M1_EIM=5       # basis functions for computing error

class GreedyParameters:
  def __init__(self):
    self.tol_greedy=1.e-3

class PODParameters:
  def __init__(self):
    self.tol_POD_new	= 5.e-2  # given a mu, POD on time evolution: big to be precise around 0.1 and safe
    self.tol_POD_1=1.e-2   # first POD to init RB, whatever, not too small
    self.tol_POD_2= 1.e-6  # on the total RB: small to be able to store a big amount of info, anyway << tol_greedy
