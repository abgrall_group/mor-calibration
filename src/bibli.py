__all__=["createInitialCondition", "solveFOM", "solveFOM_ref", "diffSolve", "diffSolve_ref","diffSolve_EIM","diffSolve_EIM_ref", "diffSolve_RB_EIM", "diffSolve_RB_EIM_ref", "diffSolve_RB_EIM_proj", "diffSolve_RB_EIM_proj_ref", "maximum_vitesse",  "createInitialCondition_reference", "PhysicalProblem", "FullSolution", "SolutionManifold"] #"diffSolve_EIM", "diffSolve_RB_EIM","diffSolve_RB_EIM_proj" ,
import math
import numpy as np
from scipy.sparse import spdiags
#import solve as so
from geometry import *
from tests import *
import time
import matplotlib.pyplot as plt
from parameters import *
try:
  from joblib import Parallel, delayed
except:
  print('joblib not found')
import multiprocessing



class PhysicalProblem:
  def __init__(self, geometry, ic_type):
    self.physical_parameters = PhysicalParameters(ic_type)
    self.geometry = geometry
    self.test = self.physical_parameters.equation
    self.ic_type = ic_type
    self.detec_type = self.geometry.geometry_parameters.detec_type
    self.ndof = self.geometry.n_dofs
    self.T = self.physical_parameters.T_fin
    self.CFL = self.physical_parameters.CFL

  def set_dt(self, param):
    dt = 10.**10.
    mus = param.param_domain
    for k in range(param.n_param):
      mu = mus[k,:]
      dt = min(dt, self.compute_dt(mu))
    Nt = int(self.T/dt)+1
    self.Nt = Nt
    self.dt = self.T/Nt
  def set_Nt(self, Nt):
    self.Nt = Nt
    self.dt = self.T/Nt
  def get_dt(self):
    return self.dt
  def get_Nt(self):
    return self.Nt

  def set_CE_error(self, CE):
    self.CE=CE

  def compute_dt(self, mu):
    u0, calib_index, calib = createInitialCondition(self, mu)
    min_dx= np.min(self.geometry.dx)
    maxJf = np.max(self.spectral_radius(u0,mu))
    return self.CFL*min_dx*(maxJf**-1)

  def flux(self, u, param):
    mu_flux=param[0]
    if self.test=="burgers":
      return mu_flux*u**2./2.
    elif self.test=="advection":
      return mu_flux*u
    else:
      raise ValueError("flux not defined for the test "+self.test)
      return ""

  def flux_non_cons(self, u, param):
    mu_flux=param[0]
    if self.test=="burgers":
      return [mu_flux*u**2./2., u]
      # flux=np.zeros((2,1))
      # flux[0,0]= 
      # flux[1,0] = u
      # return flux
    elif self.test=="advection":
      return [mu_flux*u, u]
      # flux=np.zeros((2,1))
      # flux[0,0]= mu_flux*u
      # flux[1,0] = u
      # return flux
    else:
      raise ValueError("flux not defined for the test "+self.test)
      return ""


  def jacobian_flux(self,u,param):
    mu_flux=param[0]
    if self.test=="burgers":
      return mu_flux*u
    elif self.test=="advection":
      return mu_flux
    else:
      raise ValueError("flux not defined for the test "+self.test)
      return ""
  def spectral_radius(self,u,param):
    jac=self.jacobian_flux(u,param)
#    if np.size(jac)==1:
    return abs(jac)
#    else:
#      return max(abs(np.linalg.eigvals(jac)))

  def non_conservative_matrix(self, u, param, dtransdcalib,dcalibdt,jac):
    H = [jac, -dtransdcalib*dcalibdt*jac]
    return np.array(H)

  def non_cons_jacobian(self,u,param):
    mu_flux=param[0]
    if self.test=="burgers":
      return [mu_flux*u, 1.]
      # JJ = np.zeros((2,1))
      # JJ[0,0] = mu_flux*u
      # JJ[1,0] = 1.
      # return JJ
    elif self.test=="advection":
      return [mu_flux, 1.]
      # JJ = np.zeros((2,1))
      # JJ[0,0] = mu_flux
      # JJ[1,0] = 1.
      # return JJ
    else:
      raise ValueError("flux not defined for the test "+self.test)
      return ""

  def non_cons_spectral_radius(self,u,param, H):
    jac=self.non_cons_jacobian(u,param)
    eigv = sum(abs(H*jac))#np.dot(np.abs(H),np.abs(jac))#np.linalg.eigvals(np.dot(H,jac))
    return eigv

class FullSolution:
  def __init__(self, mu, physical):
    self.mu = mu
    self.physical = physical
    self.U = np.zeros((self.physical.geometry.n_dofs, self.physical.Nt+1))
    self.EIM_fun = np.zeros((self.physical.geometry.n_dofs, self.physical.Nt))
    self.calib_values  = np.zeros(self.physical.Nt+1)
  def set_solution(self, it, U_time):
    self.U[:,it] = U_time
  def set_calib_value(self,it, calib):
    self.calib_values[it] = calib
  def set_flux(self, it, flux_time):
    self.EIM_fun[:,it-1] = flux_time
  def get_solution_time(self, it):
    return self.U[:,it]
  def get_flux_time(self, it):
    return self.EIM_fun[:,it-1]
  def get_whole_solution(self):
    return U
  def get_whole_flux(self):
    return EIM_fun

class ReducedSolution:
  def __init__(self, nRB, mu, physical):
    self.mu = mu
    self.nRB = nRB
    self.Nt = physical.Nt
    self.alpha = np.zeros((self.nRB, self.Nt+1))
    self.calib_values = np.zeros((self.Nt+1))
    self.true_err = 100.
    self.err_ind = 100.
    self.final_error = 100.
  def set_solution(self, it, alpha):
    self.alpha[:,it] = alpha
  def get_solution_time(self, it):
    return alpha[:,it]
  def set_calib_value(self,it, calib):
    self.calib_values[it] = calib
  def get_whole_solution(self):
    return alpha
  def set_true_error(self, err):
    self.true_err = err
  def set_error_ind(self, err):
    self.err_ind = err
  def set_final_error(self, err):
    self.final_error = err



class SolutionManifold:
  def __init__(self, param, physical, ref = None):
    if ref is None:
      self.reference_flag  = False
    else:
      self.reference_flag = ref
    self.param = param
    self.physical = physical
    self.n =  self.physical.geometry.n_dofs
    self.num_fluxes = self.param.n_param * (self.physical.Nt)
    self.real_sol = []
    self.fluxes = np.zeros( (self.n, self.num_fluxes)  )
  def build_all_solutions(self, parallel = None, physical_or=None, original_manifold = None):
    print('Starting building all FOM solutions')
    num_cores = multiprocessing.cpu_count()
    if self.reference_flag == False:
      self.real_sol = Parallel(n_jobs=num_cores)(delayed(solveFOM)(self.physical,mu) for mu in self.param.param_domain)
    else:
      if physical_or is None or original_manifold is None:
        raise ValueError('You should pass the original physical and original manifold to compute the calibrated solutions')
      self.real_sol = Parallel(n_jobs=num_cores)(delayed(solveFOM_ref)(self.physical, physical_or, self.param.param_domain[cand], original_manifold.real_sol[cand].calib_values, self.physical.geometry.x) for cand in range(self.param.n_param))
    Nt = self.physical.Nt
    for cand in range(self.param.n_param):
      self.fluxes[:,cand*Nt:(cand+1)*Nt] = self.real_sol[cand].EIM_fun
    print('Finished building all FOM solutions')

class ReducedManifold:
  def __init__(self, param, physical, nRB):
    self.param = param
    self.physical = physical
    self.reduced_dimension = 0
    self.nRB = nRB
    self.reduced_sol = []
  def initialize_reduced_solutions(self):
    for k in range(param.n_param):
      self.reduced_sol.append(ReducedSolution(self.nRB, self.param.param_domain[k], self.physical))


#####################
def  createInitialCondition(physical, mu, calib=None):
  x = physical.geometry.x
  u0 = physical.physical_parameters.initialCondition(x,mu)
  if calib is None:
    calib_ind = detect_calib_ind(u0, physical.detec_type)
    calib = x[calib_ind]
  else:
    calib_ind = np.argmin(np.abs(x-calib))
  return u0, calib_ind, calib


def createInitialCondition_reference(physical_ref, physical_or, mu, calib=None, y=None):
   u0, calib_index, calib =createInitialCondition(physical_or, mu, calib)
   x = physical_or.geometry.x
   if y is None:
     y = transform_or2ref(x,calib,physical_or.geometry)
   v0_ref = interpolate_or2ref(x,y,calib, u0, physical_or.geometry)
   return y, v0_ref, calib

##########################################
def solveFOM(physical, param):
# solve the Burger problem with upwind first order scheme.
  Nt = physical.Nt
  solution = FullSolution(param, physical)
  u0, calib_index, calib = createInitialCondition(physical,param)
  solution.set_solution(0,u0)
  solution.set_calib_value(0,calib)
  x = physical.geometry.x
  for it in range(1,Nt+1):
     uu0=solution.get_solution_time(it-1)
     uu1,RHS=diffSolve(physical,uu0,param)
     calib_index=detect_calib_ind(uu1, physical.detec_type, calib_index)
     solution.set_solution(it,uu1)
     solution.set_calib_value(it,x[calib_index])
     solution.set_flux(it,RHS)
  return solution


def solveFOM_ref(physical_ref, physical_or, param, calibs, y=None):
# solve the Burger problem with upwind first order scheme.
  Nt = physical_or.Nt
  solution = FullSolution(param, physical_ref)
  if y is None:
    y, v0, calib = createInitialCondition_reference(physical_ref, physical_or,param, calibs[0])
  else:
    y, v0, calib = createInitialCondition_reference(physical_ref, physical_or,param, calibs[0], y)
  solution.set_solution(0,v0)
  solution.set_calib_value(0,calibs[0])
  for it in range(1,Nt+1):
     solution.set_calib_value(it, calibs[it])
     uu0=solution.get_solution_time(it-1)
     uu1,RHS=diffSolve_ref(physical_ref,physical_or, uu0,param, calibs[it-1], calibs[it])
     solution.set_solution(it,uu1)
     solution.set_flux(it,RHS)
  return solution

######
def diffSolve(physical, w0, param):
    spectral_radii=np.zeros(len(w0))
    for k in range(len(w0)):
        spectral_radii[k]=physical.spectral_radius(w0[k],param)
    check_CFL(spectral_radii, physical, param)
    n_elem=physical.geometry.n_elem
    n_dofs = physical.geometry.n_dofs
    w1=np.zeros(n_dofs)
    res=np.zeros(n_dofs)
    dt = physical.dt
    for k in range(n_elem):
        idxs = physical.geometry.elem_to_dofs[k,:]
        dx=physical.geometry.dx[k]
        res_k = compute_res(w0[idxs],physical, param, dx,dt)
        res[idxs] = res[idxs] + res_k
    for i in range(n_dofs):
        w1[i]=w0[i]-res[i]
    return w1, res

def diffSolve_ref(physical_ref, physical_or, w0, param, calib_p, calib_a):
    n_elem=physical_ref.geometry.n_elem
    n_dofs = physical_ref.geometry.n_dofs
    w1=np.zeros(n_dofs)
    res=np.zeros(n_dofs)
    dt = physical_ref.dt
    y = physical_ref.geometry.x
    dcalibdt = np.ones(n_dofs)*derivative_calib(calib_a, calib_p, physical_or.geometry)/dt
    jac=np.zeros(n_dofs)
    dtransdcalib = np.zeros(n_dofs)
    non_cons_matrix = np.zeros((n_dofs, 1,2))
    x = transform_ref2or(y,calib_a, physical_or.geometry)
    for i in range(n_dofs):
        jac[i] = jacobian_or2ref(x[i], calib_a, physical_or.geometry)
        dtransdcalib[i] = ddcalib_trasform_ref2or(y[i],calib_a, physical_or.geometry)
        non_cons_matrix[i,0,:] = physical_ref.non_conservative_matrix(w0[i], param, dtransdcalib[i], dcalibdt[i], jac[i])

    fluxes = np.zeros((n_dofs, 2,1))      
    spectral_radii=np.zeros(len(w0))
    for k in range(len(w0)):
        spectral_radii[k]=physical_ref.non_cons_spectral_radius(w0[k],param, non_cons_matrix[k,0,:])
#        spectral_radii[k]=physical_ref.spectral_radius(w0[k],param) # non_cons_matrix[k,0,:])
        fluxes[k,:,0] = physical_ref.flux_non_cons(w0[k], param)
    check_CFL(spectral_radii, physical_ref, param)

    for k in range(n_elem):
        idxs = physical_ref.geometry.elem_to_dofs[k,:]
        dx=physical_ref.geometry.dx[k]
        res_k = compute_res_non_conservative(w0[idxs],physical_ref, param, dx,dt, fluxes[idxs], non_cons_matrix[idxs,:,:],spectral_radii[idxs], dtransdcalib[idxs], dcalibdt[idxs], jac[idxs] )
        res[idxs] = res[idxs] + res_k
    for i in range(n_dofs):
        w1[i]=w0[i]-res[i]
    return w1, res


################################################
def diffSolve_EIM(physical,w0,EIM, param):
# on resoud le schema machin
    spectral_radii=np.zeros(len(EIM.magic_pts))
    for k,pt in enumerate(EIM.magic_pts):
        spectral_radii[k]=physical.spectral_radius(w0[pt],param)
    check_CFL(spectral_radii, physical, param)
    n_elem=physical.geometry.n_elem
    n_dofs = physical.geometry.n_dofs
    w1=np.zeros(n_dofs)
    res=np.zeros(n_dofs)
    dt = physical.dt
    for k in EIM.real_magic_elem:
        idxs = physical.geometry.elem_to_dofs[k,:]
        dx=physical.geometry.dx[k]
        res_k = compute_res(w0[idxs],physical, param, dx,dt)
        res[idxs] = res[idxs] + res_k
    beta=np.dot(EIM.EIM_inv,rhs[EIM.magic_pts])
    RHS= np.dot(EIM.EIM_fun,beta)
    w1=w0 -RHS
    #print "dt    ==== ",dt
    return w1,RHS

def diffSolve_EIM_ref(physical_ref, physical_or,w0,EIM, param, calib_p, calib_a):


    n_elem=physical_ref.geometry.n_elem
    n_dofs = physical_ref.geometry.n_dofs
    w1=np.zeros(n_dofs)
    res=np.zeros(n_dofs)
    dt = physical_ref.dt
    y = physical_ref.geometry.x
    dcalibdt = np.ones(n_dofs)*derivative_calib(calib_a, calib_p, physical_or.geometry)/dt
    jac=np.zeros(n_dofs)
    dtransdcalib = np.zeros(n_dofs)
    non_cons_matrix = np.zeros((n_dofs, 1,2))
    x = transform_ref2or(y,calib_a, physical_or.geometry)
    for i in EIM.real_magic_pts:
        jac[i] = jacobian_or2ref(x[i], calib_a, physical_or.geometry)
        dtransdcalib[i] = ddcalib_trasform_ref2or(y[i],calib_a, physical_or.geometry)
        non_cons_matrix[i,0,:] = physical_ref.non_conservative_matrix(w0[i], param, dtransdcalib[i], dcalibdt[i], jac[i])

    spectral_radii=np.zeros(len(EIM.all_magic_points))
    spectral_radii_all = np.zeros(n_dofs)
    fluxes = np.zeros((n_dofs, 2,1))
    for k,pt in enumerate(EIM.all_magic_points):
        spectral_radii[k]=physical_ref.non_cons_spectral_radius(w0[pt],param, non_cons_matrix[pt,0,:])
        spectral_radii_all[pt] = spectral_radii[k]
        fluxes[pt,:,0] = physical_ref.flux_non_cons(w0[pt], param)
    check_CFL(spectral_radii, physical_ref, param)
    

    for k in EIM.real_magic_elem:
        idxs = physical_ref.geometry.elem_to_dofs[k,:]
        dx=physical_ref.geometry.dx[k]
        res_k = compute_res_non_conservative(w0[idxs],physical_ref, param, dx,dt,fluxes[idxs],non_cons_matrix[idxs,:,:],spectral_radii_all[idxs],spectral_radii[idxs], dtransdcalib[idxs], dcalibdt[idxs], jac[idxs] )
        res[idxs] = res[idxs] + res_k
    beta=np.dot(EIM.EIM_inv,res[EIM.magic_pts])
    RHS= np.dot(EIM.EIM_fun,beta)
    w1=w0 -RHS

    #print "dt    ==== ",dt
    return w1,RHS

########################################################
def diffSolve_RB_EIM(physical,w0,w0_alp,EIM,RB, param):
# on resoud le schema machin
    spectral_radii=np.zeros(len(EIM.magic_pts))
    for k,pt in enumerate(EIM.magic_pts):
        spectral_radii[k]=physical.spectral_radius(w0[pt],param)
    check_CFL(spectral_radii, physical, param)
    n_elem=physical.geometry.n_elem
    n_dofs = physical.geometry.n_dofs
    w1=np.zeros(n_dofs)
    res=np.zeros(n_dofs)
    dt = physical.dt
    for k in EIM.real_magic_elem:
        idxs = physical.geometry.elem_to_dofs[k,:]
        dx=physical.geometry.dx[k]
        res_k = compute_res(w0[idxs],physical, param, dx,dt)
        res[idxs] = res[idxs] + res_k
    beta=np.dot(EIM.EIM_inv, res[EIM.magic_pts])
    RHS= np.dot(EIM.EIM_fun,beta)
    RHS_alp=so.solve(np.shape(RHS)[0],np.shape(RB.RB)[1],RB.RB,RHS)
    w1_alp=w0_alp -RHS_alp
    #print "dt    ==== ",dt
    return w1_alp, RHS

def diffSolve_RB_EIM_ref(physical_ref, physical_or,w0,w0_alp, EIM,RB, param, calib_p, calib_a):
# on resoud le schema machin

    n_elem=physical_ref.geometry.n_elem
    n_dofs = physical_ref.geometry.n_dofs
    w1=np.zeros(n_dofs)
    res=np.zeros(n_dofs)
    dt = physical_ref.dt
    y = physical_ref.geometry.x
    dcalibdt = np.ones(n_dofs)*derivative_calib(calib_a, calib_p, physical_or.geometry)/dt
    jac=np.zeros(n_dofs)
    dtransdcalib = np.zeros(n_dofs)
    non_cons_matrix = np.zeros((n_dofs, 1,2))
    x = transform_ref2or(y,calib_a, physical_or.geometry)
    for i in EIM.real_magic_pts:
        jac[i] = jacobian_or2ref(x[i], calib_a, physical_or.geometry)
        dtransdcalib[i] = ddcalib_trasform_ref2or(y[i],calib_a, physical_or.geometry)
        non_cons_matrix[i,0,:] = physical_ref.non_conservative_matrix(w0[i], param, dtransdcalib[i], dcalibdt[i], jac[i])

    spectral_radii=np.zeros(len(EIM.all_magic_points))
    spectral_radii_all = np.zeros(n_dofs)
    fluxes = np.zeros((n_dofs, 2,1))
    for k,pt in enumerate(EIM.all_magic_points):
        spectral_radii[k]=physical_ref.non_cons_spectral_radius(w0[pt],param, non_cons_matrix[pt,0,:])
        spectral_radii_all[pt] = spectral_radii[k]
        fluxes[pt,:,0] = physical_ref.flux_non_cons(w0[pt], param)
    check_CFL(spectral_radii, physical_ref, param)
    


    for k in EIM.real_magic_elem:
        idxs = physical_ref.geometry.elem_to_dofs[k,:]
        dx=physical_ref.geometry.dx[k]
        res_k = compute_res_non_conservative(w0[idxs],physical_ref, param, dx,dt,fluxes[idxs],non_cons_matrix[idxs,:,:],spectral_radii_all[idxs], dtransdcalib[idxs], dcalibdt[idxs], jac[idxs] )
        res[idxs] = res[idxs] + res_k
    beta=np.dot(EIM.EIM_inv,res[EIM.magic_pts])
    RHS= np.dot(EIM.EIM_fun,beta)
    RHS_alp=so.solve(np.shape(RHS)[0],np.shape(RB.RB)[1],RB.RB,RHS)
    w1_alp=w0_alp -RHS_alp
    #print "dt    ==== ",dt
    return w1,RHS
##############################################
def diffSolve_RB_EIM_proj(physical,w0,w0_alp,EIM, RB, param):
# on resoud le schema machin
    spectral_radii=np.zeros(len(EIM.magic_pts))
    for k,pt in enumerate(EIM.magic_pts):
        spectral_radii[k]=physical.spectral_radius(w0[pt],param)
    check_CFL(spectral_radii, physical, param)
    n_elem=physical.geometry.n_elem
    n_dofs = physical.geometry.n_dofs
    w1=np.zeros(n_dofs)
    res=np.zeros((n_dofs,1))
    dt = physical.dt
    for k in EIM.all_magic_elements:
        idxs = physical.geometry.elem_to_dofs[k,:]
        dx=physical.geometry.dx[k]
        res_k = compute_res(w0[idxs],physical, param, dx,dt)
        res[idxs] = res[idxs] + res_k

    RHS_M1= np.zeros((np.shape(EIM.magic_M1)[0],1))
    RHS_M1[:,0] = res[EIM.magic_M1]

    RHS_alp_EIM=np.dot(EIM.EIM_inv,res[EIM.magic_pts])
    RHS_alp = np.reshape(np.dot(RB.EIM_fun_RB,RHS_alp_EIM), np.shape(w0_alp))

    w1_alp=w0_alp -RHS_alp

    return w1_alp, RHS_alp, RHS_M1

def diffSolve_RB_EIM_proj_ref(physical_ref, physical_or,w0,w0_alp, EIM, RB, param, calib_p, calib_a):
# on resoud le schema machin

    n_elem=physical_ref.geometry.n_elem
    n_dofs = physical_ref.geometry.n_dofs
    w1=np.zeros(n_dofs)
    res=np.zeros(n_dofs)
    dt = physical_ref.dt
    y = physical_ref.geometry.x
    dcalibdt = np.ones(n_dofs)*derivative_calib(calib_a, calib_p, physical_or.geometry)/dt
    jac=np.zeros(n_dofs)
    dtransdcalib = np.zeros(n_dofs)
    non_cons_matrix = np.zeros((n_dofs, 1,2))
    x = transform_ref2or(y,calib_a, physical_or.geometry)
    for i in EIM.all_magic_points:
        jac[i] = jacobian_or2ref(x[i], calib_a, physical_or.geometry)
        dtransdcalib[i] = ddcalib_trasform_ref2or(y[i],calib_a, physical_or.geometry)
        non_cons_matrix[i,0,:] = physical_ref.non_conservative_matrix(w0[i], param, dtransdcalib[i], dcalibdt[i], jac[i])

    spectral_radii=np.zeros(len(EIM.all_magic_points))
    spectral_radii_all = np.zeros(n_dofs)
    fluxes = np.zeros((n_dofs, 2,1))
    for k,pt in enumerate(EIM.all_magic_points):
        spectral_radii[k]=physical_ref.non_cons_spectral_radius(w0[pt],param, non_cons_matrix[pt,0,:])
#        spectral_radii[k]=physical_ref.spectral_radius(w0[pt],param) #, non_cons_matrix[pt,0,:])
        spectral_radii_all[pt] = spectral_radii[k]
        fluxes[pt,:,0] = physical_ref.flux_non_cons(w0[pt], param)
#    check_CFL(spectral_radii, physical_ref, param)
    

    for k in EIM.all_magic_elements:
        idxs = physical_ref.geometry.elem_to_dofs[k,:]
        dx=physical_ref.geometry.dx[k]
        res_k = compute_res_non_conservative(w0[idxs],physical_ref, param, dx,dt,fluxes[idxs], non_cons_matrix[idxs,:,:],spectral_radii_all[idxs], dtransdcalib[idxs], dcalibdt[idxs], jac[idxs] )
        res[idxs] = res[idxs] + res_k

    RHS_M1= np.zeros((np.shape(EIM.magic_M1)[0],1))
    RHS_M1[:,0] = res[EIM.magic_M1]

    RHS_alp_EIM=np.dot(EIM.EIM_inv,res[EIM.magic_pts])
    RHS_alp = np.reshape(np.dot(RB.EIM_fun_RB,RHS_alp_EIM), np.shape(w0_alp))

    w1_alp=w0_alp -RHS_alp

    return w1_alp, RHS_alp, RHS_M1

#############################################

def compute_res(w0, physical, param, dx, dt):
  res=np.zeros(np.shape(w0))
  fl=physical.flux(w0[0],param)
  fr=physical.flux(w0[1],param)
  lam = np.max([physical.spectral_radius(w0[0],param),physical.spectral_radius(w0[1],param)])
  ubar= 0.5*(w0[1]+w0[0]) #np.mean(w0)
  for k in range(len(w0)):
    res[k]= dt/dx*(0.5* (fr-fl) + lam*(w0[k]-ubar))
  return res

def compute_res_non_conservative(w0, physical, param, dx,dt, fluxes, non_cons_matrixes, spectral_radii,dtransdcalib, dcalibdt, jac):
  Hl=non_cons_matrixes[0,0,:]
  Hr=non_cons_matrixes[1,0,:]
  H=0.5*(Hl+Hr)#np.mean([Hl,Hr],axis=0)
  flux_l=fluxes[0]#physical.flux_non_cons(w0[0], param)
  flux_r=fluxes[1] #physical.flux_non_cons(w0[1], param)
  ubar = 0.5*(w0[1]+w0[0])#np.mean(w0)
  lam = np.max(spectral_radii)
  # lam = np.max([physical.spectral_radius(w0[0],param),physical.spectral_radius(w0[1],param)])
  res = np.zeros(np.shape(w0))
  for k in range(len(w0)):
    res[k] = dt/dx*(0.5*np.dot(H,flux_r-flux_l) + lam*(w0[k]-ubar))
  return res

def maximum_vitesse(w, param, physical):
# I compute the maximum possible speed for all the possible samples.
# doing so, I am sure that all the samples have the same chronology.
# otherwise, the time stepping relies on the CFL only, so we do not
# have the same chronology
    maxi=0.
    for i in range(len(w)):
      maxi=max(maxi,physical.jacobian_flux(w[i],param))
    return maxi

def check_CFL(sp_radius, physical, param):
  problem = 0
  for i,umax in enumerate(sp_radius):
    if umax*physical.dt>physical.geometry.dx[i]:
      problem = 1
  if problem==1:
    print("Warning: CFL not satisfied!!!")
    print("Parameter "+ str(param))
  return problem
###########################################
