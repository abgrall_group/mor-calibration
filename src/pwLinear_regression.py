__all__=["PWRegressionMap"]
import numpy as np
import matplotlib.pyplot as plt
import pickle
from bibli import *
from geometry import *
import time


class PWRegressionMap:
	def __init__(self):
		self.time_training=0.


	def train(self, x_train, y_train):
		self.x_train = x_train
		self.y_train = y_train
		self.Nt = np.shape(y_train)[0]-1

	def validate(self, x_val,y_val):
		n_param_test=np.shape(y_val)[1]
		y_test_predict= np.zeros([self.Nt+1,n_param_test])
		for par in range(n_param_test):
			for it in range(self.Nt+1):
				y_test_predict[it,par]=regression(x_val[par,:], self.x_train, self.y_train[it,:])

		error= np.reshape(y_val,np.shape(y_test_predict))-y_test_predict
		self.mean_error = np.mean(np.abs(error))
		self.max_error = np.max(np.abs(error))
		return self.mean_error, self.max_error


	def predict(self, x_test):
		try:
			y_predict=np.zeros(self.Nt+1)
			for it in range(self.Nt+1):
				y_predict[it]=regression(x_test, self.x_train, self.y_train[it,:])
			return y_predict
		except:
			raise ValueError('Regression Map not trained yet')
			

def bary_coords(mu, all_mu):
    D=np.shape(all_mu)[1]
    dist=np.linalg.norm(all_mu-mu,axis=1)
    idxs=np.argsort(dist)
    mus=np.zeros((D+1,D+1)) # parm dim, different mus
    mu_tilde = np.zeros((D+1,1))
    mu_tilde[:D,0]=mu
    mu_tilde[D,0]=1.
    mus[D,:] =1.
    for k in range(D+1):
        mus[:D,k] = all_mu[idxs[k]]
    bary_coo = np.linalg.solve(mus,mu_tilde )
    return bary_coo, mus[:D], idxs[:D+1]

def regression(mu, all_mu, all_theta):
    #no time involved here, mu only param, all_mu= param_domain, all_theta = calibs[it]
    bary,mus,idxs = bary_coords(mu, all_mu)
    theta = np.dot(np.transpose(bary), all_theta[idxs])
    return theta