__all__=["EIM_init","EIM_add", "EIMStructure"]

import numpy as np
import multiprocessing
try:
  from joblib import Parallel, delayed
except:
  print('joblib not found')

import os
from RB_parameters import EIMParameters

class EIMStructure:
  def __init__(self, geometry, EIM_param=None):
    if EIM_param is None:
      self.EIM_param = EIMParameters()
    else:
      self.EIM_param = EIM_param
    self.geometry = geometry
    self.n_elem = self.geometry.n_elem
    self.EIM_fun=[]
    self.magic_pts = []
    self.fnc_M1 = []
    self.magic_M1 = []
    self.EIM_err = []
    self.EIM_errors = []
    self.EIM_dimensions = []
    self.EIM_inv = []
    self.EIM_dim = 0

  def set_tol_EIM(self, tol):
    self.EIM_param.tol_EIM = tol

  def compute_EIM(self, functions):
    [self.EIM_fun, self.magic_pts, self.magic_M1, self.fnc_M1, self.EIM_err] = \
    EIM_init(functions, self.EIM_param.tol_EIM_init, self.EIM_param.max_it_EIM , self.EIM_param.M1_EIM)
    self.n = np.shape(self.EIM_fun)[0]

  def extend_EIM(self, functions):
    [self.EIM_fun, self.magic_pts, self.magic_M1, self.fnc_M1, self.EIM_err] = \
    EIM_add(functions, self.EIM_param.max_it_EIM, self.EIM_param.M_add_EIM, self.EIM_param.tol_EIM, \
    self.EIM_fun, list(self.magic_pts), self.EIM_param.M1_EIM)

  def compute_extras(self):
    truthN = np.shape(self.EIM_fun)[0]
    self.norm_fnc_M1 = np.linalg.norm(self.fnc_M1,2,axis=0)/np.sqrt(truthN)
    self.EIM_M1_dim=np.shape(self.fnc_M1)[0]
    self.EIM_dim = np.shape(self.EIM_fun)[1]
    self.EIM_inv = np.linalg.inv(self.EIM_fun[self.magic_pts,:])

  def compute_boundary_magic_points(self):
    real_magic_pts=list(self.magic_pts)
    real_magic_pts_m1=[ (k-1)%(self.n) for k in real_magic_pts ]
    real_magic_pts_p1=[ (k+1)%(self.n) for k in real_magic_pts ]
    pts_pts_m1=list(set(real_magic_pts).union(set(real_magic_pts_m1)))
    self.real_magic_elem=[k%self.n_elem for k in pts_pts_m1]
    self.dim_real_magic_elem=np.shape(self.real_magic_elem)[0]
    self.real_magic_pts = list(set(pts_pts_m1).union(set(real_magic_pts_p1)))
    self.dim_real_magic_pts=np.shape(self.real_magic_pts)[0]

    real_magic_pts_M1=list(self.magic_M1)
    real_magic_pts_M1_m1=[ (k-1)%(self.n) for k in real_magic_pts_M1 ]
    real_magic_pts_M1_p1=[ (k+1)%(self.n) for k in real_magic_pts_M1 ]
    M1_pts_pts_m1=list(set(real_magic_pts_M1).union(set(real_magic_pts_M1_m1)))
    self.real_magic_elem_M1= [k%self.n_elem for k in M1_pts_pts_m1]
    self.dim_real_magic_elem_M1=np.shape(self.real_magic_elem_M1)[0]
    self.real_magic_pts_M1 = list(set(M1_pts_pts_m1).union(set(real_magic_pts_M1_p1)))
    self.dim_real_magic_pts_M1=np.shape(self.real_magic_pts_M1)[0]

    self.all_magic_elements = list(set(self.real_magic_elem_M1).union(self.real_magic_elem))
    self.all_magic_points = list(set(self.real_magic_pts_M1).union(self.real_magic_pts))

    self.all_points=range(self.n)#list(set(real_magic_elem).union(set(real_magic_M1)))

  def update_history(self):
    self.EIM_errors.append(self.EIM_err)
    self.EIM_dimensions.append(self.EIM_dim)

  def save_EIM(self, fname):
    EIMfname = fname+'EIM/'
    os.system('mkdir '+fname+'EIM/')
    np.save(EIMfname+'fun_EIM.npy', self.EIM_fun)
    np.save(EIMfname+'magic_pts.npy', self.magic_pts)
    np.save(EIMfname+'magic_M1.npy', self.magic_M1)
    np.save(EIMfname+'fnc_M1.npy', self.fnc_M1)
    np.save(EIMfname+'EIM_err.npy', self.EIM_err)

  def load_EIM(self, fname):
    try:
      EIMfname = fname+'EIM/'
      self.EIM_fun = np.load(EIMfname+'fun_EIM.npy')
      self.magic_pts = np.load(EIMfname+'magic_pts.npy')
      self.magic_M1=np.load(EIMfname+'magic_M1.npy')
      self.fnc_M1=np.load(EIMfname+'fnc_M1.npy')
      self.EIM_err=np.load(EIMfname+'EIM_err.npy')
      self.n = np.shape(self.EIM_fun)[0]
    except:
      raise ValueError('EIM files not found')




def EIM_init(fluxes, tol, max_it, Mp):
	print( '#######################')
	print('EIM begins')
	print('#######################')
	err=1.+tol
	m=0
	fluxes=np.array(fluxes)
	truthN, N_flux = np.shape(fluxes)
	EIM_fun=np.zeros((truthN, max_it))
	magic_pts=[]
	proj=np.zeros((truthN,N_flux))

	norms=np.linalg.norm(fluxes, np.inf ,axis=0)
	max_ind=np.argmax(norms)
	while (err>tol and m<max_it):
		m=m+1
		print ('EIM iteration: ',m)
		if m==1:
			r=fluxes[:,max_ind]
		else:
			beta=np.linalg.solve(EIM_fun[magic_pts,:m-1],fluxes[magic_pts,max_ind])
			r=fluxes[:,max_ind]- np.dot(EIM_fun[:,:m-1],beta)
		new_pt=np.argmax(abs(r))
		magic_pts.append(new_pt)
		EIM_fun[:,m-1]=r/r[magic_pts[-1]]
		betas=[]
		for i in range(N_flux):
			betas.append(np.linalg.solve(EIM_fun[magic_pts,:m],fluxes[magic_pts,i]))
		proj=np.dot(EIM_fun[:,:m],np.transpose(betas))
		diff=fluxes-proj
		norms=np.linalg.norm(diff, np.inf ,axis=0)
		err=np.max(norms)
		print ('EIM error: ',err)
		max_ind=np.argmax(norms)
	m_EIM= m
	Mp_fun=np.zeros((truthN,Mp))
	err_EIM=err

	print( m, Mp, m_EIM)
	while (m<Mp+m_EIM):
		m=m+1
		print( 'EIM iteration: ',m)
		if m==1:
			r=fluxes[:,max_ind]
		else:
			beta=np.linalg.solve(EIM_fun[magic_pts,:m-1],fluxes[magic_pts,max_ind])
			r=fluxes[:,max_ind]- np.dot(EIM_fun[:,:m-1],beta)
		new_pt=np.argmax(abs(r))
		magic_pts.append(new_pt)
		EIM_fun[:,m-1]=r/r[magic_pts[-1]]
		betas=[]
		for i in range(N_flux):
			betas.append(np.linalg.solve(EIM_fun[magic_pts,:m],fluxes[magic_pts,i]))
		proj=np.dot(EIM_fun[:,:m],np.transpose(betas))
		diff=fluxes-proj
		norms=np.linalg.norm(diff, np.inf ,axis=0)
		err=np.max(norms)
		print ('EIM error: ',err)
		max_ind=np.argmax(norms)

	Mp_pts = magic_pts[m_EIM:m]
	Mp_fun = EIM_fun[:,m_EIM:m]
	EIM_fun=np.delete(EIM_fun, np.s_[m_EIM:max_it], axis=1)
	magic_pts = magic_pts[:m_EIM]
	print( np.shape(EIM_fun))
	print( np.shape(magic_pts))
	print('#######################')
	print('EIM ends')
	print('EIM basis functions selected: ', m_EIM)
	print('EIM error: ', err_EIM)
	print('#######################')
	return EIM_fun, magic_pts,Mp_pts,Mp_fun, err_EIM

def EIM_add(fluxes, max_it, max_M, tol, EIM_fun_old, magic_pts, Mp):
	print( '#######################')
	print('EIM begins')
	print('#######################')

	fluxes=np.array(fluxes)
	print( np.shape(fluxes))
	truthN, N_flux = np.shape(fluxes)
	m_init=np.shape(EIM_fun_old)[1]
	m=m_init
	EIM_fun=np.zeros((truthN, max_it))
	EIM_fun[:,:m]=EIM_fun_old
	inv_EIM=np.linalg.inv(EIM_fun[magic_pts,:m])
	betas=[]
	for i in range(N_flux):
		betas.append(np.matmul(inv_EIM,fluxes[magic_pts,i]))
	proj=np.dot(EIM_fun[:,:m],np.transpose(betas))
	diff=fluxes-proj
	norms=np.linalg.norm(diff, np.inf ,axis=0)
	err=np.max(norms)
	print ('EIM error: ',err)
	max_ind=np.argmax(norms)
	err= 1.
	while (tol<err and m<max_it and m<m_init+max_M): #
		m=m+1
		print ('EIM iteration: ',m)
		if m==1:
			r=fluxes[:,max_ind]
		else:
			beta=np.matmul(inv_EIM,fluxes[magic_pts,max_ind])
			r=fluxes[:,max_ind]- np.dot(EIM_fun[:,:m-1],beta)
		new_pt=np.argmax(abs(r))
		magic_pts.append(new_pt)
		EIM_fun[:,m-1]=r/r[magic_pts[-1]]
		betas=[]
		inv_EIM = np.linalg.inv(EIM_fun[magic_pts, :m])
		for i in range(N_flux):
			betas.append(np.matmul(inv_EIM,fluxes[magic_pts,i]))
		proj=np.dot(EIM_fun[:,:m],np.transpose(betas))
		diff=fluxes-proj
		norms=np.linalg.norm(diff, np.inf ,axis=0)
		err=np.max(norms)
		print ('EIM error: ',err)
		max_ind=np.argmax(norms)
	m_EIM= m
	err_EIM=err
	Mp_fun=np.zeros((truthN,Mp))

	print (m, Mp, m_EIM)
	while (m<Mp+m_EIM):
		m=m+1
		print( 'EIM iteration: ',m)
		if m==1:
			r=fluxes[:,max_ind]
		else:
			beta=np.matmul(inv_EIM,fluxes[magic_pts,max_ind])
			r=fluxes[:,max_ind]- np.dot(EIM_fun[:,:m-1],beta)
		new_pt=np.argmax(abs(r))
		magic_pts.append(new_pt)
		EIM_fun[:,m-1]=r/r[magic_pts[-1]]
		betas=[]
		inv_EIM=np.linalg.inv(EIM_fun[magic_pts,:m])
		for i in range(N_flux):
			betas.append(np.matmul(inv_EIM,fluxes[magic_pts,i]))
		proj=np.dot(EIM_fun[:,:m],np.transpose(betas))
		diff=fluxes-proj
		norms=np.linalg.norm(diff, np.inf ,axis=0)
		#norms=np.linalg.norm(diff, 2 ,axis=0)
		err=np.max(norms)
		print( 'EIM error: ',err)
		max_ind=np.argmax(norms)

	Mp_pts = magic_pts[m_EIM:m]
	Mp_fun = EIM_fun[:,m_EIM:m]


	EIM_fun= EIM_fun[:,:m_EIM]
	magic_pts = magic_pts[:m_EIM]


	print( np.shape(EIM_fun))
	print( np.shape(magic_pts))
	print( '#######################')
	print ('EIM ends')
	print( 'EIM basis functions selected: ', m_EIM)
	print( 'EIM error: ', err_EIM)
	print( '#######################')
	return EIM_fun, magic_pts,Mp_pts,Mp_fun, err_EIM
