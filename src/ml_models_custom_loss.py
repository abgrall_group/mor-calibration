import keras
from keras.models import Sequential
from keras.layers import Input
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.preprocessing import normalize, RobustScaler
import numpy as np
import matplotlib.pyplot as plt
import pickle
import sklearn.model_selection
import pandas as pd
from bibli import *
from geometry import *


class BasicNeuralModel(object):
    # problem with Keras and TF interface
    def __init__(self,ndim):
        inputs = Input(shape=(ndim,))
        x=keras.layers.core.Dense(6, activation='tanh')(inputs)
        x=keras.layers.normalization.BatchNormalization()(x)
        x=keras.layers.core.Dense(6, activation='tanh')(x)
        x=keras.layers.normalization.BatchNormalization()(x)
        x=keras.layers.core.Dense(6, activation='tanh')(x)
        x=keras.layers.normalization.BatchNormalization()(x)
        x=keras.layers.core.Dense(6, activation='tanh')(x)
        prediction=keras.layers.core.Dense(2,   activation='linear')(x)
        self.model = keras.models.Model(inputs=inputs, outputs=prediction)
        self.model.compile(loss=self.custom_loss_wrapper(inputs), optimizer="adam") #,metrics=["accuracy"])

        # Use Early-Stopping
        # self.callback_early_stopping = keras.callbacks.EarlyStopping(monitor='val_loss', patience=30, verbose=0, mode='auto')

    def custom_loss_wrapper(self,input_tensor):
        def custom_loss(y_true, y_pred):
            y_out=y_pred[:,0]+input_tensor[:,0]*y_pred[:,1]#
            return keras.losses.mean_squared_error(y_true, y_out ) #np.mean((y_out-y_pred)**2.)#
        return custom_loss

    def summary(self):
        print(self.model.summary())

    def fit(self,x_train,y_train,x_valid,y_valid,batch_size=1024,epochs=100):
        self.model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs,\
        validation_data=(x_valid, y_valid), verbose=1)#, callbacks=[self.callback_early_stopping])

    def predict(self,x):
        #x = normalize(x,axis=0)
        res_p = self.model.predict(x)
        return res_p

class BasicRecurrentModel(object):
    # problem with Keras and TF interface
    def __init__(self):
        self.model = keras.models.Sequential()

        self.model.compile(loss="mean_squared_error", optimizer="adam") #,metrics=["accuracy"])

        # Use Early-Stopping
        self.callback_early_stopping = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10, verbose=0, mode='auto')


    def summary(self):
        print(self.model.summary())

    def fit(self,x_train,y_train,x_valid,y_valid,batch_size=1024,epochs=50):
        model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs,\
        validation_data=(x_valid, y_valid), verbose=1, callbacks=[self.callback_early_stopping])

    def predict(self,x):
        #x = normalize(x,axis=0)
        res_p = self.model.predict(x)
        return res_p
