import numpy as np
import random
from cvxopt import matrix
import matplotlib.pyplot as plt
from numpy import linalg as LA
import shutil, os
import math
import solve as so
from bibli import *
from minL1 import *
from GS import *
from EIM import *
import time
from online_func import *
import scipy.special as spc
from geometry import *
import os


###########################################
with_calibration=True


folder="data_3par_big/"
#folder="data_"+str(dim_par)+"_param"

###############################################################################
## DEFINE PDE Problem Parameters !

problem_data=np.load(folder+'problem_data.npz')

truthN=problem_data['arr_0']
CFL=problem_data['arr_1']
Nt=problem_data['arr_2']
dx=problem_data['arr_3']
dt=problem_data['arr_4']
CE=problem_data['arr_5']
x=problem_data['arr_6']
T_fin=problem_data['arr_7']
interval_min=problem_data['arr_8']
interval_max=problem_data['arr_9']

dim_par=np.size(interval_max)

#Import EIM and RB


EIM_fun = np.load(folder+'fun_EIM.npy')
magic_pts = np.load(folder+'magic_pts.npy')
magic_M1=np.load(folder+'magic_M1.npy')
fnc_M1=np.load(folder+'fnc_M1.npy')
EIM_fun_RB = np.load(folder+'EIM_fun_RB.npy')
M1_EIM = np.shape(fnc_M1)[0]

print np.shape(EIM_fun)
print np.shape(EIM_fun_RB)


norm_fnc_M1=np.linalg.norm(fnc_M1,2,axis=0)/np.sqrt(truthN)
EIM_dim=np.shape(EIM_fun)[1]
EIM_inv=np.linalg.inv(EIM_fun[magic_pts,:])
RB = np.load (folder+'RB.npy')
print np.shape(RB)

count_ndisc = np.shape(EIM_fun_RB)[0]
RB=RB[:,:count_ndisc]


if with_calibration:
	file_shift=open(folder+'/shift_basis_data.pkl','rb')
	proj_shift_basis_coeff=pickle.load(file_shift)
	proj_shift_basis=pickle.load(file_shift)
	err_proj_shift_basis=pickle.load(file_shift)
	map_shifts=pickle.load(file_shift)
	file_shift.close()

ndisc=6
if dim_par==3:
	distribution=['unif_MC','unif_MC','unif_MC']#['uniform','uniform','uniform']#'normal'#'
	a=interval_min
	b=interval_max
if dim_par==1:
	distribution=['unif_MC']#['uniform']
	a=[interval_min]
	b=[interval_max]

#weights=[]
#disc_vect=[]
#for k in range(dim_par):
#	if (distribution[k]=='uniform'):
#		#uniform dist interval 
#		#dict used here :disc_locs=[0.47,0.48,0.52,0.53]   
#		qp,weights_qp=np.polynomial.legendre.leggauss(ndisc)
#		disc_vect.append(0.5*(b[k]-a[k])*qp+0.5*(a[k]+b[k]))	
#		weights.append(0.5*(b[k]-a[k])*weights_qp/(b[k]-a[k]))

#	elif (distribution[k]=='normal'):	
#		qp,weights_qp=np.polynomial.hermite.hermgauss(ndisc)
#		M=(a[k]+b[k])/2.
#		sigma=(b[k]-a[k])/4.# 0.1#np.sqrt((b-a)/2./np.sqrt(2.)/spc.erfinv(0.01))
#		disc_vect.append(sigma*np.sqrt(2.0)*qp+M)
#		weights.append((1.0/np.sqrt(math.pi))*weights_qp)

#	elif (distribution[k]=='unif_MC'):	
#		qp=np.random.rand(ndisc)*(b[k]-a[k])+a[k]
#		weights_qp = 1./ndisc*np.ones(ndisc)
#		disc_vect.append(qp)
#		weights.append(weights_qp)

#param_weights=[]

#if dim_par==3:
#	param_domain=[]
#	for a in range(ndisc):
#		for b in range(ndisc):
#			for c in range(ndisc):
#				param_domain.append([disc_vect[0][a],disc_vect[1][b],disc_vect[2][c]])
#				param_weights.append(weights[0][a]*weights[1][b]*weights[2][c])

#if dim_par==1:
#	param_domain=disc_vect[0]
#	param_weights = weights[0]

param_domain=[]
param_weights=[]
num_param=100
for k in range(num_param):
	param_domain.append(np.random.rand(dim_par)*(b-a)+a)
	param_weights.append(1./num_param)
	
print "parameters", param_domain

num_param=len(param_domain)
########################################


Uinit=np.zeros((np.size(param_domain),truthN))
U=np.zeros((truthN, Nt+1))
U_exact = []
U_exact_not_calib = []
U_ROM =[]
U_ROM_not_calib =[]
u0=np.zeros(truthN);
t_truthN=[]
t_ROM=[]
errors=[]
calibs_ind=np.zeros((len(param_domain),Nt+1), dtype=np.int)
calibs_ROM_ind=np.zeros((len(param_domain),Nt+1), dtype=np.int)
for k,mu in enumerate(param_domain):
	print "parameter ",k
	t_init=time.time()
	u0, calibs_ind[k,0] =createInitialCondition(truthN,mu,x, with_calibration)
	Uinit[k,:]=u0
	U_not_calib,U_flux, calib_index = solveFOM(truthN,Nt,u0,dx,dt, with_calibration, mu);
	calibs_ind[k,1:]=calib_index	
	for it in range(0,Nt+1):
		U[:,it]=interpolate(calibs_ind[k,it],U_not_calib[:,it], with_calibration)
	U_exact_not_calib.append(U_not_calib[:,Nt])
	t_truthN.append(time.time()-t_init)
	U_exact.append(U[:,Nt]) # U[(n+calibs_ind[cand,it])%truthN,it]

	
	
	w_ROM, calib_ROM_index, err_ind, true_err, t_fin= online(mu, truthN, Nt,dt,dx, CFL, CE,x, RB, EIM_fun_RB, magic_pts,EIM_inv, magic_M1 ,norm_fnc_M1, M1_EIM , U, with_calibration , proj_shift_basis_coeff, proj_shift_basis, err_proj_shift_basis, map_shifts)
	U_ROM.append(w_ROM)
	t_ROM.append(t_fin)
	errors.append(true_err)
	calibs_ROM_ind[k,:]=calib_ROM_index
	U_ROM_not_calib.append(inverse_interpolate(calibs_ROM_ind[k,Nt], U_ROM[k], with_calibration))


U_exact2=[]
U_ROM2=[]
U_exact2_not_calib=[]
U_ROM2_not_calib=[]
for j in range(len(U_exact)):
	U_exact2.append(U_exact[j]**2.)
	U_ROM2.append(U_ROM[j]**2.)
	U_exact2_not_calib.append(U_exact_not_calib[j]**2.)
	U_ROM2_not_calib.append(U_ROM_not_calib[j]**2.)


expect_exact = np.matmul(param_weights, U_exact)
expect_exact2 = np.matmul(param_weights, U_exact2)
expect_ROM = np.matmul(param_weights, U_ROM)  
expect_ROM2= np.matmul(param_weights, U_ROM2)
expect_exact_not_calib = np.matmul(param_weights, U_exact_not_calib)
expect_exact2_not_calib = np.matmul(param_weights, U_exact2_not_calib)
expect_ROM_not_calib = np.matmul(param_weights, U_ROM_not_calib)  
expect_ROM2_not_calib= np.matmul(param_weights, U_ROM2_not_calib)

variance_exact = num_param/(num_param-1)*(expect_exact2-expect_exact**2.)
variance_ROM = num_param/(num_param-1)*(expect_ROM2-expect_ROM**2.)
variance_exact_not_calib = num_param/(num_param-1)*(expect_exact2_not_calib-expect_exact_not_calib**2.)
variance_ROM_not_calib = num_param/(num_param-1)*(expect_ROM2_not_calib-expect_ROM_not_calib**2.)

err_exp=np.matmul(param_weights, errors)

expect_m_sqrt_var=expect_ROM-np.sqrt(variance_ROM)  - err_exp 
expect_p_sqrt_var=expect_ROM+np.sqrt(variance_ROM) +err_exp
expect_m_sqrt_var_not_calib=expect_ROM_not_calib-np.sqrt(variance_ROM_not_calib)  - err_exp 
expect_p_sqrt_var_not_calib=expect_ROM_not_calib+np.sqrt(variance_ROM_not_calib) +err_exp

expect_m_sqrt_var_truth=expect_exact-np.sqrt(variance_exact)  
expect_p_sqrt_var_truth=expect_exact+np.sqrt(variance_exact) 
expect_m_sqrt_var_truth_not_calib=expect_exact_not_calib-np.sqrt(variance_exact_not_calib)  
expect_p_sqrt_var_truth_not_calib=expect_exact_not_calib+np.sqrt(variance_exact_not_calib) 

#for k in range(len(disc_vect)):
#	plt.plot(U_ROM[k],label="rom")
#	plt.plot(U_exact[k],label="exact")
#	plt.legend()
#	plt.show()

os.system("mkdir "+folder+"UQ \n")


plt.plot(expect_exact, label="exact expectation")
plt.plot(expect_ROM, label="ROM expectation")
plt.plot(expect_p_sqrt_var, label="ROM+ std + ROM err")
plt.plot(expect_m_sqrt_var, label="ROM- std - ROM err")
plt.plot(expect_p_sqrt_var_truth, label="exact+ std ")
plt.plot(expect_m_sqrt_var_truth, label="exact- std ")
plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.01), ncol=2, fancybox=True, shadow=True)
minim=min(expect_m_sqrt_var)
maxim=max(expect_p_sqrt_var)
hight=maxim-minim
plt.ylim([minim-0.1*hight, maxim+0.3*hight])
plt.title("Expectation of calibrated solutions")
plt.savefig(folder+"UQ/expectation.pdf")
plt.show()


plt.plot(variance_exact, label="exact variance")
plt.plot(variance_ROM, label="ROM variance")
minim=min(variance_ROM)
maxim=max(variance_ROM)
hight=maxim-minim
plt.ylim([minim-0.1*hight, maxim+0.2*hight])
plt.title("Variance of calibrated solutions")
plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.01), ncol=2, fancybox=True, shadow=True)
plt.savefig(folder+"UQ/variance.pdf")
plt.show()

plt.plot(expect_exact_not_calib, label="exact expectation")
plt.plot(expect_ROM_not_calib, label="ROM expectation")
plt.plot(expect_p_sqrt_var_not_calib, label="ROM+ std + ROM err")
plt.plot(expect_m_sqrt_var_not_calib, label="ROM- std - ROM err")
plt.plot(expect_p_sqrt_var_truth_not_calib, label="exact+ std ")
plt.plot(expect_m_sqrt_var_truth_not_calib, label="exact- std ")
minim=min(expect_m_sqrt_var_not_calib)
maxim=max(expect_p_sqrt_var_not_calib)
hight=maxim-minim
plt.ylim([minim-0.1*hight, maxim+0.3*hight])
plt.title("Expectation of not calibrated solutions")
plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.01), ncol=2, fancybox=True, shadow=True)
plt.savefig(folder+"UQ/expectation_not_calib.pdf")
plt.show()


plt.plot(variance_exact_not_calib, label="exact variance")
plt.plot(variance_ROM_not_calib, label="ROM variance")
minim=min(variance_ROM_not_calib)
maxim=max(variance_ROM_not_calib)
hight=maxim-minim
plt.ylim([minim-0.1*hight, maxim+0.2*hight])
plt.title("Variance of not calibrated solutions")
plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.01), ncol=2, fancybox=True, shadow=True)
plt.savefig(folder+"UQ/variance_not_calib.pdf")
plt.show()

plt.figure()
for k in range(num_param):
	plt.plot(x, U_ROM[k], label="ROM")
	plt.plot(x, U_exact[k], label="exact")

plt.title("Calibrated position")
plt.show(block=False)

plt.figure()
for k in range(num_param):
	plt.plot(x, U_ROM_not_calib[k], label="ROM")
	plt.plot(x, U_exact_not_calib[k], label="exact")
plt.title("Back to original position")
plt.show(block=False)


np.savez(folder+"UQ/times_error.npz", np.mean(t_truthN),  np.mean(t_ROM), np.mean(errors) )

print "time ROM", np.mean(t_ROM)
print "time truthN", np.mean(t_truthN)



